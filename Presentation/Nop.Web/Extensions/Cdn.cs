﻿using Nop.Core.Infrastructure;
using Nop.Services.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Extensions
{
    public class Cdn
    {   
        public static string Content(string urlStringParameter)
        {

            var settingService = EngineContext.Current.Resolve<ISettingService>();
            var setting = settingService.GetSettingByKey<string>("setting.addressof.cdn");
            string optimizedUrl = urlStringParameter;
            if (!String.IsNullOrEmpty(setting))
            {

            

            if (setting.Length > 5)
            {
                string _path = urlStringParameter.Replace("https://www.doctormito.com/", "").Replace("http://www.doctormito.com/", "").Replace("~/", "");
                if (_path.StartsWith("/"))
                {
                    _path = _path.Substring(1);
                }
                
                if (_path.StartsWith("/"))
                {
                    _path = _path.Substring(1);
                }

                if (_path.StartsWith("/"))
                {
                    _path = _path.Substring(1);
                }

                optimizedUrl = setting + _path;
            }
            }

            return optimizedUrl;
        }
    }
}
