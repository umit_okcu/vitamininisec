﻿using System;
using System.Linq;
using FluentValidation;
using Nop.Core.Domain.Common;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Common;


namespace Nop.Web.Validators.Common
{
    public partial class AddressValidator : BaseNopValidator<AddressModel>
    {

        public AddressValidator(ILocalizationService localizationService,
            IStateProvinceService stateProvinceService,
            AddressSettings addressSettings)
        {


            RuleFor(x => x.TCKN).NotEmpty().WithMessage("Kimlik numarası gereklidir").When(x => x.IsEnterprice == false);
            RuleFor(x => x.TCKN).Must((model, TC) =>
            {


                bool returnvalue = false;

                if (TC != null)
                {

                    if (TC.Length == 11)
                    {
                        Int64 ATCNO, BTCNO, TcNo;
                        long C1, C2, C3, C4, C5, C6, C7, C8, C9, Q1, Q2;

                        TcNo = Int64.Parse(TC);

                        ATCNO = TcNo / 100;
                        BTCNO = TcNo / 100;

                        C1 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C2 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C3 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C4 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C5 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C6 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C7 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C8 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C9 = ATCNO % 10; ATCNO = ATCNO / 10;
                        Q1 = ((10 - ((((C1 + C3 + C5 + C7 + C9) * 3) + (C2 + C4 + C6 + C8)) % 10)) % 10);
                        Q2 = ((10 - (((((C2 + C4 + C6 + C8) + Q1) * 3) + (C1 + C3 + C5 + C7 + C9)) % 10)) % 10);

                        returnvalue = ((BTCNO * 100) + (Q1 * 10) + Q2 == TcNo);
                    }
                }
                return returnvalue;
            }).WithMessage("Lütfen geçerli bir kimlik numarası giriniz.").When(x => x.IsEnterprice == false);

      //      RuleFor(x => x.PhoneNumber).NotEmpty().WithMessage("Telefon numarası gereklidir");
            RuleFor(x => x.PhoneNumber).Must((model, Phone) =>
            {

                string _phone = Phone.Replace("(",null).Replace(")", null).Replace(" ", null).Trim();
                bool returnvalue = false;

                if (_phone != null)
                {

                    if (_phone.Length == 10)
                    {
                        if(_phone[0] == '5' || _phone[0] == '5')
                        {

                        returnvalue = true;
                        }

                    }
                }
                return returnvalue;
            }).WithMessage("Lütfen geçerli bir Telefon numarası giriniz. 0 (5**) *** ** ** ");

            RuleFor(x => x.VD).NotEmpty().WithMessage("Lütfen vergi dairenizi belirtiniz.").When(x => x.IsEnterprice == true);
            RuleFor(x => x.VKN).NotEmpty().WithMessage("Lütfen vergi numaranızı yazınız.").When(x => x.IsEnterprice == true);
            RuleFor(x => x.VKN).Must((model, VKN) =>
            {
                bool result = false;

                if (VKN == null || VKN.Length != 10) { result = false; }
                else
                {

                    var vkn = VKN.ToCharArray();

                    foreach (var a in vkn)
                    {
                        if (!char.IsNumber(a)) return false;
                    }


                    var lastDigit = Convert.ToInt32(vkn[9].ToString());
                    int total = 0;
                    for (int i = 9; i >= 1; i--)
                    {
                        int digit = Convert.ToInt32(vkn[9 - i].ToString());
                        var v1 = ((digit + i) % 10);
                        int v11 = (int)(v1 * Math.Pow(2, i)) % 9;
                        if (v1 != 0 && v11 == 0) v11 = 9;
                        total += v11;
                    }

                    total = (total % 10 == 0) ? 0 : (10 - (total % 10));
                    result = (total == lastDigit);
                }


                bool returnvalue = false;

                if (VKN != null)
                {

                    if (VKN.Length == 11)
                    {
                        Int64 ATCNO, BTCNO, TcNo;
                        long C1, C2, C3, C4, C5, C6, C7, C8, C9, Q1, Q2;

                        TcNo = Int64.Parse(VKN);

                        ATCNO = TcNo / 100;
                        BTCNO = TcNo / 100;

                        C1 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C2 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C3 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C4 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C5 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C6 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C7 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C8 = ATCNO % 10; ATCNO = ATCNO / 10;
                        C9 = ATCNO % 10; ATCNO = ATCNO / 10;
                        Q1 = ((10 - ((((C1 + C3 + C5 + C7 + C9) * 3) + (C2 + C4 + C6 + C8)) % 10)) % 10);
                        Q2 = ((10 - (((((C2 + C4 + C6 + C8) + Q1) * 3) + (C1 + C3 + C5 + C7 + C9)) % 10)) % 10);

                        returnvalue = ((BTCNO * 100) + (Q1 * 10) + Q2 == TcNo);
                    }
                }


                return returnvalue || result;

            }).WithMessage("Lütfen geçerli bir verginumarası giriniz.").When(x => x.IsEnterprice == true);

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Address.Fields.FirstName.Required"));
            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Address.Fields.LastName.Required"));
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Address.Fields.Email.Required"));
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage(localizationService.GetResource("Common.WrongEmail"));
            if (addressSettings.CountryEnabled)
            {
                RuleFor(x => x.CountryId)
                    .NotNull()
                    .WithMessage(localizationService.GetResource("Address.Fields.Country.Required"));
                RuleFor(x => x.CountryId)
                    .NotEqual(0)
                    .WithMessage(localizationService.GetResource("Address.Fields.Country.Required"));
            }
            if (addressSettings.CountryEnabled && addressSettings.StateProvinceEnabled)
            {
                RuleFor(x => x.StateProvinceId).Must((x, context) =>
                {
                    //does selected country has states?
                    var countryId = x.CountryId.HasValue ? x.CountryId.Value : 0;
                    var hasStates = stateProvinceService.GetStateProvincesByCountryId(countryId).Any();

                    if (hasStates)
                    {
                        //if yes, then ensure that state is selected
                        if (!x.StateProvinceId.HasValue || x.StateProvinceId.Value == 0)
                            return false;
                    }

                    return true;
                }).WithMessage(localizationService.GetResource("Address.Fields.StateProvince.Required"));
            }
            if (addressSettings.CompanyRequired && addressSettings.CompanyEnabled)
            {
                RuleFor(x => x.Company).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Company.Required"));
            }
            if (addressSettings.StreetAddressRequired && addressSettings.StreetAddressEnabled)
            {
                RuleFor(x => x.Address1).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress.Required"));
            }
            if (addressSettings.StreetAddress2Required && addressSettings.StreetAddress2Enabled)
            {
                RuleFor(x => x.Address2).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.StreetAddress2.Required"));
            }
            if (addressSettings.ZipPostalCodeRequired && addressSettings.ZipPostalCodeEnabled)
            {
                RuleFor(x => x.ZipPostalCode).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.ZipPostalCode.Required"));
            }
            if (addressSettings.CountyEnabled && addressSettings.CountyRequired)
            {
                RuleFor(x => x.County).NotEmpty().WithMessage(localizationService.GetResource("Address.Fields.County.Required"));
            }
            if (addressSettings.CityRequired && addressSettings.CityEnabled)
            {
                RuleFor(x => x.City).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.City.Required"));
            }
            if (addressSettings.PhoneRequired && addressSettings.PhoneEnabled)
            {
                RuleFor(x => x.PhoneNumber).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Phone.Required"));

            

            }
            if (addressSettings.FaxRequired && addressSettings.FaxEnabled)
            {
                RuleFor(x => x.FaxNumber).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Fax.Required"));
            }
        }

    }

}



