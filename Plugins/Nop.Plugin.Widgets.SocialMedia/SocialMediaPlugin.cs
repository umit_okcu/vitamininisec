﻿using Nop.Core;
using Nop.Core.Domain.Tasks;
using Nop.Core.Plugins;
using Nop.Plugin.Widgets.SocialMedia.Data;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia
{
    public class SocialMediaPlugin : BasePlugin, IWidgetPlugin
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly ISettingService _settingService;
        private readonly SocialMediaObjectContext _socialMediaObjectContext;
        #endregion

        #region Ctor

        public SocialMediaPlugin(SocialMediaObjectContext socialMediaObjectContext,
                 IScheduleTaskService scheduleTaskService,
        ILocalizationService localizationService,
            IWebHelper webHelper,
            ISettingService settingService)
        {
            this._scheduleTaskService = scheduleTaskService;
            this._socialMediaObjectContext = socialMediaObjectContext;
            this._localizationService = localizationService;
            this._webHelper = webHelper;
            this._settingService = settingService;
        }
        #endregion

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsSocialMedia";
        }

        public IList<string> GetWidgetZones()
        {
            return new List<string> { "WidgetsSocialMediaZone" };
        }

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/WidgetsSocialMedia/Configure";
        }

        public override void Install()
        {

            //settings
            var settings = new SocialMediaSettings
            {
                InstagramUserName = "kelebekmobilya",
                InstagramPassword = "uwy89*2018-ke*",
                InstagramUri = "https://www.instagram.com/p/",
                IsSocialMediaEnabled = true
            };
            _settingService.SaveSetting(settings);
            if (_scheduleTaskService.GetTaskByType(SocialMediaDefaults.RenewShippingTask) == null)
            {
                _scheduleTaskService.InsertTask(new ScheduleTask
                {
                    Enabled = true,
                    Seconds = SocialMediaDefaults.AccessTokenRenewalPeriodRecommended * 60 * 60,

                    Name = SocialMediaDefaults.RenewShippingTaskName,
                    Type = SocialMediaDefaults.RenewShippingTask,
                });
            }

            _socialMediaObjectContext.Install();

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUserName", "Soru");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUserName.Hint", "Müş¸terilerinizi şarj etmek için ek ücret girin.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramPassword", "Ek ücret. Kullanım yüzdesi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramPassword.Hint", "Sipariş¸ toplamına yüzde ek bir ücret uygulayıp uygulamayacağınızı belirler. Etkinleş¸tirilmemiş¸se, sabit bir değer kullanılır.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUri", "Ek ücret");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUri.Hint", "Müş¸terilerinizi şarj etmek için ek ücret girin.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.IsSocialMediaEnabled", "Ek ücret. Kullanım yüzdesi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.SocialMedia.IsSocialMediaEnabled.Hint", "Sipariş¸ toplamına yüzde ek bir ücret uygulayıp uygulamayacağınızı belirler. Etkinleş¸tirilmemiş¸se, sabit bir değer kullanılır.");



            base.Install();
        }

        public override void Uninstall()
        {
            _settingService.DeleteSetting<SocialMediaSettings>();

            var task = _scheduleTaskService.GetTaskByType(SocialMediaDefaults.RenewShippingTask);
            if (task != null)
                _scheduleTaskService.DeleteTask(task);

            _socialMediaObjectContext.Uninstall();

            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUserName");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUserName.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramPassword");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramPassword.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUri");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.InstagramUri.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.IsSocialMediaEnabled");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.SocialMedia.IsSocialMediaEnabled.Hint");



            base.Uninstall();

        }



        }
    }
