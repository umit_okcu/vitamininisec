﻿using Nop.Core;
using Nop.Plugin.Widgets.SocialMedia.Domain;
using Nop.Plugin.Widgets.SocialMedia.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia.Services
{
    public partial interface ISocialMediaServices
    {

    
        IPagedList<SocialMediaRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);


        SocialMediaRecord GetById(int socialMediaRecordId);



        void InsertsocialMediaRecord(SocialMediaRecord socialMediaRecord);


        void UpdatesocialMediaRecord(SocialMediaRecord socialMediaRecord);

   
        void DeletesocialMediaRecord(SocialMediaRecord socialMediaRecord);

    }
}
