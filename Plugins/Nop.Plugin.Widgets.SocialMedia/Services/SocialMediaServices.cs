﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Widgets.SocialMedia.Domain;

using System.Linq;


namespace Nop.Plugin.Widgets.SocialMedia.Services
{
    public partial class SocialMediaServices : ISocialMediaServices
    {

        private const string SOCIALMEDIA_ALL_KEY = "Nop.socialmedia.all-{0}-{1}";
        private const string SOCIALMEDIA_PATTERN_KEY = "Nop.socialmedia.";

        private readonly ICacheManager _cacheManager;
        private readonly IRepository<SocialMediaRecord> _socialMediaRepository;


        public SocialMediaServices(IRepository<SocialMediaRecord> socialMediaRepository,
            ICacheManager cacheManager)
        {
            this._socialMediaRepository = socialMediaRepository;
            this._cacheManager = cacheManager;
        }


        public virtual IPagedList<SocialMediaRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var key = string.Format(SOCIALMEDIA_ALL_KEY, pageIndex, pageSize);

            return _cacheManager.Get(key, () =>
            {
                var query = from socialMedia in _socialMediaRepository.Table
                            //orderby socialMedia.Id
                            select socialMedia;

                var records = new PagedList<SocialMediaRecord>(query, pageIndex, pageSize);
                return records;
            });
        }

        public SocialMediaRecord GetById(int socialMediaRecordId)
        {
            if (socialMediaRecordId == 0)
                return null;

            return _socialMediaRepository.GetById(socialMediaRecordId);
        }

        public void InsertsocialMediaRecord(SocialMediaRecord socialMediaRecord)
        {
            if (socialMediaRecord == null)
                throw new ArgumentNullException(nameof(socialMediaRecord));

            _socialMediaRepository.Insert(socialMediaRecord);

            _cacheManager.RemoveByPattern(SOCIALMEDIA_PATTERN_KEY);
        }

        public void UpdatesocialMediaRecord(SocialMediaRecord socialMediaRecord)
        {
            if (socialMediaRecord == null)
                throw new ArgumentNullException(nameof(socialMediaRecord));

            _socialMediaRepository.Update(socialMediaRecord);

            _cacheManager.RemoveByPattern(SOCIALMEDIA_PATTERN_KEY);
        }

        public void DeletesocialMediaRecord(SocialMediaRecord socialMediaRecord)
        {
            if (socialMediaRecord == null)
                throw new ArgumentNullException(nameof(socialMediaRecord));

            _socialMediaRepository.Delete(socialMediaRecord);

            _cacheManager.RemoveByPattern(SOCIALMEDIA_PATTERN_KEY);
        }
    }
}
