﻿using System;
using System.Collections.Generic;
using System.Text;

using Nop.Core;

using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Payments;
using Nop.Services.Tasks;
using Nop.Services.Shipping;
using Nop.Core.Domain.Shipping;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;
using System.Linq;
using Nop.Services.Common;

using Newtonsoft.Json;
using System.Threading.Tasks;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Classes.Models;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Logger;
using System.IO;
using InstagramApiSharp;
using Nop.Plugin.Widgets.SocialMedia.Domain;
using Nop.Plugin.Widgets.SocialMedia.Models;

namespace Nop.Plugin.Widgets.SocialMedia.Services
{
    public class SocialMediaScheduledTask : IScheduleTask
    {

        private readonly ISettingService _settingService;
        private readonly ISocialMediaServices _socialMediaServices;
        private readonly SocialMediaSettings _socialMediaSettings;
        private readonly ILogger _logger;

        
        public SocialMediaScheduledTask(ILogger logger,ISettingService settingService, SocialMediaSettings socialMediaSettings, ISocialMediaServices socialMediaServices
                                       )
        {
            this._logger = logger;
            this._settingService = settingService;
            this._socialMediaServices = socialMediaServices;
            this._socialMediaSettings = socialMediaSettings;
        }


        public void Execute()
        {
            var SocialMediaSettings = _settingService.LoadSetting<SocialMediaSettings>(0);

            IPagedList<SocialMediaRecord> SocialMediaData = _socialMediaServices.GetAll(0, Int32.MaxValue);

            string userName = SocialMediaSettings.InstagramUserName;
            string password = SocialMediaSettings.InstagramPassword;
            string instagramUri = SocialMediaSettings.InstagramUri;


            Task<IResult<InstaMediaList>> requestData = GetInstagramData(userName, password);
            requestData.Wait();


            List<SocialMediaModel> model = new List<SocialMediaModel>();


            if (requestData.Result.Succeeded == true)
            {
                IResult<InstaMediaList> data = requestData.Result;


                foreach (var item in data.Value)
                {
                    var code = instagramUri+ item.Code; // page with https://www.instagram.com/p/+data 
                                          //  var mahmut77 = item.Caption.Text; // comment of posty
                    var identifier = item.InstaIdentifier; /// Id content
                    InstaImage image = new InstaImage();
                    var images = item.Images.Where(x => x.Width == 240);
                    bool gotImage = false;
                    bool gotImageRealy = false;
                    var e = images as System.Collections.IEnumerable;
                   // if (e == null || !e.GetType().GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))) gotOrder = false;

                    foreach (object _ in e)
                    {
                        gotImage = true;
                    }

                    if(gotImage == true)
                    {
                        image = images.First();

                    }
                    else
                    {
                    var t = images as System.Collections.IEnumerable;
                    

                    foreach (object _ in t)
                    {
                        gotImageRealy = true;
                    }



                        if (gotImageRealy)
                        {

                        image = item.Images.FirstOrDefault();
                        }
                    }




                    //      Console.ReadLine();
                    //     Console.WriteLine(image.Uri);

                    if(gotImageRealy || gotImage)
                    {
                    _logger.Error("SocialMedia | "+image.Uri);
                    var myImageUri =  image.Uri;




                    SocialMediaModel mod = new SocialMediaModel()
                    {

                        ImageUri = myImageUri,
                        Code = code
                    };
                    model.Add(mod);

                    }


                }


            }
            model.Reverse();

            foreach (var item in model)


            {
                SocialMediaRecord socialMediaRecord = new SocialMediaRecord()
                {
                    Code = item.Code,
                    ImageUri =item.ImageUri
                };
                if(SocialMediaData.FirstOrDefault(x => x.Code == item.Code) == null)
                {

                _socialMediaServices.InsertsocialMediaRecord(socialMediaRecord);
                    _logger.Error("SocialMedia |  new data Inserted for Instagram for code : "+ item.Code);
                }
                else
                {
                    _socialMediaServices.UpdatesocialMediaRecord(socialMediaRecord);
                    _logger.Error("SocialMedia |  new data Updated for Instagram for code : " + item.Code);
                }
            }

            _logger.Error("SocialMedia | " + model.Count + " new data fetched for Instagram.");



        }
           

            public  async Task<IResult<InstaMediaList>> GetInstagramData(string userName, string password)
        {

            var socialMediaSettings = _settingService.LoadSetting<SocialMediaSettings>(0);

            var userSession = new UserSessionData
            {
                UserName = socialMediaSettings.InstagramUserName,
                Password = socialMediaSettings.InstagramPassword
            };

            var _instaApi = InstaApiBuilder.CreateBuilder()
                .SetUser(userSession)
                .UseLogger(new DebugLogger(LogLevel.Exceptions))
                .Build();
            const string stateFile = "state.bin";
            try
            {
                // load session file if exists
                if (File.Exists(stateFile))
                {
                   // Console.WriteLine("Loading state from file");
                    using (var fs = File.OpenRead(stateFile))
                    {
                        // _instaApi.LoadStateDataFromStream(fs);
                        // in .net core or uwp apps don't use LoadStateDataFromStream
                        // use this one:
                        _instaApi.LoadStateDataFromString(new StreamReader(fs).ReadToEnd());
                        // you should pass json string as parameter to this function.
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error("SocialMedia | Error occured while fetching data: "+e);
               // Console.WriteLine(e);
            }

            if (!_instaApi.IsUserAuthenticated)
            {
                _logger.Error($"SocialMedia | Logging in as {userSession.UserName} ");
                // login
                //   Console.WriteLine($"Logging in as {userSession.UserName}");
                var logInResult = await _instaApi.LoginAsync();
                if (!logInResult.Succeeded)
                {

                    _logger.Error($"SocialMedia | Unable to login: {logInResult.Info.Message} " );
                  //  Console.WriteLine($"Unable to login: {logInResult.Info.Message}");

                }
            }
            var state = _instaApi.GetStateDataAsString();
            // save session in file
            //  var state = _instaApi.GetStateDataAsStream();
            // in .net core or uwp apps don't use GetStateDataAsStream.
            // use this one:
            // var state = _instaApi.GetStateDataAsString();
            // this returns you session as json string.
            // using (var fileStream = File.Create(stateFile))
            // {
            //     state.Seek(0, SeekOrigin.Begin);
            //     state.CopyTo(fileStream);
            // }


            IResult<InstaMediaList> userFeed = await _instaApi.UserProcessor.GetUserMediaAsync(socialMediaSettings.InstagramUserName, PaginationParameters.MaxPagesToLoad(1));


            return userFeed;
        }
    }
}





