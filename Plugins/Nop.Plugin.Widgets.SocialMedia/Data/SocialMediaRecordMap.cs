﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Data.Mapping;
using Nop.Plugin.Widgets.SocialMedia.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia.Data
{
    public partial class SocialMediaRecordMap : NopEntityTypeConfiguration<SocialMediaRecord>
    {
        public override void Configure(EntityTypeBuilder<SocialMediaRecord> builder)
        {
            builder.ToTable(nameof(SocialMediaRecord));
            builder.HasKey(record => record.Id);

            builder.Property(record => record.Code);
            builder.Property(record => record.ImageUri);
        }


    }
}
