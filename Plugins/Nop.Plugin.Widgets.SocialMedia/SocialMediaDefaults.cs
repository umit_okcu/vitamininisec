﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia
{
    public class SocialMediaDefaults
    {

        /// <summary>
        /// Name of the renew access token schedule task
        /// </summary>
        public static string RenewShippingTaskName => "Social Media Task";

        /// <summary>
        /// Type of the renew access token schedule task
        /// </summary>
        public static string RenewShippingTask => "Nop.Plugin.Widgets.SocialMedia.Services.SocialMediaScheduledTask";

        /// <summary>
        /// Default access token renewal period in days
        /// </summary>
        public static int AccessTokenRenewalPeriodRecommended => 12;
    }
}
