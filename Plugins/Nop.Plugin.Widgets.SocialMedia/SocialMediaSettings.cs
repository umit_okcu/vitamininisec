﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.SocialMedia
{
    /// <summary>
    /// Represents settings of the "Fixed or by weight" shipping plugin
    /// </summary>
    public class SocialMediaSettings : ISettings
    {
        
        public string InstagramUserName { get; set; }
        public string InstagramPassword { get; set; }
        public string InstagramUri{ get; set; } 
        public bool IsSocialMediaEnabled { get; set; }

    }
}