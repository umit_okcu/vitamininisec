﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia.Models
{
    public class ConfigurationModel : BaseNopModel
    {

        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.Code")]
        public string Code { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.ImageUri")]
        public string ImageUri { get; set; }
        
        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.Id")]
        public int Id { get; set; }
   
       
    }

    public class ConfigurationSettingsModel : BaseNopModel
    {
       // [NopResourceDisplayName("Plugins.Widgets.SocialMedia.Code")]
        public List<ConfigurationModel> List { get; set; }
       // [NopResourceDisplayName("Plugins.Widgets.SocialMedia.ImageUri")]
       // public string ImageUri { get; set; }
       //
        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.Id")]
        public int Id { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.InstagramUserName")]
        public string InstagramUserName { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.InstagramPassword")]
        public string InstagramPassword { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.InstagramUri")]
        public string InstagramUri { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.SocialMedia.IsSocialMediaEnabled")]
        public bool IsSocialMediaEnabled { get; set; }


    }
}
