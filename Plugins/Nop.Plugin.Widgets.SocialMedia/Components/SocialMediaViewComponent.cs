﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.SocialMedia.Domain;
using Nop.Plugin.Widgets.SocialMedia.Models;
using Nop.Plugin.Widgets.SocialMedia.Services;
using Nop.Services.Configuration;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia.Components
{
    [ViewComponent(Name = "WidgetsSocialMedia")]
    public class SocialMediaViewComponent : NopViewComponent
    {

        private readonly ISettingService _settingService;
        private readonly ISocialMediaServices _socialMediaServices;
        private readonly SocialMediaSettings _socialMediaSettings;
        
        public SocialMediaViewComponent(ISettingService settingService, SocialMediaSettings socialMediaSettings, ISocialMediaServices socialMediaServices
                                       )
        {
            this._settingService = settingService;
            this._socialMediaServices = socialMediaServices;
            this._socialMediaSettings = socialMediaSettings;
        }

        public IViewComponentResult Invoke()
        {

            var SocialMediaSettings = _settingService.LoadSetting<SocialMediaSettings>(0);

            var SocialMediaData = _socialMediaServices.GetAll();
            List<SocialMediaModel> model = new List<SocialMediaModel>();
            foreach (var item in SocialMediaData)
            {
            SocialMediaModel mod = new SocialMediaModel() {

                ImageUri = item.ImageUri,
                Code = item.Code,
                Id = item.Id
            };


                model.Add(mod);

            }

            model.Reverse();

            return View("~/Plugins/Widgets.SocialMedia/Views/SocialMedia.cshtml", model);
        }
        }
    }
