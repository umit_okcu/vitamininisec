﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Widgets.SocialMedia.Models;
using Nop.Plugin.Widgets.SocialMedia.Services;
using Nop.Services.Configuration;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class SocialMediaController : BasePluginController
    {

        private readonly ISocialMediaServices _socialMediaService;
        private readonly SocialMediaSettings _socialMediaSettings;
        private readonly ISettingService _settingService;
        


        public SocialMediaController(ISocialMediaServices socialMediaServices,SocialMediaSettings socialMediaSettings,ISettingService settingService)
        {
            this._settingService = settingService;
            this._socialMediaService = socialMediaServices;
            this._socialMediaSettings = socialMediaSettings;
        }

        public IActionResult Configure()
        {
            var model = new List<ConfigurationModel>();

            var rows =_socialMediaService.GetAll();
            foreach (var item in rows)
            {
                var row = new ConfigurationModel() {
                    Id=item.Id,
                    Code=item.Code,
                    ImageUri  = item.ImageUri
                    
                };
                model.Add(row);

            }

            ConfigurationSettingsModel models = new ConfigurationSettingsModel();
            models.List = model;

            return View("~/Plugins/Widgets.SocialMedia/Views/Configure.cshtml", models);
        }


        [HttpPost]
        [AdminAntiForgery]
        public IActionResult Configure(ConfigurationSettingsModel model)
        {

            var settings = new SocialMediaSettings()
            {
                InstagramUserName = model.InstagramUserName,
                InstagramPassword = model.InstagramPassword,
                InstagramUri = model.InstagramUri,
                IsSocialMediaEnabled = model.IsSocialMediaEnabled
            };
         
            _settingService.SaveSetting(settings);
            return Configure();
        }



    }
}
