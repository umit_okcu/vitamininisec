﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Plugin.Widgets.SocialMedia.Data;
using Nop.Web.Framework.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;




namespace Nop.Plugin.Widgets.SocialMedia.Infrastructure
{
    public class PluginDbStartup : INopStartup
    {
        public int Order => 12;

        public void Configure(IApplicationBuilder application)
        {
            
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SocialMediaObjectContext>(optionsBuilder => {
                optionsBuilder.UseSqlServerWithLazyLoading(services);
            });
        }
    }
}
