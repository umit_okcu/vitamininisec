﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Widgets.SocialMedia.Data;
using Nop.Plugin.Widgets.SocialMedia.Domain;
using Nop.Plugin.Widgets.SocialMedia.Services;
using Nop.Web.Framework.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.SocialMedia.Infrastructure
{
    class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 1;

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<SocialMediaServices>().As<ISocialMediaServices>().InstancePerLifetimeScope();
          
            //data context
            builder.RegisterPluginDataContext<SocialMediaObjectContext>("nop_object_context_socialmedia_zip");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<SocialMediaRecord>>().As<IRepository<SocialMediaRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_socialmedia_zip"))
                .InstancePerLifetimeScope();
        }
    }
}
