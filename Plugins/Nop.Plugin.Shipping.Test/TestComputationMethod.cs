﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;

using System.Xml;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
//using Nop.Plugin.Shipping.UPS.Domain;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
namespace Nop.Plugin.Shipping.Test
{
    public class TestComputationMethod : BasePlugin, IShippingRateComputationMethod
    {
        public ShippingRateComputationMethodType ShippingRateComputationMethodType
        {
            get { return ShippingRateComputationMethodType.Offline; }
        }

        public IShipmentTracker ShipmentTracker
        {
            get
            {
                //uncomment a line below to return a general shipment tracker (finds an appropriate tracker by tracking number)
                //return new GeneralShipmentTracker(EngineContext.Current.Resolve<ITypeFinder>());
                return null;
            }
        }

        /// <summary>
        /// Gets fixed shipping rate (if shipping rate computation method allows it and the rate can be calculated before checkout).
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Fixed shipping rate; or null in case there's no fixed shipping rate</returns>
        public decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest)
        {
            if (getShippingOptionRequest == null)
                throw new ArgumentNullException(nameof(getShippingOptionRequest));

            ////if the "shipping calculation by weight" method is selected, the fixed rate isn't calculated
            //if (_fixedByWeightByTotalSettings.ShippingByWeightByTotalEnabled)
            //    return null;

            //var restrictByCountryId = getShippingOptionRequest.ShippingAddress?.Country?.Id;
            //var rates = _shippingService.GetAllShippingMethods(restrictByCountryId)
            //    .Select(shippingMethod => GetRate(shippingMethod.Id)).Distinct().ToList();

            ////return default rate if all of them equal
            //if (rates.Count == 1)
            //    return rates.FirstOrDefault();

            return 9.9m;
        }
        /// <summary>
        ///  Gets available shipping options
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Represents a response of getting shipping rate options</returns>
        public GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest)
        {
            if (getShippingOptionRequest == null)
                throw new ArgumentNullException(nameof(getShippingOptionRequest));

            var response = new GetShippingOptionResponse();

            if (getShippingOptionRequest.Items == null || !getShippingOptionRequest.Items.Any())
            {
                response.AddError("No shipment items");
                return response;
            }

            //choose the shipping rate calculation method
            //if (_fixedByWeightByTotalSettings.ShippingByWeightByTotalEnabled)
            //{
            //    //shipping rate calculation by products weight

            //    if (getShippingOptionRequest.ShippingAddress == null)
            //    {
            //        response.AddError("Shipping address is not set");
            //        return response;
            //    }

            //    var storeId = getShippingOptionRequest.StoreId != 0 ? getShippingOptionRequest.StoreId : _storeContext.CurrentStore.Id;
            //    var countryId = getShippingOptionRequest.ShippingAddress.CountryId ?? 0;
            //    var stateProvinceId = getShippingOptionRequest.ShippingAddress.StateProvinceId ?? 0;
            //    var warehouseId = getShippingOptionRequest.WarehouseFrom?.Id ?? 0;
            //    var zip = getShippingOptionRequest.ShippingAddress.ZipPostalCode;

            //    //get subtotal of shipped items
            //    var subTotal = decimal.Zero;
            //    foreach (var packageItem in getShippingOptionRequest.Items)
            //    {
            //        if (_shippingService.IsFreeShipping(packageItem.ShoppingCartItem))
            //            continue;

            //        //TODO we should use getShippingOptionRequest.Items.GetQuantity() method to get subtotal
            //        subTotal += _priceCalculationService.GetSubTotal(packageItem.ShoppingCartItem);
            //    }

            //    //get weight of shipped items (excluding items with free shipping)
            //    var weight = _shippingService.GetTotalWeight(getShippingOptionRequest, ignoreFreeShippedItems: true);

            //    foreach (var shippingMethod in _shippingService.GetAllShippingMethods(countryId))
            //    {
            //        var rate = GetRate(subTotal, weight, shippingMethod.Id, storeId, warehouseId, countryId, stateProvinceId, zip);
            //        if (!rate.HasValue)
            //            continue;

            //        response.ShippingOptions.Add(new ShippingOption
            //        {
            //            Name = _localizationService.GetLocalized(shippingMethod, x => x.Name),
            //            Description = _localizationService.GetLocalized(shippingMethod, x => x.Description),
            //            Rate = rate.Value
            //        });
            //    }
            //}
            //else
            //{
            //    //shipping rate calculation by fixed rate
            //    var restrictByCountryId = getShippingOptionRequest.ShippingAddress?.Country?.Id;
            //    response.ShippingOptions = _shippingService.GetAllShippingMethods(restrictByCountryId).Select(shippingMethod => new ShippingOption
            //    {
            //        Name = _localizationService.GetLocalized(shippingMethod, x => x.Name),
            //        Description = _localizationService.GetLocalized(shippingMethod, x => x.Description),
            //        Rate = GetRate(shippingMethod.Id)
            //    }).ToList();
            //}

            return response;
        }
    }
}
