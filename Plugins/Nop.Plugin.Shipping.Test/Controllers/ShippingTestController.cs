﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Directory;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

using Nop.Core;
//using Nop.Plugin.Shipping.UPS.Domain;
using Nop.Plugin.Shipping.Test.Models;
using Nop.Services;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;


namespace Nop.Plugin.Shipping.Test.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class ShippingTestController : BasePluginController
    {

        #region Fields

        private readonly CurrencySettings _currencySettings;
        //private readonly FixedByWeightByTotalSettings _fixedByWeightByTotalSettings;
        //private readonly ICountryService _countryService;
        //private readonly ICurrencyService _currencyService;
        private readonly ILocalizationService _localizationService;
        //  private readonly IMeasureService _measureService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        //   private readonly IShippingByWeightByTotalService _shippingByWeightService;
        //   private readonly IShippingService _shippingService;
        //   private readonly IStateProvinceService _stateProvinceService;
        //  private readonly IStoreService _storeService;
        private readonly MeasureSettings _measureSettings;

        #endregion

        #region Ctor

        public ShippingTestController(CurrencySettings currencySettings,
            // FixedByWeightByTotalSettings fixedByWeightByTotalSettings,
            //  ICountryService countryService,
            //  ICurrencyService currencyService,
            ILocalizationService localizationService,
            //  IMeasureService measureService,
            IPermissionService permissionService,
            ISettingService settingService,
            //  IShippingByWeightByTotalService shippingByWeightService,
            //  IShippingService shippingService,
            // IStateProvinceService stateProvinceService,
            // IStoreService storeService,
            MeasureSettings measureSettings)
        {
            this._currencySettings = currencySettings;
            //     this._fixedByWeightByTotalSettings = fixedByWeightByTotalSettings;
            //     this._countryService = countryService;
            //     this._currencyService = currencyService;
            this._localizationService = localizationService;
            //    this._measureService = measureService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            //     this._shippingByWeightService = shippingByWeightService;
            //      this._stateProvinceService = stateProvinceService;
            //      this._shippingService = shippingService;
            //      this._storeService = storeService;
            this._measureSettings = measureSettings;
        }

        #endregion

        #region Methods
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return AccessDeniedView();

            var model = new ConfigurationModel
            {
                //LimitMethodsToCreated = _fixedByWeightByTotalSettings.LimitMethodsToCreated,
                //ShippingByWeightByTotalEnabled = _fixedByWeightByTotalSettings.ShippingByWeightByTotalEnabled
            };

            //stores
            //model.AvailableStores.Add(new SelectListItem { Text = "*", Value = "0" });
            //foreach (var store in _storeService.GetAllStores())
            //    model.AvailableStores.Add(new SelectListItem { Text = store.Name, Value = store.Id.ToString() });
            ////warehouses
            //model.AvailableWarehouses.Add(new SelectListItem { Text = "*", Value = "0" });
            //foreach (var warehouses in _shippingService.GetAllWarehouses())
            //    model.AvailableWarehouses.Add(new SelectListItem { Text = warehouses.Name, Value = warehouses.Id.ToString() });
            ////shipping methods
            //foreach (var sm in _shippingService.GetAllShippingMethods())
            //    model.AvailableShippingMethods.Add(new SelectListItem { Text = sm.Name, Value = sm.Id.ToString() });
            ////countries
            //model.AvailableCountries.Add(new SelectListItem { Text = "*", Value = "0" });
            //var countries = _countryService.GetAllCountries();
            //foreach (var c in countries)
            //    model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            ////states
            //model.AvailableStates.Add(new SelectListItem { Text = "*", Value = "0" });

            return View("~/Plugins/Shipping.Test/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageShippingSettings))
                return Content("Access denied");

            ////save settings
            //_fixedByWeightByTotalSettings.LimitMethodsToCreated = model.LimitMethodsToCreated;
            //_settingService.SaveSetting(_fixedByWeightByTotalSettings);

            return Json(new { Result = true });
        }
        #endregion
    }
}
