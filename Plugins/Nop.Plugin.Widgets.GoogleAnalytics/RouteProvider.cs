﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.GoogleAnalytics
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Plugin.Widgets.GoogleAnalytics.GetOrders", "GetOrders",
                new { controller = "InvoiceOrderInfo", action = "GetOrderInfo" }
         );
            routeBuilder.MapRoute("Plugin.Widgets.GoogleAnalytics.GetOrdersById", "GetOrdersById",
              new { controller = "InvoiceOrderInfo", action = "GetOrderFromId" }
       );
            routeBuilder.MapRoute("Plugin.Widgets.GoogleAnalytics.PutInvoiceSeries", "PutInvoiceSeries",
    new { controller = "InvoiceOrderInfo", action = "PutInvoiceSeries" }
);
            
        }
        


        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
