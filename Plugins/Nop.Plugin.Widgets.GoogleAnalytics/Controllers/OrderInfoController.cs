﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Nop.Services.Orders;
using Nop.Plugin.Widgets.GoogleAnalytics.Models;
using Nop.Services.Logging;
using Nop.Services.Configuration;
using System.Linq;
using Nop.Core.Domain.Shipping;
using Newtonsoft.Json;
using Nop.Core.Domain.Orders;

namespace Nop.Plugin.Widgets.GoogleAnalytics.Controllers
{
    [Route("api/")]
    [ApiController]
    public class OrderInfoController : ControllerBase
    {
        #region Fields


        private readonly IOrderService _orderService;
        private readonly ILogger _logger;
        private readonly ISettingService _settingService;


        #endregion

        #region Ctor

        public OrderInfoController(
            ILogger logger,
            IOrderService orderService,
            ISettingService settingService
        )
        {
            this._logger = logger;
            this._orderService = orderService;
            this._settingService = settingService;

        }

        #endregion

   //     [HttpPost("{username}/{password}/{ordercount}")]
   //     public IActionResult GetOrderInfo(string username, string password, string ordercount)
   //     {
   //         string nameShould = _settingService.GetSettingByKey<string>("faturalamaservisi.username");
   //         string passShould = _settingService.GetSettingByKey<string>("faturalamaservisi.password");
   //
   //         if(! (username == nameShould && password == passShould))
   //         {
   //             _logger.Error("OrderInfoRequested | username or password is not valid");
   //
   //             string errorString = "OrderInfoRequested | username or password is not valid";
   //
   //             return StatusCode(404, errorString); //NotFound(errorString);//  new JsonResult(errorString);
   //         }
   //             _logger.Error("OrderInfoRequested | engine started");
   //
   //             //var DateFrom = null;// DateTime.UtcNow.AddDays(-1);
   //             //var DateTo = null;//DateTime.UtcNow;
   //             bool shouldContinue = Int32.TryParse(ordercount, out int _ordercount);
   //
   //         if (!(shouldContinue))
   //         {
   //             _logger.Error("OrderInfoRequested | 'ordercount' is not valid");
   //
   //             string errorString = "OrderInfoRequested | 'ordercount' is not valid";
   //
   //             return StatusCode(404, errorString); //NotFound(errorString);//  new JsonResult(errorString);
   //         }
   //         //         var _order = _orderService.GetOrderByCustomOrderNumber(orderid);
   //
   //         //       while (_order == null)
   //         //     {
   //         //       _orderid++;
   //         //
   //         //       _order = _orderService.GetOrderByCustomOrderNumber(_orderid.ToString());
   //         //
   //         //   }
   //         
   //         var _orders = _orderService.SearchOrders(0, 0, 0, 0, 0, 0, 0, null, null, null, null, null, null, null, null, string.Empty, null, 0, 999999999, false);
   //             
   //             InvoiceDataModel data = new InvoiceDataModel();
   //             HashSet<Datum> Orders = new HashSet<Datum>();
   //         if (_orders == null) //&& shouldContinue)
   //         {
   //
   //             string errorString = "OrderInfoRequested | There is no new order to send.";
   //
   //             return StatusCode(400, errorString);
   //
   //         }
   //                 var selectedOrders =  _orders.OrderBy(x=>x.CreatedOnUtc).Where(x => x.ShippingStatus != ShippingStatus.Delivered&&x.AuthorizationTransactionCode==null).Take(_ordercount);
   //
   //
   //
   //         if (!(selectedOrders.Count()>0)) //&& shouldContinue)
   //         {
   //
   //             string errorString = "OrderInfoRequested | There is no new order to send.";
   //
   //             return StatusCode(400, errorString);
   //
   //         }
   //         // var _ordersData = _orders.Where(x => x.Id > _orderid);
   //         //_orders.Reverse();
   //
   //         foreach (var item in selectedOrders)
   //                 {
   //
   //                     Datum order = new Datum();
   //
   //                     SiparisData siparisData = new SiparisData();
   //
   //                     siparisData.AdresId = item.BillingAddress.Id;
   //                     siparisData.Adres1 = item.BillingAddress.Address1;
   //                     siparisData.Adres2 = item.BillingAddress.Address2;
   //                     siparisData.Eposta = item.BillingAddress.Email;
   //                     siparisData.Il = item.BillingAddress.StateProvince.Name;
   //                     siparisData.Ilce = item.BillingAddress.City;
   //                     siparisData.Kargo = Convert.ToDouble(item.OrderShippingInclTax);
   //                     siparisData.MusteriAdi = item.BillingAddress.FirstName;
   //                     siparisData.MusteriId = item.CustomerId;
   //                     siparisData.MusteriSoyadi = item.BillingAddress.LastName;
   //
   //                     if (item.BillingAddress.IsEnterprice)
   //
   //                     {
   //
   //                         siparisData.MusteriTipi = "Tüzel";
   //                         siparisData.MusteriUnvani = item.BillingAddress.Company;
   //                         siparisData.Tckn = "";
   //                         siparisData.VergiDairesi = item.BillingAddress.VD;
   //                         siparisData.VergiNo = item.BillingAddress.VKN;
   //                     }
   //                     else
   //                     {
   //                         siparisData.MusteriTipi = "Şahıs";
   //                         siparisData.MusteriUnvani = item.BillingAddress.FirstName + " " + item.BillingAddress.LastName;
   //                         siparisData.Tckn = item.BillingAddress.TCKN;
   //                         siparisData.VergiDairesi = "";
   //                         siparisData.VergiNo = "";
   //                     }
   //                     siparisData.PostaKodu = item.BillingAddress.ZipPostalCode;
   //                     siparisData.SiparisId = item.Id;
   //                     siparisData.SiparisTarihi = Convert.ToDateTime(item.PaidDateUtc);
   //                     siparisData.Tel1 = item.BillingAddress.PhoneNumber;
   //
   //                     siparisData.Ulke = "Türkiye";
   //
   //                     SiparisSevkiyat siparisSevkiyat = new SiparisSevkiyat();
   //
   //                     siparisSevkiyat.AdresId = item.ShippingAddress.Id;
   //                     siparisSevkiyat.SevkAdi = item.ShippingAddress.FirstName;
   //                     siparisSevkiyat.SevkAdres1 = item.ShippingAddress.Address1;
   //                     siparisSevkiyat.SevkAdres2 = item.ShippingAddress.Address2;
   //                     siparisSevkiyat.SevkIl = item.ShippingAddress.StateProvince.Name;
   //                     siparisSevkiyat.SevkIlce = item.ShippingAddress.City;
   //                     siparisSevkiyat.SevkiyatEposta = item.ShippingAddress.Email;
   //                     siparisSevkiyat.SevkPostakodu = item.ShippingAddress.ZipPostalCode;
   //                     siparisSevkiyat.SevkSoyadi = item.ShippingAddress.LastName;
   //                     siparisSevkiyat.SevkTel1 = item.ShippingAddress.PhoneNumber;
   //                     siparisSevkiyat.SevkUlke = "Türkiye";
   //
   //                     if (item.ShippingAddress.IsEnterprice)
   //                     {
   //
   //                         siparisSevkiyat.SevkUnvan = item.ShippingAddress.Company;
   //
   //                     }
   //                     else
   //                     {
   //                         siparisSevkiyat.SevkUnvan = item.ShippingAddress.FirstName + " " + item.ShippingAddress.LastName;
   //                     }
   //
   //                     HashSet<SiparisUrunBilgileri> siparisUrunBilgileri = new HashSet<SiparisUrunBilgileri>();
   //
   //
   //                     foreach (var cartItem in item.OrderItems)
   //                     {
   //                         var price = cartItem.UnitPriceInclTax;
   //                         var kdv = price * 0.08m;
   //                         var priceWithoutKdv = price - kdv;
   //
   //                         SiparisUrunBilgileri urunBilgisi = new SiparisUrunBilgileri();
   //                         urunBilgisi.Birim = "Adet";
   //                         urunBilgisi.Fiyat = Convert.ToDouble(priceWithoutKdv);
   //                         urunBilgisi.Kdv = Convert.ToDouble(kdv);
   //                         urunBilgisi.Kdvharictutar = Convert.ToDouble(priceWithoutKdv * cartItem.Quantity);
   //                         urunBilgisi.Miktar = cartItem.Quantity;
   //                         urunBilgisi.UrunAdi = cartItem.Product.Name;
   //                         urunBilgisi.UrunKodu = cartItem.Product.Sku;
   //                         urunBilgisi.Tutar = Convert.ToDouble(price);
   //
   //                         siparisUrunBilgileri.Add(urunBilgisi);
   //                     }
   //
   //
   //
   //                     order.SiparisData = siparisData;
   //                     order.SiparisSevkiyat = siparisSevkiyat;
   //                     order.SiparisUrunBilgileri = siparisUrunBilgileri;
   //                     Orders.Add(order);
   //
   //
   //                     item.AuthorizationTransactionCode = "InvoiceId";
   //                    ////kjh //_orderService.UpdateOrder(item);
   //                 }
   //
   //             data.Data = Orders;
   //                 _logger.Error("OrderInfoRequested | Json returned for Orders: " + Orders.First().SiparisData.SiparisId.ToString() + " - " + Orders.Last().SiparisData.SiparisId.ToString());
   //                                                    
   //             return Ok(data);//new JsonResult(data);
   //
   //         }
   //     [HttpPost("withid{orderid}/{username}/{password}/{ordercount}")]
   //     public IActionResult GetOrderFromId(string orderid, string username, string password, string ordercount)
   //     {
   //         string nameShould = _settingService.GetSettingByKey<string>("faturalamaservisi.username");
   //         string passShould = _settingService.GetSettingByKey<string>("faturalamaservisi.password");
   //         int _ordercount = 1;
   //         int _orderId = 1;
   //
   //
   //         if (!(username == nameShould && password == passShould))
   //         {
   //             _logger.Error("OrderInfoRequested | username or password is not valid");
   //
   //             string errorString = "OrderInfoRequested | username or password is not valid";
   //
   //             return StatusCode(404, errorString); //NotFound(errorString);//  new JsonResult(errorString);
   //         }
   //         _logger.Error("OrderInfoRequested | engine started");
   //
   //         //var DateFrom = null;// DateTime.UtcNow.AddDays(-1);
   //         //var DateTo = null;//DateTime.UtcNow;
   //         bool shouldContinue = Int32.TryParse(ordercount, out _ordercount);
   //
   //         if (!(shouldContinue))
   //         {
   //             _logger.Error("OrderInfoRequested | 'ordercount' is not valid");
   //
   //             string errorString = "OrderInfoRequested | 'ordercount' is not valid";
   //
   //             return StatusCode(404, errorString); //NotFound(errorString);//  new JsonResult(errorString);
   //         }
   //
   //         bool shouldContinueWithId = Int32.TryParse(orderid, out _orderId);
   //         if (!(shouldContinueWithId))
   //         {
   //             _logger.Error("OrderInfoRequested | '_orderId' is not valid");
   //
   //             string errorString = "OrderInfoRequested | 'orderid' is not valid";
   //
   //             return StatusCode(404, errorString); //NotFound(errorString);//  new JsonResult(errorString);
   //         }
   //         //         var _order = _orderService.GetOrderByCustomOrderNumber(orderid);
   //
   //         //       while (_order == null)
   //         //     {
   //         //       _orderid++;
   //         //
   //         //       _order = _orderService.GetOrderByCustomOrderNumber(_orderid.ToString());
   //         //
   //         //   }
   //
   //         var _orders = _orderService.SearchOrders(0, 0, 0, 0, 0, 0, 0, null, null, null, null, null, null, null, null, string.Empty, null, 0, 999999999, false);
   //
   //         InvoiceDataModel data = new InvoiceDataModel();
   //         HashSet<Datum> Orders = new HashSet<Datum>();
   //         if (_orders == null) //&& shouldContinue)
   //         {
   //
   //             string errorString = "OrderInfoRequested | There is no new order to send.";
   //
   //             return StatusCode(400, errorString);
   //
   //         }
   //         var selectedOrders = _orders.Where(x => x.ShippingStatus != ShippingStatus.Delivered&& Int32.Parse(x.CustomOrderNumber)>_orderId).OrderBy(x => x.CreatedOnUtc).Take(_ordercount).ToList();
   //
   //
   //
   //         if (!(selectedOrders.Count() > 0)) //&& shouldContinue)
   //         {
   //
   //             string errorString = "OrderInfoRequested | There is no new order to send.";
   //
   //             return StatusCode(400, errorString);
   //
   //         }
   //         // var _ordersData = _orders.Where(x => x.Id > _orderid);
   //         //_orders.Reverse();
   //
   //         foreach (var item in selectedOrders)
   //         {
   //
   //             Datum order = new Datum();
   //
   //             SiparisData siparisData = new SiparisData();
   //
   //             siparisData.AdresId = item.BillingAddress.Id;
   //             siparisData.Adres1 = item.BillingAddress.Address1;
   //             siparisData.Adres2 = item.BillingAddress.Address2;
   //             siparisData.Eposta = item.BillingAddress.Email;
   //             siparisData.Il = item.BillingAddress.StateProvince.Name;
   //             siparisData.Ilce = item.BillingAddress.City;
   //             siparisData.Kargo = Convert.ToDouble(item.OrderShippingInclTax);
   //             siparisData.MusteriAdi = item.BillingAddress.FirstName;
   //             siparisData.MusteriId = item.CustomerId;
   //             siparisData.MusteriSoyadi = item.BillingAddress.LastName;
   //
   //             if (item.BillingAddress.IsEnterprice)
   //
   //             {
   //
   //                 siparisData.MusteriTipi = "Tüzel";
   //                 siparisData.MusteriUnvani = item.BillingAddress.Company;
   //                 siparisData.Tckn = "";
   //                 siparisData.VergiDairesi = item.BillingAddress.VD;
   //                 siparisData.VergiNo = item.BillingAddress.VKN;
   //             }
   //             else
   //             {
   //                 siparisData.MusteriTipi = "Şahıs";
   //                 siparisData.MusteriUnvani = item.BillingAddress.FirstName + " " + item.BillingAddress.LastName;
   //                 siparisData.Tckn = item.BillingAddress.TCKN;
   //                 siparisData.VergiDairesi = "";
   //                 siparisData.VergiNo = "";
   //             }
   //             siparisData.PostaKodu = item.BillingAddress.ZipPostalCode;
   //             siparisData.SiparisId = item.Id;
   //             siparisData.SiparisTarihi = Convert.ToDateTime(item.PaidDateUtc);
   //             siparisData.Tel1 = item.BillingAddress.PhoneNumber;
   //
   //             siparisData.Ulke = "Türkiye";
   //
   //             SiparisSevkiyat siparisSevkiyat = new SiparisSevkiyat();
   //
   //             siparisSevkiyat.AdresId = item.ShippingAddress.Id;
   //             siparisSevkiyat.SevkAdi = item.ShippingAddress.FirstName;
   //             siparisSevkiyat.SevkAdres1 = item.ShippingAddress.Address1;
   //             siparisSevkiyat.SevkAdres2 = item.ShippingAddress.Address2;
   //             siparisSevkiyat.SevkIl = item.ShippingAddress.StateProvince.Name;
   //             siparisSevkiyat.SevkIlce = item.ShippingAddress.City;
   //             siparisSevkiyat.SevkiyatEposta = item.ShippingAddress.Email;
   //             siparisSevkiyat.SevkPostakodu = item.ShippingAddress.ZipPostalCode;
   //             siparisSevkiyat.SevkSoyadi = item.ShippingAddress.LastName;
   //             siparisSevkiyat.SevkTel1 = item.ShippingAddress.PhoneNumber;
   //             siparisSevkiyat.SevkUlke = "Türkiye";
   //
   //             if (item.ShippingAddress.IsEnterprice)
   //             {
   //
   //                 siparisSevkiyat.SevkUnvan = item.ShippingAddress.Company;
   //
   //             }
   //             else
   //             {
   //                 siparisSevkiyat.SevkUnvan = item.ShippingAddress.FirstName + " " + item.ShippingAddress.LastName;
   //             }
   //
   //             HashSet<SiparisUrunBilgileri> siparisUrunBilgileri = new HashSet<SiparisUrunBilgileri>();
   //
   //
   //             foreach (var cartItem in item.OrderItems)
   //             {
   //                 var price = cartItem.UnitPriceInclTax;
   //                 var kdv = price * 0.08m;
   //                 var priceWithoutKdv = price - kdv;
   //
   //                 SiparisUrunBilgileri urunBilgisi = new SiparisUrunBilgileri();
   //                 urunBilgisi.Birim = "Adet";
   //                 urunBilgisi.Fiyat = Convert.ToDouble(priceWithoutKdv);
   //                 urunBilgisi.Kdv = Convert.ToDouble(kdv);
   //                 urunBilgisi.Kdvharictutar = Convert.ToDouble(priceWithoutKdv * cartItem.Quantity);
   //                 urunBilgisi.Miktar = cartItem.Quantity;
   //                 urunBilgisi.UrunAdi = cartItem.Product.Name;
   //                 urunBilgisi.UrunKodu = cartItem.Product.Sku;
   //                 urunBilgisi.Tutar = Convert.ToDouble(price);
   //
   //                 siparisUrunBilgileri.Add(urunBilgisi);
   //             }
   //
   //
   //
   //             order.SiparisData = siparisData;
   //             order.SiparisSevkiyat = siparisSevkiyat;
   //             order.SiparisUrunBilgileri = siparisUrunBilgileri;
   //             Orders.Add(order);
   //
   //
   //             //      item.AuthorizationTransactionCode = "InvoiceId";
   //             ////kjh //_orderService.UpdateOrder(item);
   //         }
   //
   //         data.Data = Orders;
   //         _logger.Error("OrderInfoRequested | Json returned for Orders: " + Orders.First().SiparisData.SiparisId.ToString() + " - " + Orders.Last().SiparisData.SiparisId.ToString());
   //
   //         return Ok(data);//new JsonResult(data);
   //
   //     }
   //
   //
   //     [HttpPut("{username}/{password}/")]
   //     [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
   //     [ProducesResponseType(StatusCodes.Status404NotFound)]
   //     public IActionResult PutInvoiceSeries(string username ,string password ,[FromBody] InvoiceJsonList jsonBody)
   //     {
   //         string nameShould = _settingService.GetSettingByKey<string>("faturalamaservisi.username");
   //         string passShould = _settingService.GetSettingByKey<string>("faturalamaservisi.password");
   //
   //         if (!(username == nameShould && password == passShould))
   //         {
   //             _logger.Error("OrderInfoRequested | username or password is not correct");
   //
   //             string errorString = "OrderInfoRequested | username or password is not correct";
   //
   //             return StatusCode(404, errorString); //NotFound(errorString);//  new JsonResult(errorString);
   //         }
   //         InvoiceJsonList invoiceJsonList = new InvoiceJsonList();
   //         invoiceJsonList.data = jsonBody.data;
   //         InvoiceJsonListError errorJsonList = new InvoiceJsonListError();
   //         List<InvoiceJsonError> newjsonList = new List<InvoiceJsonError>();
   //
   //
   //         if (jsonBody == null)
   //         {
   //             string errorString = "OrderInfoRequested | jsonbody is null.";
   //             return StatusCode(400, errorString);
   //         }
   //
   //
   //
   //         foreach (var item in invoiceJsonList.data)
   //         {
   //
   //                 var order =  _orderService.GetOrderByCustomOrderNumber(item.SIPARIS_ID);
   //
   //             if(order==null)
   //             {
   //                 InvoiceJsonError error = new InvoiceJsonError()
   //                 {
   //                     FATURA_ID = item.FATURA_ID,
   //                     SIPARIS_ID = item.SIPARIS_ID,
   //                     ErrorMessage = item.SIPARIS_ID + " Id'sine ait bir sipariş bulunamadı."
   //
   //                 };
   //                 newjsonList.Add(error);
   //             }
   //             else
   //             {
   //
   //                 if (order.AuthorizationTransactionCode == "InvoiceId")
   //                 {
   //
   //                     Order neworder = order;
   //                     neworder.AuthorizationTransactionCode = item.FATURA_ID;
   //
   //
   //                       _orderService.UpdateOrder(neworder);
   //
   //
   //                 }
   //                 else
   //                 {
   //
   //                     if (order.AuthorizationTransactionCode == null)
   //                     {
   //
   //                         InvoiceJsonError error = new InvoiceJsonError()
   //                         {
   //                             FATURA_ID = item.FATURA_ID,
   //                             SIPARIS_ID = item.SIPARIS_ID,
   //                             ErrorMessage = item.SIPARIS_ID+" Id'li siparişin bilgileri sitemden çekilmemiş olarak görünüyor. İlgili SIPARIS_ID sine ait bilgilerin POST request gönderilerek çekilip çekilmediğini kontrol ediniz. "// "Fatura numarası üretilmeden önce"+item.SIPARIS_ID + " id li siparişin bilgisi sistemden alınmamış ola sonra üretilen idsi tarfımıza iletilmelidir."
   //
   //
   //
   //                         };
   //                         newjsonList.Add(error);
   //                     }
   //                     else
   //                     {
   //                         InvoiceJsonError error = new InvoiceJsonError()
   //                         {
   //                             FATURA_ID = item.FATURA_ID,
   //                             SIPARIS_ID = item.SIPARIS_ID,
   //                             ErrorMessage = item.SIPARIS_ID + " Id'li siparişin sistemde kayıtlı fatura bilgisi bulunmaktadır. Sistem yetkilisiyle irtibata geçiniz."
   //
   //
   //
   //                         };
   //                         newjsonList.Add(error);
   //                     }
   //                 }
   //             }
   //         }
   //         errorJsonList.data = newjsonList;
   //         return Ok(errorJsonList);
   //     }
    }

 
   
    public class InvoiceJsonError
    {
        public string SIPARIS_ID { get; set; }
        public string FATURA_ID { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class InvoiceJsonListError
    {
        public List<InvoiceJsonError> data { get; set; }
    }

    public class InvoiceJson
    {
        public string SIPARIS_ID { get; set; }
        public string FATURA_ID { get; set; }
    }
    public class InvoiceJsonList
    {
        public List<InvoiceJson> data { get; set; }
    }
}
