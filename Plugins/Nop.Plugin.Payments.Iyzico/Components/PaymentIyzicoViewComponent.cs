﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using Armut.Iyzipay.Model;
using Armut.Iyzipay.Request;
using Armut.Iyzipay;
using Nop.Core.Domain.Customers;
using Nop.Core;
using Nop.Services.Stores;
using Nop.Services.Configuration;
using Nop.Services.Orders;
using Microsoft.AspNetCore.Http;
using Nop.Services.Common;
using System.Diagnostics.Contracts;
using Nop.Plugin.Payments.Iyzico.Models;
using System;
using Nop.Services.Payments;
using System.Linq;
using Nop.Core.Domain.Orders;
using System.Collections.Generic;
using Nop.Core.Http.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Services.Discounts;
using System.Globalization;
using Nop.Web.Models.ShoppingCart;
using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;
using Nop.Web.Factories;

namespace Nop.Plugin.Payments.Iyzico.Components
{
    [ViewComponent(Name = "PaymentIyzico")]
    public class PaymentIyzicoViewComponent : NopViewComponent
    {

        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly ISettingService _settingService;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        //private readonly HttpContext _httpContext;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        //private readonly IHttpContextAccessor _httpContextAccessor;
        public PaymentIyzicoViewComponent(IWorkContext workContext,
                                       IStoreService storeService,
                                       ISettingService settingService,
                                  // HttpContext _httpContext,
                                          IWebHelper webHelper,
             IShoppingCartModelFactory shoppingCartModelFactory,
                                       IOrderService orderService,
                                       IOrderTotalCalculationService orderTotalCalculationService,
                                       IStoreContext storeContext,
                                  
                                       IGenericAttributeService genericAttributeService
                                       )
        {
            this._workContext = workContext;
            this._storeService = storeService;
            this._settingService = settingService;
           // this._httpContext = HttpContext;
            this._webHelper = webHelper;
           // this._httpContextAccessor = httpContextAccessor;
            this._orderService = orderService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._storeContext = storeContext;
            this._shoppingCartModelFactory = shoppingCartModelFactory;






            this._genericAttributeService = genericAttributeService;
        }

        public IViewComponentResult Invoke()
        {
            Contract.Ensures(Contract.Result<ActionResult>() != null);
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var model = new PaymentInfoModel();

            var customer = _workContext.CurrentCustomer;
            
            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(0);
            var dateTimeNowTrim = DateTime.Today.ToString().Replace(" ", "-").Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty);
            var conversationId = customer.Id + "-" + dateTimeNowTrim + "-" + customer.CustomerGuid;
            var basketId = customer.CustomerGuid + "_" + dateTimeNowTrim;
            //ProcessPaymentRequest processPaymentRequest = _httpContextAccessor.HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo"); //_httpContext.Session["OrderPaymentInfo"] as ProcessPaymentRequest;
            //if (processPaymentRequest == null) processPaymentRequest = new ProcessPaymentRequest();
            
            var cart = customer.ShoppingCartItems
             .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
             .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
             .ToList();
       

            //IList<ShoppingCartItem> cart = new IList<ShoppingCartItem>();
            //foreach (var item in cartbasic)
            //{
            //    for (int i = 0; i < item.Quantity; i++)
            //    {
            //        cart.Add(item);
            //    }
            //}
            decimal total ;
            var shoppingCartTotalBase = (decimal)_orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderTotalDiscountAmountBase, out List<DiscountForCaching> _, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);
           _orderTotalCalculationService.GetShoppingCartSubTotal(cart, false,
                out var discountAmount, out var appliedDiscounts,
                out var subTotalWithoutDiscount, out var subTotalWithDiscount, out var taxRates);
            total = shoppingCartTotalBase;
            decimal shippingtotal;
            
                shippingtotal = (decimal)_orderTotalCalculationService.GetShoppingCartShippingTotal(cart,true);
            var cartTotal =total - shippingtotal;

            // var orderTotal = (int)(paymentRequest.OrderTotal * 100);
            //  var amountMoney = new Iyzico.Models.Money(Amount: orderTotal, Currency: moneyCurrency);

            
            CreateCheckoutFormInitializeRequest request = new CreateCheckoutFormInitializeRequest();

            request.Locale = Locale.TR.ToString();// _workContext.WorkingLanguage.Name.ToString();
            request.ConversationId = conversationId;
            request.Price = total.ToString("0.0000").Replace(",", ".");//cartTotal .ToString("#.######").Replace(",", ".");
            request.PaidPrice = total.ToString("0.0000").Replace(",", ".");
            request.Currency = "TRY";
            request.BasketId = basketId;
            bool sslSettingsForCallback = _storeContext.CurrentStore.SslEnabled ? true : false;
            request.CallbackUrl = _webHelper.GetStoreLocation(sslSettingsForCallback) + "OdemeOnay";

            List<int> enabledInstallments = new List<int>();

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////IMPORTANT BREAK-POINT FOR INSTALMENTS////////////////////////////////////////////////////////////////////
            ///////////IMPORTANT BREAK-POINT FOR INSTALMENTS////////////////////////////////////////////////////////////////////
            ///////////IMPORTANT BREAK-POINT FOR INSTALMENTS////////////////////////////////////////////////////////////////////
            ///////////IMPORTANT BREAK-POINT FOR INSTALMENTS////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            enabledInstallments.Add(1);
            bool shouldEnableInstalments = false;

            foreach (var item in cart)
            {
                if (!item.Product.IsTelecommunicationsOrBroadcastingOrElectronicServices)
                {
                    shouldEnableInstalments = false;
                    break;


                }
                else
                {
                    shouldEnableInstalments = true;
                }
            }
            if (shouldEnableInstalments)
            {
                enabledInstallments.Add(2);
                enabledInstallments.Add(3);
                enabledInstallments.Add(6);
                enabledInstallments.Add(9);
                enabledInstallments.Add(12);
            }
          
            request.EnabledInstallments = enabledInstallments;

            Buyer buyer = new Buyer();
            buyer.Id = customer.Id.ToString();
            buyer.Name = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute); //_genericAttributeService.GetAttribute<string>(SystemCustomerAttributeNames.FirstName); //customer.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
            buyer.Surname = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute); //customer.GetAttribute<string>(Core.Domain.Customers.SystemCustomerAttributeNames.LastName);
            buyer.GsmNumber = customer.BillingAddress.PhoneNumber;
            buyer.Email = customer.BillingAddress.Email;

            if (customer.BillingAddress.IsEnterprice == true)
            {
                buyer.IdentityNumber = customer.BillingAddress.VKN;
            }
            else
            {
                buyer.IdentityNumber = customer.BillingAddress.TCKN;
            }
            
            buyer.RegistrationAddress = customer.BillingAddress.Address1 + " " + customer.BillingAddress.Address2 +" "+customer.BillingAddress.City;
            buyer.Ip = customer.LastIpAddress;
            buyer.City = customer.BillingAddress.StateProvince.Name;
            buyer.Country = customer.BillingAddress.Country.Name;
            
            request.Buyer = buyer;

            Address shippingAddress = new Address();
            shippingAddress.ContactName = customer.ShippingAddress.FirstName + " " + customer.ShippingAddress.LastName;
            shippingAddress.City = customer.ShippingAddress.StateProvince.Name;
            shippingAddress.Country = customer.ShippingAddress.Country.Name;
            shippingAddress.Description = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2 + " " + customer.BillingAddress.City;
            request.ShippingAddress = shippingAddress;

            Address billingAddress = new Address();
            billingAddress.ContactName = customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName;
            billingAddress.City = customer.BillingAddress.StateProvince.Name;
            billingAddress.Country = customer.BillingAddress.Country.Name;
            billingAddress.Description = customer.BillingAddress.Address1 + " " + customer.BillingAddress.Address2 + " " + customer.BillingAddress.City;
            request.BillingAddress = billingAddress;

            if (String.IsNullOrWhiteSpace(buyer.Name) || String.IsNullOrWhiteSpace(buyer.Surname)) /* müşteri ziyaretçi ise */
            {
                buyer.Name = customer.BillingAddress.FirstName;
                buyer.Surname = customer.BillingAddress.LastName;
            }

            if (String.IsNullOrWhiteSpace(billingAddress.ContactName)) /* müşteri ziyaretçi ise */
            {
                billingAddress.ContactName = customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName;
            }

            if (String.IsNullOrWhiteSpace(shippingAddress.ContactName)) /* müşteri ziyaretçi ise */
            {
                shippingAddress.ContactName = customer.ShippingAddress.FirstName + " " + customer.ShippingAddress.LastName;
            }

            List<BasketItem> basketItems = new List<BasketItem>();
            //
            //      for (int i = 0; i < cart.Count; i++)
            //      {
            //
            //          decimal discounttotal = 0.00m;
            //
            //          List<decimal> allDiscounts = new List<decimal>();
            //
            //          List<decimal> cumulativeDiscounts = new List<decimal>();
            //          List<decimal> notCumulativeDiscounts = new List<decimal>();
            //          List<dynamic> allTheSingleDiscounts = new List<dynamic>();
            //          List<decimal> allTheSingleDiscountAmounts = new List<decimal>();
            //
            //          int alldiscounts = cart[i].Product.DiscountProductMappings.Count;
            //          int cumulativeQuantity = 0;
            //          int notCumulativeQuantity = 0;
            //
            //          if (alldiscounts >= 1)
            //          {
            //
            //              foreach (var item in cart[i].Product.DiscountProductMappings)
            //              {
            //                  bool iscumulative = (bool)item.Discount.IsCumulative;
            //                  if (iscumulative == true)
            //                  {
            //                      cumulativeQuantity++;
            //                      cumulativeDiscounts.Add(item.Discount.DiscountAmount);
            //                  }
            //                  else
            //                  {
            //                      notCumulativeQuantity++;
            //                      notCumulativeDiscounts.Add(item.Discount.DiscountAmount);
            //                  }
            //
            //                  allTheSingleDiscounts.Add(item.Discount);
            //                  allTheSingleDiscountAmounts.Add(item.Discount.DiscountAmount);
            //              }
            //
            //          }
            //
            //
            //
            //          decimal totalcumulative = 0.00m;
            //          decimal max = 0.00m;
            //
            //          if (cumulativeDiscounts.Count > 0)  totalcumulative = cumulativeDiscounts.Sum(x => Convert.ToDecimal(x)); 
            //
            //          if (allTheSingleDiscountAmounts.Count > 0)  max = allTheSingleDiscountAmounts.Max(); 
            //
            //
            //
            //
            //
            //
            //          if (totalcumulative > max)
            //          {
            //
            //              discounttotal = totalcumulative;
            //
            //          }
            //          else
            //          {
            //              discounttotal = max;
            //          }
            //
            //          
            //
            //
            //
            //
            //
            //
            //
            //
            //
            //          for (int ii = 0; ii < cart[i].Quantity; ii++)
            //          {
            //
            //          var category = cart[i].Product.ProductCategories.FirstOrDefault(x => x.Category != null);
            //          BasketItem basketItem = new BasketItem();
            //          basketItem.Id = cart[i].ProductId.ToString();
            //          basketItem.Name = cart[i].Product.Name;
            //          if (category != null)
            //          {
            //
            //          basketItem.Category1 = category.Category.Name;
            //          }
            //          else
            //          {
            //              basketItem.Category1 = "--";
            //          }
            //        //  basketItem.Category2 = "--";
            //          basketItem.ItemType = BasketItemType.PHYSICAL.ToString();
            //          if (cart[i].Product.HasTierPrices ==true)
            //          {
            //
            //
            //
            //
            //
            //
            //                  bool customerRoleAccurate = false;
            //
            //                  List<int> tierQuantities = new List<int>();
            //                  foreach (var item in cart[i].Product.TierPrices)
            //                  {
            //                     
            //                      int quantity = item.Quantity;
            //                      tierQuantities.Add(quantity);
            //                  }
            //                  int indexOfTier = -1;
            //                  tierQuantities.Sort();
            //                 
            //                  for (int z = 0; z < tierQuantities.Count; z++)
            //                  {
            //                      TierPrice _tierPrice = (TierPrice)cart[i].Product.TierPrices.ElementAt(z);
            //                      if (cart[i].Quantity >= tierQuantities[z] &&
            //                          (
            //                          (_tierPrice.StartDateTimeUtc <= DateTime.Now && _tierPrice.EndDateTimeUtc>= DateTime.Now) ||
            //                          (_tierPrice.StartDateTimeUtc == null && _tierPrice.EndDateTimeUtc == null)
            //                          )
            //                          )
            //                      {
            //                          indexOfTier = z;
            //                      }
            //                      foreach (var item in customer.CustomerRoles)
            //                      {
            //                          if (_tierPrice.CustomerRole != null)
            //                          {
            //
            //                          if(item.SystemName== _tierPrice.CustomerRole.SystemName)
            //                          {
            //                              customerRoleAccurate = true;
            //                          }
            //                          }
            //                          else
            //                          {
            //                              customerRoleAccurate = true;
            //                          }
            //                             
            //                      }
            //                      
            //                  }
            //
            //
            //                
            //
            //                            
            //                  if (indexOfTier >= 0&& customerRoleAccurate)
            //                  {
            //
            //                  TierPrice tierPrice = (TierPrice)cart[i].Product.TierPrices.ElementAt(indexOfTier);
            //
            //                  basketItem.Price = (tierPrice.Price-discounttotal).ToString("0.0000").Replace(",", ".");
            //                  }
            //                  else
            //                  {
            //                      basketItem.Price = (cart[i].Product.Price - discounttotal).ToString("0.0000").Replace(",", ".");
            //                  }
            //              }
            //              else
            //              {
            //                  basketItem.Price = (cart[i].Product.Price-discounttotal).ToString("0.0000").Replace(",", ".");
            //              }
            //          basketItems.Add(basketItem);
            //          }
            //
            //
            //      }

            ///////////////////////////////////////////////
            // Create Products for Invoice/////////////////
            ///////////////////////////////////////////////
            //  List<BasketItem> basketItems = new List<BasketItem>();




            ShoppingCartModel _subtotals = GetSubTotals(customer);

            List<ShoppingCartItemModel> _paidCartItems = _subtotals.Items as List<ShoppingCartItemModel>;





            for (int i = 0; i < cart.Count; i++)
            {
                decimal discounttotal = 0.00m;





                //List<decimal> allDiscounts = new List<decimal>();

                //List<decimal> cumulativeDiscounts = new List<decimal>();
                //List<decimal> notCumulativeDiscounts = new List<decimal>();
                //List<dynamic> allTheSingleDiscounts = new List<dynamic>();
                //List<decimal> allTheSingleDiscountAmounts = new List<decimal>();

                //int alldiscounts = cart[i].Product.DiscountProductMappings.Count;
                //int cumulativeQuantity = 0;
                //int notCumulativeQuantity = 0;

                //if (alldiscounts >= 1)
                //{

                //    foreach (var item in cart[i].Product.DiscountProductMappings)
                //    {
                //        bool iscumulative = (bool)item.Discount.IsCumulative;
                //        if (iscumulative == true)
                //        {
                //            cumulativeQuantity++;
                //            cumulativeDiscounts.Add(item.Discount.DiscountAmount);
                //        }
                //        else
                //        {
                //            notCumulativeQuantity++;
                //            notCumulativeDiscounts.Add(item.Discount.DiscountAmount);
                //        }

                //        allTheSingleDiscounts.Add(item.Discount);
                //        allTheSingleDiscountAmounts.Add(item.Discount.DiscountAmount);
                //    }

                //}



                //decimal totalcumulative = 0.00m;
                //decimal max = 0.00m;

                //if (cumulativeDiscounts.Count > 0) totalcumulative = cumulativeDiscounts.Sum(x => Convert.ToDecimal(x));

                //if (allTheSingleDiscountAmounts.Count > 0) max = allTheSingleDiscountAmounts.Max();






                //if (totalcumulative > max)
                //{

                //    discounttotal = totalcumulative;

                //}
                //else
                //{
                //    discounttotal = max;
                //}





                for (int ii = 0; ii < cart[i].Quantity; ii++)
                {
                    var category = cart[i].Product.ProductCategories.FirstOrDefault(x => x.Category != null);

                    
                              BasketItem basketItem = new BasketItem();
                              basketItem.Id = cart[i].ProductId.ToString();
                              basketItem.Name = cart[i].Product.Name;
                              if (category != null)
                              {
                    
                              basketItem.Category1 = category.Category.Name;
                              }
                              else
                              {
                                  basketItem.Category1 = "--";
                              }
                            //  basketItem.Category2 = "--";
                              basketItem.ItemType = BasketItemType.PHYSICAL.ToString();




                  //  BasketItem basketItem = new BasketItem();
                //    basketItem.Id = cart[i].ProductId.ToString();
                //    basketItem.Name = cart[i].Product.Name;

//                    if (category != null)
//                    {
//
//                        basketItem.Category1 = category.Category.Name;
//                    }
//                    else
//                    {
//                        basketItem.Category1 = "--";
//                    }
//                    //  basketItem.Category2 = "--";
//                    basketItem.ItemType = BasketItemType.PHYSICAL.ToString();
//                    //if(false)// (cart[i].Product.HasTierPrices == true)
//                    //{

                    //    List<int> tierQuantities = new List<int>();
                    //    foreach (var item in cart[i].Product.TierPrices)
                    //    {

                    //        int quantity = item.Quantity;
                    //        tierQuantities.Add(quantity);
                    //    }
                    //    int indexOfTier = 0;
                    //    tierQuantities.Sort();
                    //    for (int z = 0; z < tierQuantities.Count; z++)
                    //    {
                    //        TierPrice _tierPrice = (TierPrice)cart[i].Product.TierPrices.ElementAt(z);
                    //        if (cart[i].Quantity >= tierQuantities[z] &&
                    //            (
                    //            (_tierPrice.StartDateTimeUtc <= DateTime.Now && _tierPrice.EndDateTimeUtc >= DateTime.Now) ||
                    //            (_tierPrice.StartDateTimeUtc == null && _tierPrice.EndDateTimeUtc == null)
                    //            )
                    //            )
                    //        {
                    //            indexOfTier = z;
                    //        }
                    //    }

                    //    TierPrice tierPrice = (TierPrice)cart[i].Product.TierPrices.ElementAt(indexOfTier);

                    //    basketItem.Price = (tierPrice.Price- discounttotal).ToString("#.#########").Replace(",", ".");


                    //    string _____price = _paidCartItems[i].UnitPrice.Replace(".", "").Replace(",", ".");



                    //   string _unitPrice = new String(_____price.Where(c => char.IsDigit(c) || c == (char)46).ToArray());







                    //    basketItem.SubMerchantPrice = _unitPrice;

                    ////    string asd = new String(_____price.Where(Char.IsPunctuation(',')).ToArray());
                    //}
                    //else
                    //{
                    //basketItem.SubMerchantPrice = (cart[i].Product.Price - discounttotal).ToString("#.#########").Replace(",", ".");
                    // string _____price = _paidCartItems[i].UnitPrice;
                    // decimal _unitPrice = Convert.ToDecimal(_____price.Replace("₺", "").Replace(",", "."));
                    // basketItem.SubMerchantPrice = _unitPrice.ToString("#.#########").Replace(",", ".");

                    string _____price = _paidCartItems[i].UnitPrice.Replace(",", "."); //.Replace(" tl", "").Replace(",", ".").Replace(" Tl", "").Replace(" TL", "").Replace(" ", "");
                    string _unitPrice = new String(_____price.Where(c => char.IsDigit(c) || c == (char)46).ToArray());
                    basketItem.Price = _unitPrice;
                    //string _____price = _paidCartItems[i].UnitPrice.Replace(".", "").Replace(",", ".");
                    //decimal _unitPrice = decimal.Parse(_____price, NumberStyles.Currency);
                    //basketItem.SubMerchantPrice = _unitPrice.ToString();
                //    basketItem.SubMerchantPrice = basketItem.Price;
                    basketItems.Add(basketItem);

                }

            }






            if (shippingtotal > 0)
            {

            BasketItem shippingItem = new BasketItem();
            shippingItem.Price = shippingtotal.ToString("0.0000").Replace(",", ".");
                shippingItem.Name = "Kargo Ücreti";
            shippingItem.Id = "0";
                shippingItem.ItemType = BasketItemType.VIRTUAL.ToString();
                shippingItem.Category1 = "Kargo";
                basketItems.Add(shippingItem);
            }
        //    foreach (var item in basketItems)
        //    {
        //        var priceOfProduct = decimal.Parse(item.Price, new NumberFormatInfo() { NumberDecimalSeparator = "." });
        //
        //        var discountedPriceOfProduct = priceOfProduct - ((priceOfProduct / (total + orderTotalDiscountAmountBase)) * orderTotalDiscountAmountBase);
        //        item.Price= discountedPriceOfProduct.ToString("0.0000").Replace(",", ".");
        //
        //    }
            
            request.BasketItems = basketItems;

            decimal cartTotalPrice = 0.00m;
            foreach (var item in basketItems)
            {
                cartTotalPrice += decimal.Parse(item.Price, new NumberFormatInfo() { NumberDecimalSeparator = "." });
            }

            var ___ordertotals = _orderTotalCalculationService.GetShoppingCartTotal(cart,false,false);
            request.Price= cartTotalPrice.ToString("0.0000").Replace(",", ".");
            Options options = new Options();
            options.ApiKey = IyzicoPaymentSettings.ClientId;
            options.BaseUrl = IyzicoPaymentSettings.UseSandbox ? IyzicoPaymentSettings.APIUrlSandbox : IyzicoPaymentSettings.APIUrl;
            options.SecretKey = IyzicoPaymentSettings.ClientSecret;
            
            CheckoutFormInitialize checkoutFormInitialize = CheckoutFormInitialize.Create(request, options);


            model.Status = checkoutFormInitialize.Status;
            model.CheckoutFormContent = "<script type = \"text/javascript\" > var iyziInit = undefined; </script> " + checkoutFormInitialize.CheckoutFormContent;
            model.ErrorMessage = checkoutFormInitialize.ErrorMessage;




            //model.CreditCardTypes = new List<SelectListItem>
            //    {
            //        new SelectListItem { Text = "Visa", Value = "visa" },
            //        new SelectListItem { Text = "Master card", Value = "MasterCard" },
                  
            //    };
            

            ////years
            //for (var i = 0; i < 15; i++)
            //{
            //    var year = (DateTime.Now.Year + i).ToString();
            //    model.ExpireYears.Add(new SelectListItem { Text = year, Value = year, });
            //}

            ////months
            //for (var i = 1; i <= 12; i++)
            //{
            //    model.ExpireMonths.Add(new SelectListItem { Text = i.ToString("D2"), Value = i.ToString(), });
            //}

            return View("~/Plugins/Payments.Iyzico/Views/PaymentInfo.cshtml", model);
        }


        public virtual ShoppingCartModel GetSubTotals(Customer customer)
        {
            var cart = customer.ShoppingCartItems
             .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
             //.LimitPerStore(_storeContext.CurrentStore.Id)
             .ToList();

            //prepare model
            var model = new ShoppingCartModel();
            model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

            return model;
        }

    }
}



