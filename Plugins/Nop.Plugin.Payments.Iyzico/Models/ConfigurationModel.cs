﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Iyzico.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }


        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ClientId")]
        public string ClientId { get; set; }
        public bool ClientId_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ClientSecret")]
        public string ClientSecret { get; set; }
        public bool ClientSecret_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.APIUrl")]
        public string APIUrl { get; set; }
        public bool APIUrl_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.APIUrlSandbox")]
        public string APIUrlSandbox { get; set; }
        public bool APIUrlSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        public int TransactModeId { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.TransactMode")]
        public SelectList TransactModeValues { get; set; }
        public bool TransactModeId_OverrideForStore { get; set; }


        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10051")]
        public string ErrorCode10051 { get; set; }
        public bool ErrorCode10051_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10005")]
        public string ErrorCode10005 { get; set; }
        public bool ErrorCode10005_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10012")]
        public string ErrorCode10012 { get; set; }
        public bool ErrorCode10012_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10041")]
        public string ErrorCode10041 { get; set; }
        public bool ErrorCode10041_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10043")]
        public string ErrorCode10043 { get; set; }
        public bool ErrorCode10043_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10054")]
        public string ErrorCode10054 { get; set; }
        public bool ErrorCode10054_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10084")]
        public string ErrorCode10084 { get; set; }
        public bool ErrorCode10084_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10057")]
        public string ErrorCode10057 { get; set; }
        public bool ErrorCode10057_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10058")]
        public string ErrorCode10058 { get; set; }
        public bool ErrorCode10058_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10034")]
        public string ErrorCode10034 { get; set; }
        public bool ErrorCode10034_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10093")]
        public string ErrorCode10093 { get; set; }
        public bool ErrorCode10093_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10201")]
        public string ErrorCode10201 { get; set; }
        public bool ErrorCode10201_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10202")]
        public string ErrorCode10202 { get; set; }
        public bool ErrorCode10202_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10203")]
        public string ErrorCode10203 { get; set; }
        public bool ErrorCode10203_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10204")]
        public string ErrorCode10204 { get; set; }
        public bool ErrorCode10204_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10205")]
        public string ErrorCode10205 { get; set; }
        public bool ErrorCode10205_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10206")]
        public string ErrorCode10206 { get; set; }
        public bool ErrorCode10206_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10207")]
        public string ErrorCode10207 { get; set; }
        public bool ErrorCode10207_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10208")]
        public string ErrorCode10208 { get; set; }
        public bool ErrorCode10208_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10209")]
        public string ErrorCode10209 { get; set; }
        public bool ErrorCode10209_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10210")]
        public string ErrorCode10210 { get; set; }
        public bool ErrorCode10210_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10211")]
        public string ErrorCode10211 { get; set; }
        public bool ErrorCode10211_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10212")]
        public string ErrorCode10212 { get; set; }
        public bool ErrorCode10212_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10213")]
        public string ErrorCode10213 { get; set; }
        public bool ErrorCode10213_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10214")]
        public string ErrorCode10214 { get; set; }
        public bool ErrorCode10214_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10215")]
        public string ErrorCode10215 { get; set; }
        public bool ErrorCode10215_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10216")]
        public string ErrorCode10216 { get; set; }
        public bool ErrorCode10216_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10217")]
        public string ErrorCode10217 { get; set; }
        public bool ErrorCode10217_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10218")]
        public string ErrorCode10218 { get; set; }
        public bool ErrorCode10218_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10219")]
        public string ErrorCode10219 { get; set; }
        public bool ErrorCode10219_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10220")]
        public string ErrorCode10220 { get; set; }
        public bool ErrorCode10220_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10221")]
        public string ErrorCode10221 { get; set; }
        public bool ErrorCode10221_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10222")]
        public string ErrorCode10222 { get; set; }
        public bool ErrorCode10222_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10223")]
        public string ErrorCode10223 { get; set; }
        public bool ErrorCode10223_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10224")]
        public string ErrorCode10224 { get; set; }
        public bool ErrorCode10224_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10225")]
        public string ErrorCode10225 { get; set; }
        public bool ErrorCode10225_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10226")]
        public string ErrorCode10226 { get; set; }
        public bool ErrorCode10226_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10227")]
        public string ErrorCode10227 { get; set; }
        public bool ErrorCode10227_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10228")]
        public string ErrorCode10228 { get; set; }
        public bool ErrorCode10228_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10229")]
        public string ErrorCode10229 { get; set; }
        public bool ErrorCode10229_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10230")]
        public string ErrorCode10230 { get; set; }
        public bool ErrorCode10230_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10231")]
        public string ErrorCode10231 { get; set; }
        public bool ErrorCode10231_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10232")]
        public string ErrorCode10232 { get; set; }
        public bool ErrorCode10232_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10233")]
        public string ErrorCode10233 { get; set; }
        public bool ErrorCode10233_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10234")]
        public string ErrorCode10234 { get; set; }
        public bool ErrorCode10234_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10235")]
        public string ErrorCode10235 { get; set; }
        public bool ErrorCode10235_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ErrorCode10236")]
        public string ErrorCode10236 { get; set; }
        public bool ErrorCode10236_OverrideForStore { get; set; }


        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutClientId")]
        public string ParasutClientId { get; set; }
        public bool ParasutClientId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutUserName")]
        public string ParasutUserName { get; set; }
        public bool ParasutUserName_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutPassword")]
        public string ParasutPassword { get; set; }
        public bool ParasutPassword_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutCompanyId")]
        public string ParasutCompanyId { get; set; }
        public bool ParasutCompanyId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutBankAccountId")]
        public string ParasutBankAccountId { get; set; }
        public bool ParasutBankAccountId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutClientSecret")]
        public string ParasutClientSecret { get; set; }
        public bool ParasutClientSecret_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutBaseApiToken")]
        public string ParasutBaseApiToken { get; set; }
        public bool ParasutBaseApiToken_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutBaseApi")]
        public string ParasutBaseApi { get; set; }
        public bool ParasutBaseApi_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.UseParasut")]
        public bool UseParasut { get; set; }
        public bool UseParasut_OverrideForStore { get; set; }




    }
}
