﻿
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Iyzico.Models
{
    public class AlertViewModel: BaseNopModel
    {

        public AlertViewModel()
        {

        }

        [NopResourceDisplayName("Payment.AlerViewModelErrorStr")]
        
        public string AlertViewModelErrorStr { get; set; }
    }
}
