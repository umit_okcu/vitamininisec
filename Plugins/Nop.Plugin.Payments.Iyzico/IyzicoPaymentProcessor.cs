﻿using Armut.Iyzipay;
using Armut.Iyzipay.Model;
using Armut.Iyzipay.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Iyzico.Controllers;
using Nop.Plugin.Payments.Iyzico.Models;
using Nop.Plugin.Payments.Iyzico.Validators;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Payments.Iyzico
{
    public class IyzicoPaymentProcessor : BasePlugin, IPaymentMethod
    {
       

        #region Fields

        private readonly ILocalizationService _localizationService;
       // private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly ISettingService _settingService;
        private readonly ICustomerService _customerService;
        private readonly IyzicoPaymentSettings _IyzicoPaymentSettings;
        private readonly IPaymentService _paymentService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IGenericAttributeService _genericAttributeService;

                    private readonly IOrderTotalCalculationService _orderTotalCalculationService;

        #endregion

        #region Ctor

        public IyzicoPaymentProcessor(ILocalizationService localizationService,
       //     IOrderTotalCalculationService orderTotalCalculationService,
            ISettingService settingService,
            ICustomerService customerService,
        IStoreContext storeContext,
                     IWorkContext workContext,
                        IOrderTotalCalculationService orderTotalCalculationService,
                                  IGenericAttributeService genericAttributeService,
           IyzicoPaymentSettings IyzicoPaymentSettings,
           IPaymentService paymentService,
           IWebHelper webHelper)
        {
            this._localizationService = localizationService;
       //     this._orderTotalCalculationService = orderTotalCalculationService;
            this._settingService = settingService;
            this._customerService = customerService;
            this._IyzicoPaymentSettings = IyzicoPaymentSettings;
            this._paymentService = paymentService;//trying
            this._genericAttributeService = genericAttributeService;
            this._webHelper= webHelper;
            this._storeContext = storeContext;
            this._workContext = workContext;
            this._orderTotalCalculationService = orderTotalCalculationService;
        }

        #endregion


        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        { 
            var result = new ProcessPaymentResult();
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;//this.GetActiveStoreScopeConfiguration(_storeService, _workContext);

            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);
            if (customer == null)
                throw new Exception("Customer cannot be loaded");

            result.AllowStoringCreditCardNumber = true;

            string token = processPaymentRequest.CustomValues["token"].ToString();

          

            RetrieveCheckoutFormRequest request = new RetrieveCheckoutFormRequest();
            
            request.Token = token;


            Options options = new Options();
            options.ApiKey = IyzicoPaymentSettings.ClientId;
            options.BaseUrl = IyzicoPaymentSettings.UseSandbox ? IyzicoPaymentSettings.APIUrlSandbox : IyzicoPaymentSettings.APIUrl;
            options.SecretKey = IyzicoPaymentSettings.ClientSecret;
            CheckoutForm checkoutForm = CheckoutForm.Retrieve(request, options);
            


            if (checkoutForm.PaymentStatus == "SUCCESS")
            {

            result.AuthorizationTransactionResult = checkoutForm.Status;
             result.AuthorizationTransactionId = checkoutForm.PaymentId;

                
                 
                result.NewPaymentStatus = PaymentStatus.Paid;

            }
            else {

                switch (_IyzicoPaymentSettings.TransactMode)
            {
                case TransactMode.Pending:
                    result.NewPaymentStatus = PaymentStatus.Pending;
                    break;
                case TransactMode.Authorize:
                    result.NewPaymentStatus = PaymentStatus.Authorized;
                    break;
                case TransactMode.AuthorizeAndCapture:
                    result.NewPaymentStatus = PaymentStatus.Paid;
                    break;
                default:
                    {
                        result.AddError("Desteklenmeyen işlem türü");
                        return result;
                    }
            }

            }








            //var dateTimeNowTrim = DateTime.Today.ToString().Replace(" ", "-").Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty);
            //var conversationId = customer.Id + "-" + dateTimeNowTrim + "-" + customer.CustomerGuid;
            //var basketId = "Cart" + "-" + customer.CustomerGuid + "-" + dateTimeNowTrim;


            //var cartbasic = customer.ShoppingCartItems
            //.Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
            //.Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
            //.ToList();
            //List<ShoppingCartItem> cart = new List<ShoppingCartItem>();
            //foreach (var item in cartbasic)
            //{
            //    for (int i = 0; i < item.Quantity; i++)
            //    {
            //        cart.Add(item);
            //    }
            //}

            //decimal total = 0;
            //total = (decimal)_orderTotalCalculationService.GetShoppingCartTotal(cartbasic, true, true);

            //CreatePaymentRequest request = new CreatePaymentRequest();
            //request.Locale = Locale.TR.ToString();
            //request.ConversationId = processPaymentRequest.CustomerId.ToString();
            //request.Price = total.ToString("#.##").Replace(",", "."); //processPaymentRequest.OrderTotal.ToString("N", new CultureInfo("tr-TR"));
            //request.PaidPrice = total.ToString("#.##").Replace(",", ".");
            //request.Currency = _workContext.WorkingCurrency.CurrencyCode.ToString(); //Currency.TRY.ToString();
            //request.Installment = 1;
            //request.BasketId = basketId;
            //request.PaymentChannel = PaymentChannel.WEB.ToString();
            //request.PaymentGroup = PaymentGroup.PRODUCT.ToString();


            //PaymentCard paymentCard = new PaymentCard();
            //paymentCard.CardHolderName = processPaymentRequest.CreditCardName;
            //paymentCard.CardNumber = processPaymentRequest.CreditCardNumber;
            //paymentCard.ExpireMonth = processPaymentRequest.CreditCardExpireMonth < 10 ? "0" + processPaymentRequest.CreditCardExpireMonth.ToString() : "" + processPaymentRequest.CreditCardExpireMonth.ToString();
            //paymentCard.ExpireYear = processPaymentRequest.CreditCardExpireYear.ToString();
            //paymentCard.Cvc = processPaymentRequest.CreditCardCvv2;
            //paymentCard.RegisterCard = 0;
            //request.PaymentCard = paymentCard;

            //Buyer buyer = new Buyer();
            //buyer.Id = customer.Id.ToString();
            //buyer.Name = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.FirstNameAttribute); //_genericAttributeService.GetAttribute<string>(SystemCustomerAttributeNames.FirstName); //customer.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
            //buyer.Surname = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.LastNameAttribute); //customer.GetAttribute<string>(Core.Domain.Customers.SystemCustomerAttributeNames.LastName);
            //buyer.GsmNumber = customer.BillingAddress.PhoneNumber;
            //buyer.Email = customer.BillingAddress.Email;
            //buyer.IdentityNumber = customer.CustomerGuid.ToString();
            //buyer.RegistrationAddress = customer.BillingAddress.Address1 + " " + customer.BillingAddress.Address2;
            //buyer.Ip = customer.LastIpAddress;
            //buyer.City = customer.BillingAddress.City;
            //buyer.Country = customer.BillingAddress.Country.Name;
            //request.Buyer = buyer;

            //Address shippingAddress = new Address();
            //shippingAddress.ContactName = customer.ShippingAddress.FirstName + " " + customer.ShippingAddress.LastName;
            //shippingAddress.City = customer.ShippingAddress.City;
            //shippingAddress.Country = customer.ShippingAddress.Country.Name;
            //shippingAddress.Description = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;
            //request.ShippingAddress = shippingAddress;

            //Address billingAddress = new Address();
            //billingAddress.ContactName = customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName;
            //billingAddress.City = customer.BillingAddress.City;
            //billingAddress.Country = customer.BillingAddress.Country.Name;
            //billingAddress.Description = customer.BillingAddress.Address1 + " " + customer.BillingAddress.Address2;
            //request.BillingAddress = billingAddress;

            //if (String.IsNullOrWhiteSpace(buyer.Name) || String.IsNullOrWhiteSpace(buyer.Surname)) /* müşteri ziyaretçi ise */
            //{
            //    buyer.Name = customer.BillingAddress.FirstName;
            //    buyer.Surname = customer.BillingAddress.LastName;
            //}

            //if (String.IsNullOrWhiteSpace(billingAddress.ContactName)) /* müşteri ziyaretçi ise */
            //{
            //    billingAddress.ContactName = customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName;
            //}

            //if (String.IsNullOrWhiteSpace(shippingAddress.ContactName)) /* müşteri ziyaretçi ise */
            //{
            //    shippingAddress.ContactName = customer.ShippingAddress.FirstName + " " + customer.ShippingAddress.LastName;
            //}

            //List<BasketItem> basketItems = new List<BasketItem>();

            //for (int i = 0; i < cart.Count; i++)
            //{
            //    BasketItem basketItem = new BasketItem();
            //    basketItem.Id = cart[i].ProductId.ToString();
            //    basketItem.Name = cart[i].Product.Name;
            //    basketItem.Category1 = cart[i].Product.ProductCategories.FirstOrDefault(x => x.Category != null).Category.Name;
            //    basketItem.Category2 = "--";
            //    basketItem.ItemType = BasketItemType.PHYSICAL.ToString();
            //    basketItem.Price = cart[i].Product.Price.ToString("#.##").Replace(",", ".");
            //    basketItems.Add(basketItem);

            //}

            //request.BasketItems = basketItems;

            //Options options = new Options();
            //options.ApiKey = IyzicoPaymentSettings.ClientId;
            //options.BaseUrl = IyzicoPaymentSettings.UseSandbox ? IyzicoPaymentSettings.APIUrlSandbox : IyzicoPaymentSettings.APIUrl;
            //options.SecretKey = IyzicoPaymentSettings.ClientSecret;

            //Payment payment = Payment.Create(request, options);


            //if (payment.Status == Status.SUCCESS.ToString())
            //{
            //    result.AuthorizationTransactionResult = payment.Status;
            //    result.AuthorizationTransactionId = payment.PaymentId;
            //}
            //else
            //{

            //    if (payment.ErrorCode == "10051" && this._IyzicoPaymentSettings.ErrorCode10051 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10051);
            //    else if (payment.ErrorCode == "10005" && this._IyzicoPaymentSettings.ErrorCode10005 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10005);
            //    else if (payment.ErrorCode == "10012" && this._IyzicoPaymentSettings.ErrorCode10012 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10012);
            //    else if (payment.ErrorCode == "10041" && this._IyzicoPaymentSettings.ErrorCode10041 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10041);
            //    else if (payment.ErrorCode == "10043" && this._IyzicoPaymentSettings.ErrorCode10043 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10043);
            //    else if (payment.ErrorCode == "10054" && this._IyzicoPaymentSettings.ErrorCode10054 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10054);
            //    else if (payment.ErrorCode == "10084" && this._IyzicoPaymentSettings.ErrorCode10084 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10084);
            //    else if (payment.ErrorCode == "10057" && this._IyzicoPaymentSettings.ErrorCode10057 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10057);
            //    else if (payment.ErrorCode == "10058" && this._IyzicoPaymentSettings.ErrorCode10058 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10058);
            //    else if (payment.ErrorCode == "10034" && this._IyzicoPaymentSettings.ErrorCode10034 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10034);
            //    else if (payment.ErrorCode == "10093" && this._IyzicoPaymentSettings.ErrorCode10093 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10093);
            //    else if (payment.ErrorCode == "10201" && this._IyzicoPaymentSettings.ErrorCode10201 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10201);
            //    else if (payment.ErrorCode == "10202" && this._IyzicoPaymentSettings.ErrorCode10202 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10202);
            //    else if (payment.ErrorCode == "10203" && this._IyzicoPaymentSettings.ErrorCode10203 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10203);
            //    else if (payment.ErrorCode == "10204" && this._IyzicoPaymentSettings.ErrorCode10204 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10204);
            //    else if (payment.ErrorCode == "10205" && this._IyzicoPaymentSettings.ErrorCode10205 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10205);
            //    else if (payment.ErrorCode == "10206" && this._IyzicoPaymentSettings.ErrorCode10206 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10206);
            //    else if (payment.ErrorCode == "10207" && this._IyzicoPaymentSettings.ErrorCode10207 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10207);
            //    else if (payment.ErrorCode == "10208" && this._IyzicoPaymentSettings.ErrorCode10208 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10208);
            //    else if (payment.ErrorCode == "10209" && this._IyzicoPaymentSettings.ErrorCode10209 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10209);
            //    else if (payment.ErrorCode == "10210" && this._IyzicoPaymentSettings.ErrorCode10210 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10210);
            //    else if (payment.ErrorCode == "10211" && this._IyzicoPaymentSettings.ErrorCode10211 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10211);
            //    else if (payment.ErrorCode == "10212" && this._IyzicoPaymentSettings.ErrorCode10212 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10212);
            //    else if (payment.ErrorCode == "10213" && this._IyzicoPaymentSettings.ErrorCode10213 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10213);
            //    else if (payment.ErrorCode == "10214" && this._IyzicoPaymentSettings.ErrorCode10214 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10214);
            //    else if (payment.ErrorCode == "10215" && this._IyzicoPaymentSettings.ErrorCode10215 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10215);
            //    else if (payment.ErrorCode == "10216" && this._IyzicoPaymentSettings.ErrorCode10216 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10216);
            //    else if (payment.ErrorCode == "10217" && this._IyzicoPaymentSettings.ErrorCode10217 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10217);
            //    else if (payment.ErrorCode == "10218" && this._IyzicoPaymentSettings.ErrorCode10218 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10218);
            //    else if (payment.ErrorCode == "10219" && this._IyzicoPaymentSettings.ErrorCode10219 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10219);
            //    else if (payment.ErrorCode == "10220" && this._IyzicoPaymentSettings.ErrorCode10220 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10220);
            //    else if (payment.ErrorCode == "10221" && this._IyzicoPaymentSettings.ErrorCode10221 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10221);
            //    else if (payment.ErrorCode == "10222" && this._IyzicoPaymentSettings.ErrorCode10222 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10222);
            //    else if (payment.ErrorCode == "10223" && this._IyzicoPaymentSettings.ErrorCode10223 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10223);
            //    else if (payment.ErrorCode == "10224" && this._IyzicoPaymentSettings.ErrorCode10224 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10224);
            //    else if (payment.ErrorCode == "10225" && this._IyzicoPaymentSettings.ErrorCode10225 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10225);
            //    else if (payment.ErrorCode == "10226" && this._IyzicoPaymentSettings.ErrorCode10226 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10226);
            //    else if (payment.ErrorCode == "10227" && this._IyzicoPaymentSettings.ErrorCode10227 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10227);
            //    else if (payment.ErrorCode == "10228" && this._IyzicoPaymentSettings.ErrorCode10228 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10228);
            //    else if (payment.ErrorCode == "10229" && this._IyzicoPaymentSettings.ErrorCode10229 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10229);
            //    else if (payment.ErrorCode == "10230" && this._IyzicoPaymentSettings.ErrorCode10230 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10230);
            //    else if (payment.ErrorCode == "10231" && this._IyzicoPaymentSettings.ErrorCode10231 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10231);
            //    else if (payment.ErrorCode == "10232" && this._IyzicoPaymentSettings.ErrorCode10232 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10232);
            //    else if (payment.ErrorCode == "10233" && this._IyzicoPaymentSettings.ErrorCode10233 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10233);
            //    else if (payment.ErrorCode == "10234" && this._IyzicoPaymentSettings.ErrorCode10234 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10234);
            //    else if (payment.ErrorCode == "10235" && this._IyzicoPaymentSettings.ErrorCode10235 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10235);
            //    else if (payment.ErrorCode == "10236" && this._IyzicoPaymentSettings.ErrorCode10236 != "") result.AddError(this._IyzicoPaymentSettings.ErrorCode10236);
            //    else { result.AddError("Hata oluş¸tu,tekrar deneyiniz"); }
            //}
        //    var result = new ProcessPaymentResult();
            
            //    result.AuthorizationTransactionId = payment.PaymentId;
            return result;
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            string aa = "";
                string ab;
            var a = 12;
            if (a == 12)
            {
                ab =aa+ "asd";
            }
            //nothing
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = _paymentService.CalculateAdditionalFee( cart,
                _IyzicoPaymentSettings.AdditionalFee, _IyzicoPaymentSettings.AdditionalFeePercentage);

            ///var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart,
            ///_IyzicoPaymentSettings.AdditionalFee, _IyzicoPaymentSettings.AdditionalFeePercentage);   ///// Original
            ///


            return result;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Yakalama yöntemi desteklenmiyor");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {


            var result = new RefundPaymentResult();
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;//this.GetActiveStoreScopeConfiguration(_storeService, _workContext);

            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);

            Options options = new Options();
            options.ApiKey = IyzicoPaymentSettings.ClientId;
            options.BaseUrl = IyzicoPaymentSettings.UseSandbox ? IyzicoPaymentSettings.APIUrlSandbox : IyzicoPaymentSettings.APIUrl;
            options.SecretKey = IyzicoPaymentSettings.ClientSecret;





            var customValues = _paymentService.DeserializeCustomValues(refundPaymentRequest.Order);

            if (refundPaymentRequest.IsPartialRefund == true)
            {
                CreateRefundRequest _request = new CreateRefundRequest();
                _request.ConversationId = customValues["conversationId"].ToString();
                _request.Locale = Locale.TR.ToString();
                _request.PaymentTransactionId = customValues["paymentTransactionId 1"].ToString();
                _request.Price = refundPaymentRequest.AmountToRefund.ToString(); ;
                _request.Ip = _webHelper.GetCurrentIpAddress();
                _request.Currency = Currency.TRY.ToString();

                Refund refund = Armut.Iyzipay.Model.Refund.Create(_request, options);
                //if refund status is 'pending', try to refund once more with the same request parameters
                if (refund.Status == "success")
                {



                    //successfully refunded
                    return new RefundPaymentResult
                    {
                        NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded
                    };



                }
                else
                {
                    var error = "";
                    if (refund.ErrorCode == null)
                        error = refund.ErrorCode + " " + refund.ErrorGroup + " " + refund.ErrorMessage;
                    result.AddError($"Error: {error} ");
                    return result;
                }








            }
            else
            {

                var date = Convert.ToDateTime(refundPaymentRequest.Order.PaidDateUtc);
                
                if (false)// date.Date == DateTime.Now.Date)
                {



                    CreateCancelRequest request = new CreateCancelRequest();
                    request.ConversationId = customValues["conversationId"].ToString();

                    request.Locale = Locale.TR.ToString();
                    request.PaymentId = customValues["paymentId"].ToString();
                    request.Ip = _webHelper.GetCurrentIpAddress();

                    Cancel cancel = Cancel.Create(request, options);






                    if (cancel.Status != "success")
                    {




                        //successfully refunded
                        return new RefundPaymentResult
                        {
                            NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded
                        };

                    }
                    else
                    {
                        var error = "";
                        if (cancel.ErrorCode == null)
                            error = cancel.ErrorCode + " " + cancel.ErrorGroup + " " + cancel.ErrorMessage;
                        result.AddError($"Error: {error} ");
                        return result;
                    }
                }
                else
                {

                    string iyzicoResponse = customValues["iyzicoResponse"].ToString();
                   // var b =
                         CheckoutForm deserializedIyzicoResponse= JsonConvert.DeserializeObject<CheckoutForm>(iyzicoResponse);
                        List<Refund> status = new List<Refund>();
                    var items = deserializedIyzicoResponse.PaymentItems;


                        for (int i = 0; i < items.Count; i++)
                    {

                     
                       

                    CreateRefundRequest _request = new CreateRefundRequest();
                        var item = items[i];
                        _request.ConversationId = item.ConversationId;
                        _request.Locale = Locale.TR.ToString();
                    _request.PaymentTransactionId = item.PaymentTransactionId;
                    _request.Price = item.Price;
                    _request.Ip = _webHelper.GetCurrentIpAddress();
                    _request.Currency = Currency.TRY.ToString();

                    Refund refund = Armut.Iyzipay.Model.Refund.Create(_request, options);
                  
                        status.Add(refund);
                       

                    }
                    //if refund status is 'pending', try to refund once more with the same request parameters
                    var error = "";
                    int countResult = 0;
                    int countError = 0;
                    foreach (var item in status)
                    {
                        countResult++;

                        if (item.Status == "success")
                        {
                            error += countResult.ToString() + "success";
                        }
                        else
                        {
                            countError++;

                            error+= item.ErrorCode + " " + item.ErrorGroup + " " + item.ErrorMessage;
                        }



                    }
                    if (countError == 0)
                    {



                        //successfully refunded
                        return new RefundPaymentResult
                        {
                            NewPaymentStatus = refundPaymentRequest.IsPartialRefund ? PaymentStatus.PartiallyRefunded : PaymentStatus.Refunded,
                           
                        };



                    }
                    else
                    {
                     
                        result.AddError($"Error: {error} ");
                        return result;
                    }

                }


                
            







            


            } 

    

        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Boşluk yöntemi desteklenmiyor");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            result.AllowStoringCreditCardNumber = true;
            switch (_IyzicoPaymentSettings.TransactMode)
            {
                case TransactMode.Pending:
                    result.NewPaymentStatus = PaymentStatus.Pending;
                    break;
                case TransactMode.Authorize:
                    result.NewPaymentStatus = PaymentStatus.Authorized;
                    break;
                case TransactMode.AuthorizeAndCapture:
                    result.NewPaymentStatus = PaymentStatus.Paid;
                    break;
                default:
                    {
                        result.AddError("Desteklenmeyen işlem türü");
                        return result;
                    }
            }

            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            //always success
            return new CancelRecurringPaymentResult();
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentIyzico";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Iyzico.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentIyzico";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Iyzico.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Get the type of controller
        /// </summary>
        /// <returns>Type</returns>
        public Type GetControllerType()
        {
            return typeof(PaymentIyzicoController);
        }

        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentIyzico/Configure";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new IyzicoPaymentSettings
            {
                TransactMode = TransactMode.Pending
            };
            _settingService.SaveSetting(settings);

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFee", "Ek ücret");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFee.Hint", "Müş¸terilerinizi şarj etmek için ek ücret girin.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFeePercentage", "Ek ücret. Kullanım yüzdesi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFeePercentage.Hint", "Sipariş¸ toplamına yüzde ek bir ücret uygulayıp uygulamayacağınızı belirler. Etkinleş¸tirilmemiş¸se, sabit bir değer kullanılır.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.TransactMode", "ödeme sonrası ödeme olarak iş¸aretleme");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.TransactMode.Hint", "ış¸lem modunu belirtin.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.PaymentMethodDescription", "Kredi / bankamatik kartıyla ödeme yap");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ClientId", "Client ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ClientSecret", "Client Secret");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.UseSandbox", "SandBox");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.APIUrl", "API URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.APIUrlSandbox", "API URL SandBox");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10051", "Kart limiti yetersiz, yetersiz bakiye");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10005", "İşlem onaylanmadı.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10012", "Geçersiz İşlem");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10041", "Kayıp kart, karta el koyunuz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10043", "Geçersiz kart, karta el koyunuz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10054", "Vadesi dolmuş¸ kart");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10084", "CVC2 bilgisi hatalı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10057", "Kart sahibi bu işlemi yapamaz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10058", "Terminalin bu işlemi yapmaya yetkisi yok");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10034", "Dolandırıcılık şüphesi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10093", "Karta el koy");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10201", "Kart, işleme izin vermedi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10202", "ödeme işlemi esnasında genel bir hata oluştu");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10203", "önceden onaylanan işlem");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10204", "ödeme işlemi esnasında genel bir hata oluştu");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10205", "E-posta geçerli formata değil");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10206", "CVC uzunluğu geçersiz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10207", "Bankanızdan onay alınız");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10208", "Üye iş¸yeri kategori kodu hatalı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10209", "Bloke statü kart");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10210", "Hatalı CAVV bilgisi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10211", "Hatalı ECI bilgisi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10212", "CVC2 yanlış¸ girme deneme sayısı aşıldı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10213", "BIN bulunamadı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10214", "ıletiş¸im veya sistem hatası");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10215", "Geçersiz kart numarası");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10216", "Bankası bulunamadı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10217", "Banka kartları sadece 3D Secure iş¸leminde kullanılabilir ");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10218", "Banka kartları ile taksit yapılamaz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10219", "Bankaya gönderilen istek zaman aşımına uğradı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10220", "ödeme alınamadı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10221", "Terminal yurtdış¸ı kartlara kapalı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10222", "Terminal taksitli işleme kapalı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10223", "Gün sonu yapılmalı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10224", "Para çekme limiti aşılmış");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10225", "Kısıtlı kart");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10226", "İzin verilen PIN giriş¸ sayısı aşılmış");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10227", "Geçersiz PIN");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10228", "Banka veya terminal iş¸lem yapamıyor");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10229", "Son kullanma tarihi geçersiz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10230", "İstek bankadan hata aldı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10231", "Satış¸ tutarı ödül puanından düşük olamaz");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10232", "Geçersiz tutar");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10233", "Geçersiz kart tipi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10234", "Yetersiz  puanı");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10235", "American Express kart hatası");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10236", "ödeme işlemi esnasında genel bir hata oluştu");

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.UseParasut", "Use Paraşüt");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutClientId", "Paraşüt Client_ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutClientSecret", "Paraşüt Client Secret");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutUserName", "Paraşüt UserName");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutPassword", "Paraşüt Password");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutCompanyId", "Paraşüt Company_ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutBankAccountId", "Paraşüt BankAccount_ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutBaseApiToken", "Paraşüt BaseApiforToken");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutBaseApi", "Paraşüt ParasutBaseApi");


            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<IyzicoPaymentSettings>();

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFee");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFee.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFeePercentage");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.AdditionalFeePercentage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.TransactMode");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.TransactMode.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.PaymentMethodDescription");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ClientId");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ClientSecret");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.UseSandbox");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.APIUrl");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.APIUrlSandbox");

            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10051");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10005");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10012");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10041");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10043");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10054");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10084");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10057");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10058");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10034");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10093");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10201");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10202");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10203");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10204");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10205");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10206");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10207");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10208");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10209");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10210");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10211");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10212");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10213");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10214");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10215");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10216");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10217");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10218");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10219");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10220");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10221");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10222");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10223");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10224");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10225");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10226");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10227");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10228");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10229");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10230");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10231");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10232");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10233");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10234");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10235");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ErrorCode10236");


            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.UseParasut");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutClientId");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutClientSecret");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutUserName");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutPassword");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutCompanyId");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutBankAccountId");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutBaseApiToken");
            _localizationService.DeletePluginLocaleResource("Plugins.Payments.Iyzico.Fields.ParasutBaseApi");


            base.Uninstall();
        }

        public IList<string> ValidatePaymentForm(IFormCollection form)
        {

            return new List<string>();

            //var warnings = new List<string>();

            ////validate
            //var validator = new PaymentInfoValidator(_localizationService);
            //var model = new PaymentInfoModel
            //{
            //    CardholderName = form["CardholderName"],
            //    CardNumber = form["CardNumber"],
            //    CardCode = form["CardCode"],
            //    ExpireMonth = form["ExpireMonth"],
            //    ExpireYear = form["ExpireYear"]
            //};
            //var validationResult = validator.Validate(model);
            //if (!validationResult.IsValid)
            //    foreach (var error in validationResult.Errors)
            //        warnings.Add(error.ErrorMessage);
            //return warnings;


            // throw new NotImplementedException();
        }

        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest();

            //var storeScope = _storeContext.ActiveStoreScopeConfiguration; //this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            //var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);
            //var model = new PaymentInfoModel();
            //var customer = _workContext.CurrentCustomer;

            //var paymentInfo = new ProcessPaymentRequest();
            //paymentInfo.CreditCardType = form["CreditCardType"];
            //paymentInfo.CreditCardName = form["CardholderName"];
            //paymentInfo.CreditCardNumber = form["CardNumber"];
            //paymentInfo.CreditCardExpireMonth = int.Parse(form["ExpireMonth"]);
            //paymentInfo.CreditCardExpireYear = int.Parse(form["ExpireYear"]);
            //paymentInfo.CreditCardCvv2 = form["CardCode"];
            //paymentInfo.OrderGuid = Guid.NewGuid();


            //return paymentInfo;


            //throw new NotImplementedException();
        }

        public string GetPublicViewComponentName()
        {

            return "PaymentIyzico";
            //throw new NotImplementedException();
        }

        #endregion


        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.Automatic; }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
           //get
           // {
           //     return PaymentMethodType.Redirection;
           // }
             get { return PaymentMethodType.Redirection; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Payments.Iyzico.PaymentMethodDescription"); }
        }

        #endregion
    }
}
