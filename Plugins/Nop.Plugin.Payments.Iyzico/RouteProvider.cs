﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.Iyzico
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {

            //PDTHandler
            routeBuilder.MapRoute("Plugin.Payments.Iyzico.PDTHandler","OdemeOnay",
                 new { controller = "PaymentIyzico", action = "PDTHandler" }
               //  ,                 new[] { "Nop.Plugin.Payments.Iyzico.Controllers" }
            );
  
            //IyzicoComponent
   //         routeBuilder.MapRoute("Plugin.Payments.Iyzico.PaymentIyzico", "Plugins/PaymentsIyzico/PaymentIyzico",
   //          new { controller = "PaymentIyzico", action = "PaymentIyzico" }
   //         //  ,                 new[] { "Nop.Plugin.Payments.Iyzico.Controllers" }
   //         );
   //
   //         routeBuilder.MapRoute("Plugin.Payments.Iyzico.GetOrderInfo", "GetOrderInfo",
   //      new { controller = "PaymentIyzico", action = "GetOrderInfo" }
   // //  ,                 new[] { "Nop.Plugin.Payments.Iyzico.Controllers" }
   // );

        }



        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
