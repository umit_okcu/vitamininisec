﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Nop.Plugin.Payments.Iyzico.Models;
using Nop.Plugin.Payments.Iyzico.Validators;
using Nop.Services;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Services.Common;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Web;
using Armut.Iyzipay.Model;
using Armut.Iyzipay.Request;
using Armut.Iyzipay;
using Nop.Core.Domain.Orders;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Http.Extensions;
using Nop.Core.Domain.Customers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Services.Discounts;
using Nop.Web.Models.ShoppingCart;

using Nop.Web.Factories;

using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;
using System.Globalization;
using Nop.Services.Logging;

namespace Nop.Plugin.Payments.Iyzico.Controllers
{
  
    public class PaymentIyzicoController : BasePaymentController
    {




        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;

        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        // private readonly HttpContext _httpContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IPaymentService _paymentService;
        private readonly PaymentSettings _paymentSettings;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IyzicoPaymentSettings _iyzicoPaymentSettings;

        private readonly IOrderProcessingService _orderProcessingService;

        private readonly OrderSettings _orderSettings;
        private readonly IRepository<Language> _languageRepository;
        private readonly IRepository<Store> _storeRepository;

        private readonly IShoppingCartService _shoppingCart;
        private readonly IGenericAttributeService _genericAttributeService;
        // private readonly IHttpClientFactory _httpClientFactory;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;

        public PaymentIyzicoController(
            ILogger logger,
        //    IHttpClientFactory httpClientFactory,
                                          IWorkContext workContext,
                                       IStoreService storeService,
                                       ISettingService settingService,
                                       ICustomerService customerService,
                                       IWebHelper webHelper,
                                         IOrderProcessingService orderProcessingService,
                                          IOrderService orderService,
                                         IShoppingCartModelFactory shoppingCartModelFactory,
                                       IOrderTotalCalculationService orderTotalCalculationService,
                                       IStoreContext storeContext,
                                       IPaymentService paymentService,
                                     //  HttpContext httpContext,  //HttpContextBase
                                     IHttpContextAccessor httpContextAccessor,
                                       PaymentSettings paymentSettings,
                                              OrderSettings orderSettings,
                                       IyzicoPaymentSettings iyzicoPaymentSettings,
                                       ILocalizationService localizationService,
                                       IRepository<Language> languageRepository,
                                       IRepository<Store> storeRepository,
                                       IShoppingCartService shoppingCart,
                                       IGenericAttributeService genericAttributeService
                                       )
        {
            // this._httpClientFactory = httpClientFactory;
            this._logger = logger;
            this._workContext = workContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._customerService = customerService;
            this._webHelper = webHelper;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._storeContext = storeContext;
          //  this._httpContext = httpContext;
          this._httpContextAccessor = httpContextAccessor;
            this._orderSettings = orderSettings;
            this._paymentService = paymentService;
            this._paymentSettings = paymentSettings;
            this._iyzicoPaymentSettings = iyzicoPaymentSettings;
            this._storeRepository = storeRepository;
            this._languageRepository = languageRepository;
            this._shoppingCart = shoppingCart;
            this._shoppingCartModelFactory = shoppingCartModelFactory;
            this._genericAttributeService = genericAttributeService;
        }
         [AuthorizeAdmin]
         [Area(AreaNames.Admin)]
        public IActionResult Configure()
        {
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;// this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);

            var model = new ConfigurationModel();
            model.TransactModeId = Convert.ToInt32(IyzicoPaymentSettings.TransactMode);
            model.AdditionalFee = IyzicoPaymentSettings.AdditionalFee;
            model.AdditionalFeePercentage = IyzicoPaymentSettings.AdditionalFeePercentage;
            model.TransactModeValues = IyzicoPaymentSettings.TransactMode.ToSelectList();

            model.ClientId = IyzicoPaymentSettings.ClientId;
            model.ClientSecret = IyzicoPaymentSettings.ClientSecret;
            model.APIUrl = IyzicoPaymentSettings.APIUrl;
            model.APIUrlSandbox = IyzicoPaymentSettings.APIUrlSandbox;
            model.UseSandbox = IyzicoPaymentSettings.UseSandbox;

            model.ErrorCode10051 = IyzicoPaymentSettings.ErrorCode10051;
            model.ErrorCode10005 = IyzicoPaymentSettings.ErrorCode10005;
            model.ErrorCode10012 = IyzicoPaymentSettings.ErrorCode10012;
            model.ErrorCode10041 = IyzicoPaymentSettings.ErrorCode10041;
            model.ErrorCode10043 = IyzicoPaymentSettings.ErrorCode10043;
            model.ErrorCode10054 = IyzicoPaymentSettings.ErrorCode10054;
            model.ErrorCode10084 = IyzicoPaymentSettings.ErrorCode10084;
            model.ErrorCode10057 = IyzicoPaymentSettings.ErrorCode10057;
            model.ErrorCode10058 = IyzicoPaymentSettings.ErrorCode10058;
            model.ErrorCode10034 = IyzicoPaymentSettings.ErrorCode10034;
            model.ErrorCode10093 = IyzicoPaymentSettings.ErrorCode10093;
            model.ErrorCode10201 = IyzicoPaymentSettings.ErrorCode10201;
            model.ErrorCode10202 = IyzicoPaymentSettings.ErrorCode10202;
            model.ErrorCode10203 = IyzicoPaymentSettings.ErrorCode10203;
            model.ErrorCode10204 = IyzicoPaymentSettings.ErrorCode10204;
            model.ErrorCode10205 = IyzicoPaymentSettings.ErrorCode10205;
            model.ErrorCode10206 = IyzicoPaymentSettings.ErrorCode10206;
            model.ErrorCode10207 = IyzicoPaymentSettings.ErrorCode10207;
            model.ErrorCode10208 = IyzicoPaymentSettings.ErrorCode10208;
            model.ErrorCode10209 = IyzicoPaymentSettings.ErrorCode10209;
            model.ErrorCode10210 = IyzicoPaymentSettings.ErrorCode10210;
            model.ErrorCode10211 = IyzicoPaymentSettings.ErrorCode10211;
            model.ErrorCode10212 = IyzicoPaymentSettings.ErrorCode10212;
            model.ErrorCode10213 = IyzicoPaymentSettings.ErrorCode10213;
            model.ErrorCode10214 = IyzicoPaymentSettings.ErrorCode10214;
            model.ErrorCode10215 = IyzicoPaymentSettings.ErrorCode10215;
            model.ErrorCode10216 = IyzicoPaymentSettings.ErrorCode10216;
            model.ErrorCode10217 = IyzicoPaymentSettings.ErrorCode10217;
            model.ErrorCode10218 = IyzicoPaymentSettings.ErrorCode10218;
            model.ErrorCode10219 = IyzicoPaymentSettings.ErrorCode10219;
            model.ErrorCode10220 = IyzicoPaymentSettings.ErrorCode10220;
            model.ErrorCode10221 = IyzicoPaymentSettings.ErrorCode10221;
            model.ErrorCode10222 = IyzicoPaymentSettings.ErrorCode10222;
            model.ErrorCode10223 = IyzicoPaymentSettings.ErrorCode10223;
            model.ErrorCode10224 = IyzicoPaymentSettings.ErrorCode10224;
            model.ErrorCode10225 = IyzicoPaymentSettings.ErrorCode10225;
            model.ErrorCode10226 = IyzicoPaymentSettings.ErrorCode10226;
            model.ErrorCode10227 = IyzicoPaymentSettings.ErrorCode10227;
            model.ErrorCode10228 = IyzicoPaymentSettings.ErrorCode10228;
            model.ErrorCode10229 = IyzicoPaymentSettings.ErrorCode10229;
            model.ErrorCode10230 = IyzicoPaymentSettings.ErrorCode10230;
            model.ErrorCode10231 = IyzicoPaymentSettings.ErrorCode10231;
            model.ErrorCode10232 = IyzicoPaymentSettings.ErrorCode10232;
            model.ErrorCode10233 = IyzicoPaymentSettings.ErrorCode10233;
            model.ErrorCode10234 = IyzicoPaymentSettings.ErrorCode10234;
            model.ErrorCode10235 = IyzicoPaymentSettings.ErrorCode10235;
            model.ErrorCode10236 = IyzicoPaymentSettings.ErrorCode10236;

            model.ParasutUserName = IyzicoPaymentSettings.ParasutUserName;
            model.ParasutPassword = IyzicoPaymentSettings.ParasutPassword;
            model.ParasutCompanyId = IyzicoPaymentSettings.ParasutCompanyId;
            model.ParasutBankAccountId = IyzicoPaymentSettings.ParasutBankAccountId;
            model.ParasutClientId = IyzicoPaymentSettings.ParasutClientId;
            model.UseParasut = IyzicoPaymentSettings.UseParasut;

            model.ParasutClientSecret = IyzicoPaymentSettings.ParasutClientSecret;
            model.ParasutBaseApiToken = IyzicoPaymentSettings.ParasutBaseApiToken;
            model.ParasutBaseApi = IyzicoPaymentSettings.ParasutBaseApi;

            model.ActiveStoreScopeConfiguration = storeScope;
            if (storeScope > 0)
            {
                model.TransactModeId_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.TransactMode, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.AdditionalFeePercentage, storeScope);

                model.ClientId_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ClientId, storeScope);
                model.ClientSecret_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ClientSecret, storeScope);
                model.APIUrl_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.APIUrl, storeScope);
                model.APIUrlSandbox_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.APIUrlSandbox, storeScope);
                model.UseSandbox = _settingService.SettingExists(IyzicoPaymentSettings, x => x.UseSandbox, storeScope);

                model.ErrorCode10051_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10051, storeScope);
                model.ErrorCode10005_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10005, storeScope);
                model.ErrorCode10012_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10012, storeScope);
                model.ErrorCode10041_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10041, storeScope);
                model.ErrorCode10043_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10043, storeScope);
                model.ErrorCode10054_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10054, storeScope);
                model.ErrorCode10084_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10084, storeScope);
                model.ErrorCode10057_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10057, storeScope);
                model.ErrorCode10058_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10058, storeScope);
                model.ErrorCode10034_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10034, storeScope);
                model.ErrorCode10093_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10093, storeScope);
                model.ErrorCode10201_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10201, storeScope);
                model.ErrorCode10202_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10202, storeScope);
                model.ErrorCode10203_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10203, storeScope);
                model.ErrorCode10204_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10204, storeScope);
                model.ErrorCode10205_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10205, storeScope);
                model.ErrorCode10206_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10206, storeScope);
                model.ErrorCode10207_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10207, storeScope);
                model.ErrorCode10208_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10208, storeScope);
                model.ErrorCode10209_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10209, storeScope);
                model.ErrorCode10210_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10210, storeScope);
                model.ErrorCode10211_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10211, storeScope);
                model.ErrorCode10212_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10212, storeScope);
                model.ErrorCode10213_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10213, storeScope);
                model.ErrorCode10214_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10214, storeScope);
                model.ErrorCode10215_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10215, storeScope);
                model.ErrorCode10216_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10216, storeScope);
                model.ErrorCode10217_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10217, storeScope);
                model.ErrorCode10218_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10218, storeScope);
                model.ErrorCode10219_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10219, storeScope);
                model.ErrorCode10220_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10220, storeScope);
                model.ErrorCode10221_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10221, storeScope);
                model.ErrorCode10222_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10222, storeScope);
                model.ErrorCode10223_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10223, storeScope);
                model.ErrorCode10224_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10224, storeScope);
                model.ErrorCode10225_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10225, storeScope);
                model.ErrorCode10226_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10226, storeScope);
                model.ErrorCode10227_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10227, storeScope);
                model.ErrorCode10228_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10228, storeScope);
                model.ErrorCode10229_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10229, storeScope);
                model.ErrorCode10230_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10230, storeScope);
                model.ErrorCode10231_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10231, storeScope);
                model.ErrorCode10232_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10232, storeScope);
                model.ErrorCode10233_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10233, storeScope);
                model.ErrorCode10234_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10234, storeScope);
                model.ErrorCode10235_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10235, storeScope);
                model.ErrorCode10236_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ErrorCode10236, storeScope);

                model.ParasutUserName_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutUserName, storeScope);
                model.ParasutPassword_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutPassword, storeScope);
                model.ParasutCompanyId_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutCompanyId, storeScope);
                model.ParasutBankAccountId_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutBankAccountId, storeScope);
                model.ParasutClientId_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutClientId, storeScope);
                model.ParasutClientSecret_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutClientSecret, storeScope);
                model.ParasutBaseApiToken_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutBaseApiToken, storeScope);
                model.ParasutBaseApi_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.ParasutBaseApi, storeScope);

                model.UseParasut_OverrideForStore = _settingService.SettingExists(IyzicoPaymentSettings, x => x.UseParasut, storeScope);








            }


            return View("~/Plugins/Payments.Iyzico/Views/Configure.cshtml", model);
        }
         [AuthorizeAdmin]
         [Area(AreaNames.Admin)]
         [HttpPost]
         [AdminAntiForgery]
        public IActionResult Configure(ConfigurationModel model)
        {
            decimal feeefalan = model.AdditionalFee;

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;// this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);

            //save settings
            IyzicoPaymentSettings.TransactMode = (TransactMode)model.TransactModeId;
            IyzicoPaymentSettings.AdditionalFee = model.AdditionalFee;
            IyzicoPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;


            IyzicoPaymentSettings.ClientId = model.ClientId;
            IyzicoPaymentSettings.ClientSecret = model.ClientSecret;
            IyzicoPaymentSettings.APIUrl = model.APIUrl;
            IyzicoPaymentSettings.APIUrlSandbox = model.APIUrlSandbox;
            IyzicoPaymentSettings.UseSandbox = model.UseSandbox;

            IyzicoPaymentSettings.ErrorCode10051 = model.ErrorCode10051;
            IyzicoPaymentSettings.ErrorCode10005 = model.ErrorCode10005;
            IyzicoPaymentSettings.ErrorCode10012 = model.ErrorCode10012;
            IyzicoPaymentSettings.ErrorCode10041 = model.ErrorCode10041;
            IyzicoPaymentSettings.ErrorCode10043 = model.ErrorCode10043;
            IyzicoPaymentSettings.ErrorCode10054 = model.ErrorCode10054;
            IyzicoPaymentSettings.ErrorCode10084 = model.ErrorCode10084;
            IyzicoPaymentSettings.ErrorCode10057 = model.ErrorCode10057;
            IyzicoPaymentSettings.ErrorCode10058 = model.ErrorCode10058;
            IyzicoPaymentSettings.ErrorCode10034 = model.ErrorCode10034;
            IyzicoPaymentSettings.ErrorCode10093 = model.ErrorCode10093;
            IyzicoPaymentSettings.ErrorCode10201 = model.ErrorCode10201;
            IyzicoPaymentSettings.ErrorCode10202 = model.ErrorCode10202;
            IyzicoPaymentSettings.ErrorCode10203 = model.ErrorCode10203;
            IyzicoPaymentSettings.ErrorCode10204 = model.ErrorCode10204;
            IyzicoPaymentSettings.ErrorCode10205 = model.ErrorCode10205;
            IyzicoPaymentSettings.ErrorCode10206 = model.ErrorCode10206;
            IyzicoPaymentSettings.ErrorCode10207 = model.ErrorCode10207;
            IyzicoPaymentSettings.ErrorCode10208 = model.ErrorCode10208;
            IyzicoPaymentSettings.ErrorCode10209 = model.ErrorCode10209;
            IyzicoPaymentSettings.ErrorCode10210 = model.ErrorCode10210;
            IyzicoPaymentSettings.ErrorCode10211 = model.ErrorCode10211;
            IyzicoPaymentSettings.ErrorCode10212 = model.ErrorCode10212;
            IyzicoPaymentSettings.ErrorCode10213 = model.ErrorCode10213;
            IyzicoPaymentSettings.ErrorCode10214 = model.ErrorCode10214;
            IyzicoPaymentSettings.ErrorCode10215 = model.ErrorCode10215;
            IyzicoPaymentSettings.ErrorCode10216 = model.ErrorCode10216;
            IyzicoPaymentSettings.ErrorCode10217 = model.ErrorCode10217;
            IyzicoPaymentSettings.ErrorCode10218 = model.ErrorCode10218;
            IyzicoPaymentSettings.ErrorCode10219 = model.ErrorCode10219;
            IyzicoPaymentSettings.ErrorCode10220 = model.ErrorCode10220;
            IyzicoPaymentSettings.ErrorCode10221 = model.ErrorCode10221;
            IyzicoPaymentSettings.ErrorCode10222 = model.ErrorCode10222;
            IyzicoPaymentSettings.ErrorCode10223 = model.ErrorCode10223;
            IyzicoPaymentSettings.ErrorCode10224 = model.ErrorCode10224;
            IyzicoPaymentSettings.ErrorCode10225 = model.ErrorCode10225;
            IyzicoPaymentSettings.ErrorCode10226 = model.ErrorCode10226;
            IyzicoPaymentSettings.ErrorCode10227 = model.ErrorCode10227;
            IyzicoPaymentSettings.ErrorCode10228 = model.ErrorCode10228;
            IyzicoPaymentSettings.ErrorCode10229 = model.ErrorCode10229;
            IyzicoPaymentSettings.ErrorCode10230 = model.ErrorCode10230;
            IyzicoPaymentSettings.ErrorCode10231 = model.ErrorCode10231;
            IyzicoPaymentSettings.ErrorCode10232 = model.ErrorCode10232;
            IyzicoPaymentSettings.ErrorCode10233 = model.ErrorCode10233;
            IyzicoPaymentSettings.ErrorCode10234 = model.ErrorCode10234;
            IyzicoPaymentSettings.ErrorCode10235 = model.ErrorCode10235;
            IyzicoPaymentSettings.ErrorCode10236 = model.ErrorCode10236;

            IyzicoPaymentSettings.ParasutUserName = model.ParasutUserName ;
           IyzicoPaymentSettings.ParasutPassword = model.ParasutPassword ;
           IyzicoPaymentSettings.ParasutCompanyId = model.ParasutCompanyId ;
         IyzicoPaymentSettings.ParasutBankAccountId = model.ParasutBankAccountId ;
            IyzicoPaymentSettings.ParasutClientId = model.ParasutClientId ;
            IyzicoPaymentSettings.ParasutClientSecret = model.ParasutClientSecret;
            IyzicoPaymentSettings.UseParasut = model.UseParasut;
            IyzicoPaymentSettings.ParasutBaseApiToken = model.ParasutBaseApiToken;
            IyzicoPaymentSettings.ParasutBaseApi = model.ParasutBaseApi;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */

            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.TransactMode, model.TransactModeId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.AdditionalFee, model.AdditionalFee_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.AdditionalFeePercentage, model.AdditionalFeePercentage_OverrideForStore, storeScope, false);


            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ClientId, model.ClientId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ClientSecret, model.ClientSecret_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.APIUrl, model.APIUrl_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.APIUrlSandbox, model.APIUrlSandbox_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.UseSandbox, model.UseSandbox_OverrideForStore, storeScope, false);


            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10051, model.ErrorCode10051_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10005, model.ErrorCode10005_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10012, model.ErrorCode10012_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10041, model.ErrorCode10041_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10043, model.ErrorCode10043_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10054, model.ErrorCode10054_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10084, model.ErrorCode10084_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10057, model.ErrorCode10057_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10058, model.ErrorCode10058_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10034, model.ErrorCode10034_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10093, model.ErrorCode10093_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10201, model.ErrorCode10201_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10202, model.ErrorCode10202_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10203, model.ErrorCode10203_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10204, model.ErrorCode10204_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10205, model.ErrorCode10205_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10206, model.ErrorCode10206_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10207, model.ErrorCode10207_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10208, model.ErrorCode10208_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10209, model.ErrorCode10209_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10210, model.ErrorCode10210_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10211, model.ErrorCode10211_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10212, model.ErrorCode10212_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10213, model.ErrorCode10213_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10214, model.ErrorCode10214_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10215, model.ErrorCode10215_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10216, model.ErrorCode10216_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10217, model.ErrorCode10217_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10218, model.ErrorCode10218_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10219, model.ErrorCode10219_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10220, model.ErrorCode10220_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10221, model.ErrorCode10221_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10222, model.ErrorCode10222_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10223, model.ErrorCode10223_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10224, model.ErrorCode10224_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10225, model.ErrorCode10225_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10226, model.ErrorCode10226_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10227, model.ErrorCode10227_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10228, model.ErrorCode10228_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10229, model.ErrorCode10229_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10230, model.ErrorCode10230_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10231, model.ErrorCode10231_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10232, model.ErrorCode10232_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10233, model.ErrorCode10233_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10234, model.ErrorCode10234_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10235, model.ErrorCode10235_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ErrorCode10236, model.ErrorCode10236_OverrideForStore, storeScope, false);

            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutClientId, model.ParasutClientId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutClientSecret, model.ParasutClientSecret_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutUserName, model.ParasutUserName_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutPassword, model.ParasutPassword_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutCompanyId, model.ParasutCompanyId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutBankAccountId, model.ParasutBankAccountId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutBaseApiToken, model.UseParasut_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.ParasutBaseApi, model.ParasutBaseApiToken_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(IyzicoPaymentSettings, x => x.UseParasut, model.ParasutBaseApi_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }



        [HttpPost]
        public JsonResult GetOrderInfo(string username,string password,string orderlimit)
        {


            string nameShould = _settingService.GetSettingByKey<string>("faturalamaservisi.username");
            string passShould = _settingService.GetSettingByKey<string>("faturalamaservisi.password");

            if (username == nameShould && password == passShould)
            {
                _logger.Error("OrderInfoRequested | engine started");

                //var DateFrom = null;// DateTime.UtcNow.AddDays(-1);
                //var DateTo = null;//DateTime.UtcNow;
                bool shouldContinue = Int32.TryParse(orderlimit, out int _orderid);
                var _order = _orderService.GetOrderById(_orderid);
                var _orders = _orderService.SearchOrders(0, 0, 0, 0, 0, 0, 0, null, _order.CreatedOnUtc, null, null, null, null, null, null, string.Empty, null, 0, 10000000, false).Where(x => x.ShippingStatus != ShippingStatus.Delivered);
                InvoiceDataModel data = new InvoiceDataModel();
                HashSet<Datum> Orders = new HashSet<Datum>();
                if (_orders != null&& shouldContinue)
                {

                   // var _ordersData = _orders.Where(x => x.Id > _orderid);

                    foreach (var item in _orders.Reverse())
                    {
                        Datum order = new Datum();
                        
                        SiparisData siparisData = new SiparisData();
                        
                        siparisData.AdresId = item.BillingAddress.Id;
                        siparisData.Adres1 = item.BillingAddress.Address1;
                        siparisData.Adres2 = item.BillingAddress.Address2;
                        siparisData.Eposta = item.BillingAddress.Email;
                        siparisData.Il = item.BillingAddress.StateProvince.Name;
                        siparisData.Ilce = item.BillingAddress.City;
                        siparisData.Kargo = Convert.ToDouble(item.OrderShippingInclTax);
                        siparisData.MusteriAdi = item.BillingAddress.FirstName;
                        siparisData.MusteriId = item.CustomerId;
                        siparisData.MusteriSoyadi = item.BillingAddress.LastName;

                        if (item.BillingAddress.IsEnterprice)

                        {

                            siparisData.MusteriTipi = "Tüzel";
                            siparisData.MusteriUnvani = item.BillingAddress.Company;
                            siparisData.Tckn = "";
                            siparisData.VergiDairesi = item.BillingAddress.VD;
                            siparisData.VergiNo = item.BillingAddress.VKN;
                        }
                        else
                        {
                            siparisData.MusteriTipi = "Şahıs";
                            siparisData.MusteriUnvani = item.BillingAddress.FirstName + " " + item.BillingAddress.LastName;
                            siparisData.Tckn = item.BillingAddress.TCKN;
                            siparisData.VergiDairesi = "";
                            siparisData.VergiNo = "";
                        }
                            siparisData.PostaKodu = item.BillingAddress.ZipPostalCode;
                            siparisData.SiparisId = item.Id;
                            siparisData.SiparisTarihi = Convert.ToDateTime(item.PaidDateUtc);
                            siparisData.Tel1 = item.BillingAddress.PhoneNumber;

                            siparisData.Ulke = "Türkiye";

                            SiparisSevkiyat siparisSevkiyat = new SiparisSevkiyat();

                            siparisSevkiyat.AdresId = item.ShippingAddress.Id;
                            siparisSevkiyat.SevkAdi = item.ShippingAddress.FirstName;
                            siparisSevkiyat.SevkAdres1 = item.ShippingAddress.Address1;
                            siparisSevkiyat.SevkAdres2 = item.ShippingAddress.Address2;
                            siparisSevkiyat.SevkIl = item.ShippingAddress.StateProvince.Name;
                            siparisSevkiyat.SevkIlce = item.ShippingAddress.City;
                            siparisSevkiyat.SevkiyatEposta = item.ShippingAddress.Email;
                            siparisSevkiyat.SevkPostakodu = item.ShippingAddress.ZipPostalCode;
                            siparisSevkiyat.SevkSoyadi = item.ShippingAddress.LastName;
                            siparisSevkiyat.SevkTel1 = item.ShippingAddress.PhoneNumber;
                            siparisSevkiyat.SevkUlke = "Türkiye";

                        if (item.ShippingAddress.IsEnterprice)
                        {

                            siparisSevkiyat.SevkUnvan = item.ShippingAddress.Company;

                        }
                        else
                        {
                            siparisSevkiyat.SevkUnvan = item.ShippingAddress.FirstName+" "+item.ShippingAddress.LastName;
                        }

                        HashSet<SiparisUrunBilgileri> siparisUrunBilgileri = new HashSet<SiparisUrunBilgileri>();


                        foreach (var cartItem in item.OrderItems)
                        {
                            var price = cartItem.UnitPriceInclTax;
                            var kdv = price * 0.08m;
                            var priceWithoutKdv = price - kdv;

                            SiparisUrunBilgileri urunBilgisi = new SiparisUrunBilgileri();
                            urunBilgisi.Birim = "Adet";
                            urunBilgisi.Fiyat = Convert.ToDouble(priceWithoutKdv);
                            urunBilgisi.Kdv = Convert.ToDouble(kdv);
                            urunBilgisi.Kdvharictutar = Convert.ToDouble(priceWithoutKdv*cartItem.Quantity);
                            urunBilgisi.Miktar = cartItem.Quantity;
                            urunBilgisi.UrunAdi = cartItem.Product.Name;
                            urunBilgisi.UrunKodu =cartItem.Product.Sku;
                            urunBilgisi.Tutar = Convert.ToDouble(price);

                            siparisUrunBilgileri.Add(urunBilgisi);
                        }



                        order.SiparisData = siparisData;
                        order.SiparisSevkiyat = siparisSevkiyat;
                        order.SiparisUrunBilgileri = siparisUrunBilgileri;
                        Orders.Add(order);

                    }


                }


                data.Data = Orders;

                if (Orders.Count>0)
                {
                    _logger.Error("OrderInfoRequested | Json returned for Orders: " + Orders.First().SiparisData.SiparisId.ToString() + " - " + Orders.Last().SiparisData.SiparisId.ToString());
                }
                else
                {
                    _logger.Error("OrderInfoRequested | There is no new Order to send ");
                }
              //  var _data = JsonConvert.SerializeObject(data);                                 
                return new JsonResult(data);

            }
            else
            {
                _logger.Error("OrderInfoRequested | username or password is not correct");
                               
                string errorString = "OrderInfoRequested | username or password is not correct";

                return new JsonResult(errorString);
            }
        }


        public IActionResult PDTHandler(IFormCollection form)
        {

            string _mahmut = _httpContextAccessor.HttpContext.Request.Cookies[".Nop.Customer"];
            _logger.Error("Iyzico | Nop Customer Cookies: " + _mahmut);
            string formToken = form["token"];

            

            _logger.Error("Iyzico | formToken2:" + formToken );
            var storeScope = _storeContext.ActiveStoreScopeConfiguration; //this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);
            var model = new PaymentInfoModel();

            _logger.Error("Iyzico | storeScope:" + storeScope.ToString());
            RetrieveCheckoutFormRequest request = new RetrieveCheckoutFormRequest();
            // request.ConversationId = conversationId;
            request.Token = formToken;//form["token"];


            Options options = new Options();
            options.ApiKey = IyzicoPaymentSettings.ClientId;
            options.BaseUrl = IyzicoPaymentSettings.UseSandbox ? IyzicoPaymentSettings.APIUrlSandbox : IyzicoPaymentSettings.APIUrl;
            options.SecretKey = IyzicoPaymentSettings.ClientSecret;
            CheckoutForm checkoutForm = CheckoutForm.Retrieve(request, options);


         


            ///////////////////////////////////////////////
            ///Check If Iyzico payment has completed//
            ///////////////////////////////////////////////
            if (checkoutForm.PaymentStatus == "SUCCESS")
            {

                string _checkoutformBasketId = checkoutForm.BasketId;
                _mahmut = _checkoutformBasketId.Split("_")[0];

                _logger.Error("Iyzico | _mahmutCheckoutCustomerGuid:" + _mahmut);
                // _logger.Error("userGuid: "+ _httpContextAccessor.HttpContext.R);
                _logger.Error("Iyzico | formStatus" + checkoutForm.Status);
                _logger.Error("Iyzico | checkoutPaymentStatus" + checkoutForm.PaymentStatus);





                //_logger.Error("Iyzico | beforeIyzicoCustomerId" + "selam");
                //string IyzicoCustomerId = checkoutForm.ConversationId;

                //_logger.Error("afterIyzicoCustomerId: " + IyzicoCustomerId);
                //string _IdString = "";

                //int ___index = IyzicoCustomerId.IndexOf('-');
                //if (___index > 0)
                //{
                //    _IdString = IyzicoCustomerId.Substring(0, ___index);
                //}
                Customer customer = _customerService.GetCustomerByGuid(new Guid(_mahmut));
                
                //var dateTimeNowTrim = DateTime.Today.ToString().Replace(" ", "-").Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty);
                //var conversationId = customer.Id + "-" + dateTimeNowTrim + "-" + customer.CustomerGuid;
                if (customer != null)
                {

                _logger.Error("Iyzico | customer" + customer.Id);
                }
                else
                {
                    _logger.Error("Iyzico | Fatal Error no customer exist" + "null");
                }



             //   var processPaymentRequest = HttpContext.Session.Get<ProcessPaymentRequest>("OrderPaymentInfo");
             //   if (processPaymentRequest == null)
                 var   processPaymentRequest = new ProcessPaymentRequest();
                processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;//_storeContext.CurrentStore.Id;
                _logger.Error("Iyzico | RegisteredInStoreId" + customer.RegisteredInStoreId.ToString());
                processPaymentRequest.CustomerId = customer.Id;//_workContext.CurrentCustomer.Id;
                processPaymentRequest.PaymentMethodSystemName = _genericAttributeService.GetAttribute<string>(customer,
                    NopCustomerDefaults.SelectedPaymentMethodAttribute, _storeContext.CurrentStore.Id);//_storeContext.CurrentStore.Id);

                _logger.Error("processPaymentRequest.PaymentMethodSystemName:" + processPaymentRequest.PaymentMethodSystemName, null, customer);
                processPaymentRequest.CreditCardType = checkoutForm.CardType;
                    processPaymentRequest.CreditCardName = checkoutForm.CardFamily +" "+ checkoutForm.CardAssociation;
                processPaymentRequest.CreditCardNumber = checkoutForm.BinNumber.ToString() + "** **** ****";
                //checkoutForm.
                string iyzicoPaymenId = checkoutForm.PaymentId.ToString();
                processPaymentRequest.CustomValues.Add("BasketId",checkoutForm.BasketId.ToString());
                processPaymentRequest.CustomValues.Add("token",request.Token.ToString());
                processPaymentRequest.CustomValues.Add("paymentId", checkoutForm.PaymentId.ToString());
                processPaymentRequest.CustomValues.Add("paidPrice", String.Format("{0:0.00}", checkoutForm.PaidPrice));
                processPaymentRequest.CustomValues.Add("installment", checkoutForm.Installment.ToString());
              //  processPaymentRequest.CustomValues.Add("conversationId", conversationId);
                int itemCount = 1;
                var iyzicoResponse = new CheckoutForm();
                iyzicoResponse = checkoutForm;
                foreach (var item in checkoutForm.PaymentItems) 
                {
                    
                    processPaymentRequest.CustomValues.Add(itemCount.ToString()+"_paymentTransactionId", item.PaymentTransactionId.ToString());
                    processPaymentRequest.CustomValues.Add(itemCount.ToString()+"_paidPrice", String.Format("{0:0.00}", item.PaidPrice));
                    processPaymentRequest.CustomValues.Add(itemCount.ToString()+"_paidPrice_convertedPayout" , item.ConvertedPayout.PaidPrice.ToString());
                    itemCount++;
                    
                }
                var a = JsonConvert.SerializeObject(iyzicoResponse);
                //var a = new JavaScriptSerializer().Serialize(iyzicoResponse);
               // var b = Json.parse(iyzicoResponse);
                processPaymentRequest.CustomValues.Add("iyzicoResponse",a);
                



                HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", processPaymentRequest);
                var placeOrderResult = _orderProcessingService.PlaceOrder(processPaymentRequest);
                //  _paymentService.PostProcessPayment(postProcessPaymentRequest);

                _logger.Error("Iyzico | placeOrderResult:" + placeOrderResult.Success,null,customer);
                _logger.Error("Iyzico | _webHelper.IsRequestBeingRedirected:" + _webHelper.IsRequestBeingRedirected.ToString(), null, customer);
                _logger.Error("Iyzico | _webHelper.IsPostBeingDone:" + _webHelper.IsPostBeingDone.ToString(), null, customer);
                _logger.Error("Iyzico | placeOrderResult.PlacedOrder.Id:" + placeOrderResult.PlacedOrder.Id, null, customer);
                ///////////////////////////////////////////////
                ///Palace Order//
                ///////////////////////////////////////////////
                if (placeOrderResult.Success)
                {
                    //   HttpContext.Session.Set<ProcessPaymentRequest>("OrderPaymentInfo", null);
                    //var postProcessPaymentRequest = new PostProcessPaymentRequest
                    //{
                    //    Order = placeOrderResult.PlacedOrder
                    //};
                    //_paymentService.PostProcessPayment(postProcessPaymentRequest);

                    //if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                    //{
                    //    //redirection or POST has been done in PostProcessPayment
                    //    return Content("Redirected");
                    //}
                    //processPaymentRequest.StoreId = _storeContext.CurrentStore.Id;
                    //var updateOrder =_orderService.GetOrderById(placeOrderResult.PlacedOrder.Id);
                    //updateOrder.StoreId = _storeContext.CurrentStore.Id;
                    //_orderService.UpdateOrder(updateOrder); //UpdateOrder(processPaymentRequest);
                    return RedirectToRoute("CheckoutCompleted", new { orderId = placeOrderResult.PlacedOrder.Id });
                }

                //return RedirectToRoute("CheckoutCompleted", new { orderId = order.Id });
               // return RedirectToAction("Confirm", "Checkout");
                return RedirectToAction("OpcCompleteRedirectionPayment", "Checkout"); //RedirectToRoute("OpcCompleteRedirectionPayment");
            }
            else
            {
             
                PaymentInfoModel modelAlert = new PaymentInfoModel();
                
                modelAlert.ErrorMessage = "Ödeme gerçekleşmedi, bilgilerinizi kontrol ediniz.";
                if (checkoutForm.ErrorCode == "10051" && this._iyzicoPaymentSettings.ErrorCode10051 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10051;
                else if (checkoutForm.ErrorCode == "10005" && this._iyzicoPaymentSettings.ErrorCode10005 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10005;
                else if (checkoutForm.ErrorCode == "10012" && this._iyzicoPaymentSettings.ErrorCode10012 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10012;
                else if (checkoutForm.ErrorCode == "10041" && this._iyzicoPaymentSettings.ErrorCode10041 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10041;
                else if (checkoutForm.ErrorCode == "10043" && this._iyzicoPaymentSettings.ErrorCode10043 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10043;
                else if (checkoutForm.ErrorCode == "10054" && this._iyzicoPaymentSettings.ErrorCode10054 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10054;
                else if (checkoutForm.ErrorCode == "10084" && this._iyzicoPaymentSettings.ErrorCode10084 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10084;
                else if (checkoutForm.ErrorCode == "10057" && this._iyzicoPaymentSettings.ErrorCode10057 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10057;
                else if (checkoutForm.ErrorCode == "10058" && this._iyzicoPaymentSettings.ErrorCode10058 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10058;
                else if (checkoutForm.ErrorCode == "10034" && this._iyzicoPaymentSettings.ErrorCode10034 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10034;
                else if (checkoutForm.ErrorCode == "10093" && this._iyzicoPaymentSettings.ErrorCode10093 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10093;
                else if (checkoutForm.ErrorCode == "10201" && this._iyzicoPaymentSettings.ErrorCode10201 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10201;
                else if (checkoutForm.ErrorCode == "10202" && this._iyzicoPaymentSettings.ErrorCode10202 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10202;
                else if (checkoutForm.ErrorCode == "10203" && this._iyzicoPaymentSettings.ErrorCode10203 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10203;
                else if (checkoutForm.ErrorCode == "10204" && this._iyzicoPaymentSettings.ErrorCode10204 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10204;
                else if (checkoutForm.ErrorCode == "10205" && this._iyzicoPaymentSettings.ErrorCode10205 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10205;
                else if (checkoutForm.ErrorCode == "10206" && this._iyzicoPaymentSettings.ErrorCode10206 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10206;
                else if (checkoutForm.ErrorCode == "10207" && this._iyzicoPaymentSettings.ErrorCode10207 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10207;
                else if (checkoutForm.ErrorCode == "10208" && this._iyzicoPaymentSettings.ErrorCode10208 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10208;
                else if (checkoutForm.ErrorCode == "10209" && this._iyzicoPaymentSettings.ErrorCode10209 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10209;
                else if (checkoutForm.ErrorCode == "10210" && this._iyzicoPaymentSettings.ErrorCode10210 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10210;
                else if (checkoutForm.ErrorCode == "10211" && this._iyzicoPaymentSettings.ErrorCode10211 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10211;
                else if (checkoutForm.ErrorCode == "10212" && this._iyzicoPaymentSettings.ErrorCode10212 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10212;
                else if (checkoutForm.ErrorCode == "10213" && this._iyzicoPaymentSettings.ErrorCode10213 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10213;
                else if (checkoutForm.ErrorCode == "10214" && this._iyzicoPaymentSettings.ErrorCode10214 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10214;
                else if (checkoutForm.ErrorCode == "10215" && this._iyzicoPaymentSettings.ErrorCode10215 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10215;
                else if (checkoutForm.ErrorCode == "10216" && this._iyzicoPaymentSettings.ErrorCode10216 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10216;
                else if (checkoutForm.ErrorCode == "10217" && this._iyzicoPaymentSettings.ErrorCode10217 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10217;
                else if (checkoutForm.ErrorCode == "10218" && this._iyzicoPaymentSettings.ErrorCode10218 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10218;
                else if (checkoutForm.ErrorCode == "10219" && this._iyzicoPaymentSettings.ErrorCode10219 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10219;
                else if (checkoutForm.ErrorCode == "10220" && this._iyzicoPaymentSettings.ErrorCode10220 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10220;
                else if (checkoutForm.ErrorCode == "10221" && this._iyzicoPaymentSettings.ErrorCode10221 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10221;
                else if (checkoutForm.ErrorCode == "10222" && this._iyzicoPaymentSettings.ErrorCode10222 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10222;
                else if (checkoutForm.ErrorCode == "10223" && this._iyzicoPaymentSettings.ErrorCode10223 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10223;
                else if (checkoutForm.ErrorCode == "10224" && this._iyzicoPaymentSettings.ErrorCode10224 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10224;
                else if (checkoutForm.ErrorCode == "10225" && this._iyzicoPaymentSettings.ErrorCode10225 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10225;
                else if (checkoutForm.ErrorCode == "10226" && this._iyzicoPaymentSettings.ErrorCode10226 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10226;
                else if (checkoutForm.ErrorCode == "10227" && this._iyzicoPaymentSettings.ErrorCode10227 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10227;
                else if (checkoutForm.ErrorCode == "10228" && this._iyzicoPaymentSettings.ErrorCode10228 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10228;
                else if (checkoutForm.ErrorCode == "10229" && this._iyzicoPaymentSettings.ErrorCode10229 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10229;
                else if (checkoutForm.ErrorCode == "10230" && this._iyzicoPaymentSettings.ErrorCode10230 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10230;
                else if (checkoutForm.ErrorCode == "10231" && this._iyzicoPaymentSettings.ErrorCode10231 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10231;
                else if (checkoutForm.ErrorCode == "10232" && this._iyzicoPaymentSettings.ErrorCode10232 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10232;
                else if (checkoutForm.ErrorCode == "10233" && this._iyzicoPaymentSettings.ErrorCode10233 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10233;
                else if (checkoutForm.ErrorCode == "10234" && this._iyzicoPaymentSettings.ErrorCode10234 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10234;
                else if (checkoutForm.ErrorCode == "10235" && this._iyzicoPaymentSettings.ErrorCode10235 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10235;
                else if (checkoutForm.ErrorCode == "10236" && this._iyzicoPaymentSettings.ErrorCode10236 != "") modelAlert.ErrorMessage =this._iyzicoPaymentSettings.ErrorCode10236;
                else { modelAlert.ErrorMessage ="Hata oluştu, tekrar deneyiniz."; }
                _logger.Error("Iyzico Error: " + modelAlert.ErrorMessage);
                _logger.Error("Iyzico status" + checkoutForm.Status);
                _logger.Error("Iyzico errorMessage" + checkoutForm.ErrorMessage);
                _logger.Error("Iyzico errorCode" + checkoutForm.ErrorCode);
                modelAlert.ErrorMessage = "<p> Hata: "+modelAlert.ErrorMessage+"</p> <p/> Durum: " + checkoutForm.Status + ".</p> <p> Hata mesajı: " + checkoutForm.ErrorMessage + ".</p> <p> Hata kodu: " + checkoutForm.ErrorCode+".</p>";
                return View("~/Plugins/Payments.Iyzico/Views/AlertView.cshtml", modelAlert);
            
            }


        }
        public virtual ShoppingCartModel GetSubTotals(Customer customer)
        {
            var cart = customer.ShoppingCartItems
             .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
             //.LimitPerStore(_storeContext.CurrentStore.Id)
             .ToList();

            //prepare model
            var model = new ShoppingCartModel();
            model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

            return model;
        }







        //////////////////////////////////////////////////////////////////////////////////7
        protected virtual bool IsMinimumOrderPlacementIntervalValid(Customer customer)
        {
            //prevent 2 orders being placed within an X seconds time frame
            if (_orderSettings.MinimumOrderPlacementInterval == 0)
                return true;

            var lastOrder = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                customerId: _workContext.CurrentCustomer.Id, pageSize: 1)
                .FirstOrDefault();
            if (lastOrder == null)
                return true;

            var interval = DateTime.UtcNow - lastOrder.CreatedOnUtc;
            return interval.TotalSeconds > _orderSettings.MinimumOrderPlacementInterval;
        }

   


        public IActionResult PaymentIyzico()
        {
            return ViewComponent("PaymentIyzico");
        }
    }
}



