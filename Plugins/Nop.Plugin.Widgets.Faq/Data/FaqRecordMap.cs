﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Data.Mapping;
using Nop.Plugin.Widgets.Faq.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Data
{
    public partial class FaqRecordMap : NopEntityTypeConfiguration<FaqRecord>
    {
        public override void Configure(EntityTypeBuilder<FaqRecord> builder)
        {
            builder.ToTable(nameof(FaqRecord));
            builder.HasKey(record => record.Id);

            builder.Property(record => record.Question);
            builder.Property(record => record.Answer);
        }


    }
}
