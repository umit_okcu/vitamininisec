﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Domain
{
    public partial class FaqRecord : BaseEntity
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool Show { get; set; }
        public int Type { get; set; }
        public int OrderNumber { get; set; }
        
    }
}
