﻿using Nop.Core;
using Nop.Plugin.Widgets.Faq.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Services
{
    public partial interface IFaqServices
    {

    
        IPagedList<FaqRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue);

        
        FaqRecord GetById(int faqRecordId);


        void InsertfaqRecord(FaqRecord faqRecord);


        void UpdatefaqRecord(FaqRecord faqRecord);

   
        void DeletefaqRecord(FaqRecord faqRecord);

    }
}
