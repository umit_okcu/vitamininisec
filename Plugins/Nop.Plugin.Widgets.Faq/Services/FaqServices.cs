﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Widgets.Faq.Domain;

using System.Linq;


namespace Nop.Plugin.Widgets.Faq.Services
{
    public partial class FaqServices : IFaqServices
    {

        private const string FAQ_ALL_KEY = "Nop.faq.all-{0}-{1}";
        private const string FAQ_PATTERN_KEY = "Nop.faq.";

        private readonly ICacheManager _cacheManager;
        private readonly IRepository<FaqRecord> _faqRepository;


        public FaqServices(IRepository<FaqRecord> faqRepository,
            ICacheManager cacheManager)
        {
            this._faqRepository = faqRepository;
            this._cacheManager = cacheManager;
        }


        public IPagedList<FaqRecord> GetAll(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var key = string.Format(FAQ_ALL_KEY, pageIndex, pageSize);

            return _cacheManager.Get(key, () =>
            {
                var query = from faq in _faqRepository.Table
                            orderby faq.OrderNumber
                            select faq;

                var records = new PagedList<FaqRecord>(query, pageIndex, pageSize);
                return records;
            });
        }

        public FaqRecord GetById(int faqRecordId)
        {
            if (faqRecordId == 0)
                return null;

            return _faqRepository.GetById(faqRecordId);
        }

        public void InsertfaqRecord(FaqRecord faqRecord)
        {
            if (faqRecord == null)
                throw new ArgumentNullException(nameof(faqRecord));

            _faqRepository.Insert(faqRecord);

            _cacheManager.RemoveByPattern(FAQ_PATTERN_KEY);
        }

        public void UpdatefaqRecord(FaqRecord faqRecord)
        {
            if (faqRecord == null)
                throw new ArgumentNullException(nameof(faqRecord));

            _faqRepository.Update(faqRecord);

            _cacheManager.RemoveByPattern(FAQ_PATTERN_KEY);
        }

        public void DeletefaqRecord(FaqRecord faqRecord)
        {
            if (faqRecord == null)
                throw new ArgumentNullException(nameof(faqRecord));

            _faqRepository.Delete(faqRecord);

            _cacheManager.RemoveByPattern(FAQ_PATTERN_KEY);
        }
    }
}
