﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Widgets.Faq.Models;
using Nop.Plugin.Widgets.Faq.Services;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class FaqController : BasePluginController
    {

        private readonly IFaqServices _faqService;


        public FaqController(IFaqServices faqServices)
        {
            this._faqService = faqServices;
        }

        public IActionResult Configure()
        {
            var model = new List<ConfigurationModel>();

            var rows =_faqService.GetAll();
            foreach (var item in rows)
            {
                var row = new ConfigurationModel() {
                    Answer=item.Answer,
                    Question=item.Question,
                    Id = item.Id,
                    Type=item.Type,
                    Show=item.Show,OrderNumber=item.OrderNumber
                };
                model.Add(row);

            }
            


            return View("~/Plugins/Widgets.Faq/Views/Configure.cshtml", model);
        }


        [HttpPost]
        [AdminAntiForgery]
        public IActionResult Configure(ConfigurationModel model)
        {


            return Configure();
        }



    }
}
