﻿using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Widgets.Faq.Models;
using Nop.Plugin.Widgets.Faq.Services;
using Nop.Web.Framework.Controllers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Controllers
{
    public class CallFaqComponentController : BasePluginController
    {
        private readonly IFaqServices _faqService;



        public CallFaqComponentController(IFaqServices faqServices) { 
                     this._faqService = faqServices;
        }

        public IActionResult Faq()
        {
            var all = _faqService.GetAll();
            List<ConfigurationModel> model = new List<ConfigurationModel>();
            foreach (var item in all)
            {
                if ((FaqMode)item.Type == FaqMode.Faq&&item.Show==true)
                {
                    ConfigurationModel newRow = new ConfigurationModel()
                {
                    Answer = item.Answer,
                    Question = item.Question,
                    Type = item.Type,
                    Id = item.Id,
                    Show = item.Show,OrderNumber=item.OrderNumber

                };

                    model.Add(newRow);
                }

            }
            

            return View("~/Plugins/Widgets.Faq/Views/Faq.cshtml",model);
        }

        public IActionResult SupportCenter()
        {


            var all = _faqService.GetAll();
            List<ConfigurationModel> model = new List<ConfigurationModel>();
            foreach (var item in all)
            {
                if ((FaqMode)item.Type == FaqMode.SupportCenter && item.Show == true)
                {
                    ConfigurationModel newRow = new ConfigurationModel()
                    {
                        Answer = item.Answer,
                        Question = item.Question,
                        Type = item.Type,
                        Id = item.Id,
                        Show = item.Show,OrderNumber=item.OrderNumber

                    };

                    model.Add(newRow);
                }

            }



            return View("~/Plugins/Widgets.Faq/Views/Faq.cshtml", model);
        }

    }
}
