﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Plugin.Widgets.Faq.Data;
using Nop.Web.Framework.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;




namespace Nop.Plugin.Widgets.Faq.Infrastructure
{
    public class PluginDbStartup : INopStartup
    {
        public int Order => 12;

        public void Configure(IApplicationBuilder application)
        {
            
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<FaqObjectContext>(optionsBuilder => {
                optionsBuilder.UseSqlServerWithLazyLoading(services);
            });
        }
    }
}
