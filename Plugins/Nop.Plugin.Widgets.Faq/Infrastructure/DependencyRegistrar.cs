﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Widgets.Faq.Data;
using Nop.Plugin.Widgets.Faq.Domain;
using Nop.Plugin.Widgets.Faq.Services;
using Nop.Web.Framework.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Infrastructure
{
    class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 1;

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            builder.RegisterType<FaqServices>().As<IFaqServices>().InstancePerLifetimeScope();

            //data context
            builder.RegisterPluginDataContext<FaqObjectContext>("nop_object_context_faq_zip");

            //override required repository with our custom context
            builder.RegisterType<EfRepository<FaqRecord>>().As<IRepository<FaqRecord>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_faq_zip"))
                .InstancePerLifetimeScope();
        }
    }
}
