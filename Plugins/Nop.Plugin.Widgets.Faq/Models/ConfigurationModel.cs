﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq.Models
{
    public class ConfigurationModel : BaseNopModel
    {

        [NopResourceDisplayName("Plugins.Widgets.Faq.Question")]
        public string Question { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.Faq.Answer")]
        public string Answer { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.Faq.Show")]
        public bool Show { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.Faq.Type")]
        public int Type { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.Faq.Id")]
        public int Id { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.Faq.OrderNumber")]
        public int OrderNumber { get; set; }
       
    }
}
