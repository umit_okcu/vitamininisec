﻿using Nop.Core;
using Nop.Core.Plugins;
using Nop.Plugin.Widgets.Faq.Data;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq
{
    public class FaqPlugin : BasePlugin, IWidgetPlugin
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;
        private readonly FaqObjectContext _faqObjectContext;
        #endregion

        #region Ctor

        public FaqPlugin(FaqObjectContext faqObjectContext,
            ILocalizationService localizationService,
            IWebHelper webHelper,
            ISettingService settingService)
        {
            this._faqObjectContext = faqObjectContext;
            this._localizationService = localizationService;
            this._webHelper = webHelper;
            this._settingService = settingService;
        }
        #endregion

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsFaq";
        }

        public IList<string> GetWidgetZones()
        {
            throw new NotImplementedException();
        }

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/WidgetsFaq/Configure";
        }

        public override void Install()
        {
            _faqObjectContext.Install();

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Question", "Soru");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Question.Hint", "Müş¸terilerinizi şarj etmek için ek ücret girin.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Answer", "Ek ücret. Kullanım yüzdesi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Answer.Hint", "Sipariş¸ toplamına yüzde ek bir ücret uygulayıp uygulamayacağınızı belirler. Etkinleş¸tirilmemiş¸se, sabit bir değer kullanılır.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Type", "Ek ücret");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Type.Hint", "Müş¸terilerinizi şarj etmek için ek ücret girin.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Show", "Ek ücret. Kullanım yüzdesi");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.Faq.Show.Hint", "Sipariş¸ toplamına yüzde ek bir ücret uygulayıp uygulamayacağınızı belirler. Etkinleş¸tirilmemiş¸se, sabit bir değer kullanılır.");



            base.Install();
        }

        public override void Uninstall()
        {
            _faqObjectContext.Uninstall();

            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Question");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Question.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Answer");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Answer.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Type");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Type.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Show");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.Faq.Show.Hint");



            base.Uninstall();

        }



        }
    }
