﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.Faq
{
    public class RouteProvider : IRouteProvider
    {

        public int Priority
        {
            get
            {
                return 0;
            }
        }
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Plugin.Payments.Widgets.Faq", "SSS",
          new { controller = "CallFaqComponent", action = "Faq" }  
     );
            routeBuilder.MapRoute("Plugin.Payments.Widgets.SupportCenter", "destekmerkezi",
new { controller = "CallFaqComponent", action = "SupportCenter" }
);
        }
    }
}
