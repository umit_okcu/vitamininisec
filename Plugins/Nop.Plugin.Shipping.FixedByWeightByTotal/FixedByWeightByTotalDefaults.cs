﻿
namespace Nop.Plugin.Shipping.FixedByWeightByTotal
{
    /// <summary>
    /// Represents constants of the "Fixed or by weight" shipping plugin
    /// </summary>
    public static class FixedByWeightByTotalDefaults
    {
        /// <summary>
        /// The key of the settings to save fixed rate of the shipping method
        /// </summary>
        public const string FixedRateSettingsKey = "ShippingRateComputationMethod.FixedByWeightByTotal.Rate.ShippingMethodId{0}";
        /// <summary>
        /// Name of the renew access token schedule task
        /// </summary>
        public static string RenewShippingTaskName => "Shipping Management";

        /// <summary>
        /// Type of the renew access token schedule task
        /// </summary>
        public static string RenewShippingTask => "Nop.Plugin.Shipping.FixedByWeightByTotal.Services.ShippingScheduledTask";

        /// <summary>
        /// Default access token renewal period in days
        /// </summary>
        public static int AccessTokenRenewalPeriodRecommended => 12;
    }
}
