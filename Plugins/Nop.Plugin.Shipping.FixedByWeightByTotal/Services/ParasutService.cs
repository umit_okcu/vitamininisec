﻿using Newtonsoft.Json;
using Nop.Core;
using Nop.Plugin.Shipping.FixedByWeightByTotal.Models;
using Nop.Services.Configuration;
using Nop.Services.Stores;
using Nop.Core.Domain.Customers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Services
{
    public class ParasutService
    {


        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public ParasutService(ISettingService settingService, IStoreContext storeContext)
        {
            this._settingService = settingService;
            this._storeContext = storeContext;
        }

        public async Task<dynamic> CheckIfInvoicePrinted(string invoiceId)
        {
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);
            var token = GetToken();
            string _token = token.Result.ToString();
            string endPoint = IyzicoPaymentSettings.ParasutBaseApi + "/" + IyzicoPaymentSettings.ParasutCompanyId;   /////// Create Parasut api EndPoint////
            string account_id = IyzicoPaymentSettings.ParasutBankAccountId;//   

            var checkInvoice =  await GetInvoice(_token,endPoint,account_id,invoiceId);





            return checkInvoice;
        }



        public async Task<string> GetInvoice(string token, string endPoint, string account_id,string invoiceId)
        {
        var storeScope = _storeContext.ActiveStoreScopeConfiguration;
        var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);



        System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            string url = endPoint + "/sales_invoices/" + invoiceId.ToString();
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //   client2.DefaultRequestHeaders.Add("Content-Type", "application/json");

            //  stringContent.Headers.Add
            //  HttpResponseMessage response2 = client2.PostAsync("", stringContent).Result;
            var response2 = await client2.GetAsync(url);//.Result;
            var a = response2.Content.ReadAsStringAsync().Result.Trim();

            var responseModel = JsonConvert.DeserializeObject<dynamic>(a);

            if (response2.IsSuccessStatusCode != false)
            {


                return a;


            }
            else
            {
                return "error";
            }




        }
        public  async Task<string> GetToken()
        {
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;// this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(storeScope);
            if (IyzicoPaymentSettings.UseParasut == true)
            {

                string uri = IyzicoPaymentSettings.ParasutBaseApiToken;
            string clientId=IyzicoPaymentSettings.ParasutClientId;
            string userMail =IyzicoPaymentSettings.ParasutUserName;
            string password = IyzicoPaymentSettings.ParasutPassword;

            string Token_Url = uri + "?client_id=" + clientId + "&username=" + userMail + "&password=" + password + "&grant_type=password&redirect_uri=urn:ietf:wg:oauth:2.0:oob";

            var stringContent = new StringContent("");
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            using (HttpResponseMessage response = await client.PostAsync(Token_Url, stringContent))
            using (HttpContent content = response.Content)
            {
                string data = await content.ReadAsStringAsync();

                if (data != null)
                {

                    // ...   
                    dynamic json = JsonConvert.DeserializeObject(data);

                    var a = json.access_token;


                    return a.ToString();
                }


            }
            }
            return "error";


        }

        public async Task<string> GetContacts(string baseUri, string token)
        {

            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);//.Add("application/json");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
                HttpResponseMessage response = await client.GetAsync(baseUri + "/contacts");
                HttpContent content = response.Content;


                string data = await content.ReadAsStringAsync();

                if (data != null)
                {

                    return data.ToString();
                }
            }


            return "";
        }
        public async Task<string> GetProducts(string baseUri, string token)
        {

            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);//.Add("application/json");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
                HttpResponseMessage response = await client.GetAsync(baseUri + "/products");
                HttpContent content = response.Content;


                string data = await content.ReadAsStringAsync();

                if (data != null)
                {

                    return data.ToString();
                }
            }


            return "";
        }





    //    public async Task<string> GetToken(string uri, string clientId, string userMail, string password)
    //    {
    //
    //        string Token_Url = uri + "?client_id=" + clientId + "&username=" + userMail + "&password=" + password + "&grant_type=password&redirect_uri=urn:ietf:wg:oauth:2.0:oob";
    //
    //        var stringContent = new StringContent("");
    //        using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
    //        using (HttpResponseMessage response = await client.PostAsync(Token_Url, stringContent))
    //        using (HttpContent content = response.Content)
    //        {
    //            string data = await content.ReadAsStringAsync();
    //
    //            if (data != null)
    //            {
    //
    //                // ...   
    //                dynamic json = JsonConvert.DeserializeObject(data);
    //
    //                var a = json.access_token;
    //
    //
    //                return a.ToString();
    //            }
    //
    //
    //        }
    //        return "error";
    //
    //
    //    }
    //



        public async Task<string> CreateCustomer(string token, string endPoint, Customer customer)
        {
            string url = endPoint + "/contacts";

            System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            ParasutContactsModel parasutContactsModel = new ParasutContactsModel();

            ParasutContactsModel.Data parasutData = new ParasutContactsModel.Data();


            parasutData.id = "";
            parasutData.type = "contacts";

            ParasutContactsModel.Attributes parasutAttributes = new ParasutContactsModel.Attributes();

            parasutAttributes.email = customer.BillingAddress.Email;
            parasutAttributes.name = customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName;

            if (customer.BillingAddress.IsEnterprice == true)
            {
                parasutAttributes.contact_type = "company";
                parasutAttributes.tax_number = customer.BillingAddress.VKN;
                parasutAttributes.tax_office = customer.BillingAddress.VD;
            }
            else
            {
                parasutAttributes.contact_type = "person";
                parasutAttributes.tax_number = customer.BillingAddress.TCKN;
            }
            parasutAttributes.district = customer.BillingAddress.City;
            parasutAttributes.city = customer.BillingAddress.StateProvince.Name;
            parasutAttributes.address = customer.BillingAddress.Address1 + customer.BillingAddress.Address2;//"asdasd";
            parasutAttributes.phone = customer.BillingAddress.PhoneNumber;
            parasutAttributes.archived = false;
            parasutAttributes.account_type = "customer";

            parasutAttributes.short_name = customer.BillingAddress.FirstName;




            parasutAttributes.fax = customer.BillingAddress.FaxNumber;
            parasutAttributes.is_abroad = false;

            parasutAttributes.iban = "";







            ParasutContactsModel.Relationships parasutRelationships = new ParasutContactsModel.Relationships();

            ParasutContactsModel.Category parasutCategory = new ParasutContactsModel.Category();

            ParasutContactsModel.Data1 parasutCategoryData = new ParasutContactsModel.Data1();

            parasutCategoryData.id = "";// "item_kategori_no";
            parasutCategoryData.type = "item_categories";// "item_categories";

            parasutCategory.data = parasutCategoryData;

            parasutRelationships.category = parasutCategory;

            parasutContactsModel.data = parasutData;

            parasutData.attributes = parasutAttributes;
            parasutData.relationships = parasutRelationships;


            var json = JsonConvert.SerializeObject(parasutContactsModel);

            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response2 = await client2.PostAsync("", stringContent);

            var _response = response2.Content.ReadAsStringAsync().Result.Trim();
            var responseModel = JsonConvert.DeserializeObject<dynamic>(_response);

            if (response2.IsSuccessStatusCode)
            {


                return _response;


            }
            else
            {
                return "error";
            }




        }


        public async Task<string> CreateProduct(string endPoint, string token, BasketItem product)
        {








            ParasutProductsModel parasutProductsModel = new ParasutProductsModel();

            ParasutProductsModel.Data parasutData = new ParasutProductsModel.Data();


            parasutData.id = "";
            parasutData.type = "products";

            ParasutProductsModel.Attributes parasutAttributes = new ParasutProductsModel.Attributes();

            decimal _price = Convert.ToDecimal(product.SubMerchantPrice);
            decimal list_price = _price / 1.18m;   ///1.18x=total =>  x = total *100/118

            decimal net_price = Math.Round(list_price, 5);
            parasutAttributes.code = product.Id;//should edit
            parasutAttributes.name = product.Name;
            parasutAttributes.vat_rate = 18.00m;
            parasutAttributes.sales_excise_duty = 0.00m;
            parasutAttributes.sales_excise_duty_type = "percentage";
            parasutAttributes.purchase_excise_duty = 0;
            parasutAttributes.purchase_excise_duty_type = "percentage";
            parasutAttributes.unit = "";
            parasutAttributes.communications_tax_rate = 0;
            parasutAttributes.archived = false;
            parasutAttributes.list_price = net_price;// 111.98m;


            parasutAttributes.currency = "TRL";



            parasutAttributes.buying_price = net_price;// _price; //142;
            parasutAttributes.buying_currency = "TRL";
            parasutAttributes.inventory_tracking = false;
            parasutAttributes.initial_stock_count = 0;






            ParasutProductsModel.Relationships parasutRelationships = new ParasutProductsModel.Relationships();

            ParasutProductsModel.Category parasutCategory = new ParasutProductsModel.Category();

            ParasutProductsModel.Data1 parasutCategoryData = new ParasutProductsModel.Data1();

            parasutCategoryData.id = "";// "item_kategori_no";
            parasutCategoryData.type = "";// "item_categories";

            parasutCategory.data = parasutCategoryData;

            parasutRelationships.category = parasutCategory;

            parasutProductsModel.data = parasutData;

            parasutData.attributes = parasutAttributes;
            //parasutData.relationships = parasutRelationships;


            var json = JsonConvert.SerializeObject(parasutProductsModel);

            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");





            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();

            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);//.Add("application/json");


            string url = endPoint + "/products";

            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var response2 = await client.PostAsync("", stringContent);
            var _response = response2.Content.ReadAsStringAsync().Result.Trim();
            var responseModel = JsonConvert.DeserializeObject<dynamic>(_response);
            if (response2.IsSuccessStatusCode)
            {


                return _response;


            }
            else
            {
                return "error";
            }







        }


        public async Task<string> CreateInvoice(string token, string endPoint, List<string> createdParasutProducts, string CustomerId, Customer customer, decimal orderTotalDiscountAmountBase, List<BasketItem> basketItems)
        {






            ParasutInvoicesModel.RootObject parasutInvoicesModel = new ParasutInvoicesModel.RootObject();

            ParasutInvoicesModel.Data parasutData = new ParasutInvoicesModel.Data();


            parasutData.id = "";
            parasutData.type = "sales_invoices";

            ParasutInvoicesModel.Attributes parasutAttributes = new ParasutInvoicesModel.Attributes();
            var date = DateTime.Now;
            var conversationId = customer.Id + "-" + date + "-" + customer.CustomerGuid;
            var _formatedDate = String.Format("{0:yyyy-MM-dd HH:mm}", date);
            parasutAttributes.item_type = "invoice";
            parasutAttributes.description = conversationId;
            parasutAttributes.issue_date = String.Format("{0:yyyy-MM-dd HH:mm}", date); //"2018-10-08";//datetime 2018-10-04
            parasutAttributes.due_date = _formatedDate; //"2018-10-08";//datetime 2018-10-04
            parasutAttributes.invoice_series = null;//"";
            parasutAttributes.invoice_id = null;
            parasutAttributes.currency = "TRL";
            parasutAttributes.exchange_rate = 1.00m;
            parasutAttributes.withholding_rate = 0.00m;
            parasutAttributes.vat_withholding_rate = 0.00m;
            parasutAttributes.invoice_discount_type = "amount"; //"amount"
            decimal discountWithoutTax = orderTotalDiscountAmountBase / 1.18m; /// discart KDV
            parasutAttributes.invoice_discount = discountWithoutTax;
            parasutAttributes.billing_address = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;
            parasutAttributes.billing_phone = customer.BillingAddress.PhoneNumber;//"5551111122";
            parasutAttributes.billing_fax = customer.BillingAddress.FaxNumber;// "";


            if (customer.BillingAddress.IsEnterprice == true)
            {

                parasutAttributes.tax_number = customer.BillingAddress.VKN;
                parasutAttributes.tax_office = customer.BillingAddress.VD;
            }
            else
            {

                parasutAttributes.tax_number = customer.BillingAddress.TCKN;
            }




            parasutAttributes.city = customer.BillingAddress.StateProvince.Name;

            parasutAttributes.district = customer.BillingAddress.City;
            parasutAttributes.is_abroad = false;
            parasutAttributes.order_no = "";//"";
            parasutAttributes.order_date = _formatedDate; //datetime 2018-10-04
            parasutAttributes.shipment_addres = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;






            ///////////Add cart items to invoice relationships details ///
            List<ParasutInvoicesModel.Datum> CartItems = new List<ParasutInvoicesModel.Datum>();
            var item_quantity = createdParasutProducts.Count;

            for (int i = 0; i < item_quantity; i++)
            {
                var productResponse = JsonConvert.DeserializeObject<dynamic>(createdParasutProducts[i]);
                if (item_quantity > 1)
                {


                    string aa = "1";
                    string b = "2";
                    string c = "1";
                    string d = " 2";
                    string e = "1";
                    string f = "2";
                    if (i >= 1)
                    {

                        var productResponseBefore = JsonConvert.DeserializeObject<dynamic>(createdParasutProducts[i - 1]);

                        aa = productResponse.data.attributes.list_price.ToString();
                        b = productResponseBefore.data.attributes.list_price.ToString();
                        c = productResponse.data.attributes.name.ToString();
                        d = productResponseBefore.data.attributes.name.ToString();
                        e = productResponse.data.attributes.buying_price.ToString();
                        f = productResponseBefore.data.attributes.buying_price.ToString();
                    }



                    if (i >= 1 && (aa == b) && (c == d) && (e == f))
                    {



                        var itemCount = CartItems.Count;

                        var item = CartItems[itemCount - 1];

                        var quantity = item.attributes.quantity;
                        quantity++;
                        item.attributes.quantity = quantity;
                        decimal discount = item.attributes.discount_value;
                        item.attributes.discount_value = (discount / (quantity - 1)) * quantity;




                        CartItems[itemCount - 1] = item;

                    }
                    else
                    {
                        decimal _price = Convert.ToDecimal(basketItems[i].Price);


                        decimal net_price = Math.Round(_price, 4) / 1.18m;
                        decimal discount = 0.00m;
                        if (basketItems[i].Price != basketItems[i].SubMerchantPrice)
                        {
                            discount = Convert.ToDecimal(basketItems[i].Price) - Convert.ToDecimal(basketItems[i].SubMerchantPrice);
                            discount = discount / 1.18m;
                        }
                        decimal net_discount = Math.Round(discount, 4);//Math.Round(discount, 5); 


                        ParasutInvoicesModel.Attributes2 parasutCartItemAtributes = new ParasutInvoicesModel.Attributes2();
                        parasutCartItemAtributes.net_total = net_price;//buying_price;// 210.04m;
                        parasutCartItemAtributes.quantity = 1;
                        parasutCartItemAtributes.unit_price = net_price;
                        parasutCartItemAtributes.vat_rate = 18;
                        parasutCartItemAtributes.discount_type = "amount";//"amount"


                        parasutCartItemAtributes.discount_value = net_discount;//discounts[i];// 0;// "0.00";


                        parasutCartItemAtributes.excise_duty_type = "amount"; //"amount"
                        parasutCartItemAtributes.excise_duty_value = 0;// "0.00";
                        parasutCartItemAtributes.communications_tax_rate = 0;// "0.00";
                        parasutCartItemAtributes.description = "";





                        /////// Add Details data  relationships /////////
                        ParasutInvoicesModel.Relationships2 parasutCartItemRelationships = new ParasutInvoicesModel.Relationships2();
                        ParasutInvoicesModel.Product product = new ParasutInvoicesModel.Product();
                        ParasutInvoicesModel.Data2 productData = new ParasutInvoicesModel.Data2();
                        productData.id = productResponse.data.id;
                        productData.type = "products";

                        product.data = productData;



                        parasutCartItemRelationships.product = product;


                        ParasutInvoicesModel.Datum singleCartItem = new ParasutInvoicesModel.Datum();

                        //////add item relationships for sales_invoice_details/////
                        singleCartItem.relationships = parasutCartItemRelationships;
                        //////add item atributes for sales_invoice_details/////
                        singleCartItem.attributes = parasutCartItemAtributes;

                        singleCartItem.id = "";
                        singleCartItem.type = "sales_invoice_details";

                        CartItems.Add(singleCartItem);
                    }
                }
                else
                {
                    decimal _price = Convert.ToDecimal(basketItems[i].Price);


                    decimal net_price = Math.Round(_price, 4) / 1.18m;
                    decimal discount = 0.00m;
                    if (basketItems[i].Price != basketItems[i].SubMerchantPrice)
                    {
                        discount = Convert.ToDecimal(basketItems[i].Price) - Convert.ToDecimal(basketItems[i].SubMerchantPrice);
                        discount = Math.Round(discount, 4) / 1.18m;
                    }
                    decimal net_discount = Math.Round(discount, 5);
                    ParasutInvoicesModel.Attributes2 parasutCartItemAtributes = new ParasutInvoicesModel.Attributes2();
                    parasutCartItemAtributes.net_total = net_price;//buying_price;// 210.04m;
                    parasutCartItemAtributes.quantity = 1;
                    parasutCartItemAtributes.unit_price = net_price;
                    parasutCartItemAtributes.discount_type = "amount";//"amount"
                    parasutCartItemAtributes.discount_value = net_discount;//discounts[i];// 0;// "0.00";
                    parasutCartItemAtributes.excise_duty_type = "amount"; //"amount"
                    parasutCartItemAtributes.excise_duty_value = 0;// "0.00";
                    parasutCartItemAtributes.communications_tax_rate = 0;// "0.00";
                    parasutCartItemAtributes.description = "";



                    /////// Add Details data  relationships /////////
                    ParasutInvoicesModel.Relationships2 parasutCartItemRelationships = new ParasutInvoicesModel.Relationships2();
                    ParasutInvoicesModel.Product product = new ParasutInvoicesModel.Product();
                    ParasutInvoicesModel.Data2 productData = new ParasutInvoicesModel.Data2();
                    productData.id = productResponse.data.id;
                    productData.type = "products";

                    product.data = productData;



                    parasutCartItemRelationships.product = product;


                    ParasutInvoicesModel.Datum singleCartItem = new ParasutInvoicesModel.Datum();

                    //////add item relationships for sales_invoice_details/////
                    singleCartItem.relationships = parasutCartItemRelationships;
                    //////add item atributes for sales_invoice_details/////
                    singleCartItem.attributes = parasutCartItemAtributes;

                    singleCartItem.id = "";
                    singleCartItem.type = "sales_invoice_details";

                    CartItems.Add(singleCartItem);
                }
            }






            ParasutInvoicesModel.Details parasutInvoiceDetails = new ParasutInvoicesModel.Details();
            parasutInvoiceDetails.data = CartItems;

            ////////////// Add Contact information to Invoive relationships////
            ParasutInvoicesModel.Contact invoiceContact = new ParasutInvoicesModel.Contact();
            ParasutInvoicesModel.Data3 contactData = new ParasutInvoicesModel.Data3();
            contactData.id = CustomerId;//"10606288";
            contactData.type = "contacts";

            invoiceContact.data = contactData;


            /////////////Add Tag Information////////////

            ParasutInvoicesModel.Tags invoceTags = new ParasutInvoicesModel.Tags();


            List<ParasutInvoicesModel.Datum2> invoiceTagsDataList = new List<ParasutInvoicesModel.Datum2>();

            ParasutInvoicesModel.Datum2 invoiceTagsData = new ParasutInvoicesModel.Datum2();
            invoiceTagsData.id = "string";
            invoiceTagsData.type = "tags";
            invoiceTagsDataList.Add(invoiceTagsData);
            invoceTags.data = invoiceTagsDataList;







            ///////////////Add category Information/////////////

            ParasutInvoicesModel.Category parasutCategory = new ParasutInvoicesModel.Category();

            ParasutInvoicesModel.Data4 parasutCategoryData = new ParasutInvoicesModel.Data4();

            parasutCategoryData.id = "";// "item_kategori_no";
            parasutCategoryData.type = "item_categories";// "item_categories";
            parasutCategory.data = parasutCategoryData;

            ////////////// Put together Relationships /////////////////


            ParasutInvoicesModel.Relationships parasutRelationships = new ParasutInvoicesModel.Relationships();

            parasutRelationships.category = parasutCategory;
            parasutRelationships.details = parasutInvoiceDetails;
            parasutRelationships.contact = invoiceContact;
            //parasutRelationships.tags = invoceTags;



            parasutInvoicesModel.data = parasutData;

            parasutData.attributes = parasutAttributes;
            parasutData.relationships = parasutRelationships;


            var json = JsonConvert.SerializeObject(parasutInvoicesModel);

            System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");

            string url = endPoint + "/sales_invoices";
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //   client2.DefaultRequestHeaders.Add("Content-Type", "application/json");
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            //  stringContent.Headers.Add
            //  HttpResponseMessage response2 = client2.PostAsync("", stringContent).Result;
            var response2 = await client2.PostAsync("", stringContent);//.Result;
            var a = response2.Content.ReadAsStringAsync().Result.Trim();

            var responseModel = JsonConvert.DeserializeObject<dynamic>(a);

            if (response2.IsSuccessStatusCode != false)
            {


                return a;


            }
            else
            {
                return "error";
            }




        }


        public async Task<string> PayInvoice(string token, string endPoint, int account_id, decimal amountToPay, int invoiceId, string iyzicoPaymenId)
        {

            ParasutPayInvoiceModel.PayInvoice payInvoice = new ParasutPayInvoiceModel.PayInvoice();


            ParasutPayInvoiceModel.Attributes payInvoiceAttributes = new ParasutPayInvoiceModel.Attributes();
            payInvoiceAttributes.account_id = account_id;
            payInvoiceAttributes.description = iyzicoPaymenId.ToString();
            payInvoiceAttributes.date = DateTime.Now.ToString("yyyy-MM-dd");
            payInvoiceAttributes.exchange_rate = 1;
            payInvoiceAttributes.amount = amountToPay;


            ParasutPayInvoiceModel.Data payInvoiceData = new ParasutPayInvoiceModel.Data();
            payInvoiceData.id = "";
            payInvoiceData.type = "payments";
            payInvoiceData.attributes = payInvoiceAttributes;


            payInvoice.data = payInvoiceData;


            var json = JsonConvert.SerializeObject(payInvoice);

            System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            string url = endPoint + "/sales_invoices/" + invoiceId.ToString() + "/payments";
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //   client2.DefaultRequestHeaders.Add("Content-Type", "application/json");
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            //  stringContent.Headers.Add
            //  HttpResponseMessage response2 = client2.PostAsync("", stringContent).Result;
            var response2 = await client2.PostAsync("", stringContent);//.Result;
            var a = response2.Content.ReadAsStringAsync().Result.Trim();

            var responseModel = JsonConvert.DeserializeObject<dynamic>(a);

            if (response2.IsSuccessStatusCode != false)
            {


                return a;


            }
            else
            {
                return "error";
            }




        }




    }
}
