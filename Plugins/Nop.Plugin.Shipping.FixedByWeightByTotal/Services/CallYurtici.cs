﻿using Newtonsoft.Json;
using Nop.Core;

using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YurticiService;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Services
{

    public class CallYurtici
    {
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly IShipmentService _shipmentService;
        private readonly IShippingService _shippingService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly FixedByWeightByTotalSettings _fixedByWeightByTotalSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;

     


        private readonly string userNameGO = "KAMPOTUGO";
        private readonly string passwordGO = "94MA8871J028Xswn";

        private readonly string userNameAO = "KAMPOTUAO";
        private readonly string passwordAO = "v192Ah8Ux2zXBhC1";
        private readonly string ProjeMusteriKodu = "470539487";
        private readonly string lang = "TR";







        #endregion

        #region Ctor

        public CallYurtici(ILocalizationService localizationService,
            ILogger logger,
            IPaymentService paymentService,
            ISettingService settingService,
            IShipmentService shipmentService,
            IShippingService shippingService,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
            IGenericAttributeService genericAttributeService,
            IWorkContext workContext,
            IStoreContext storeContext,
            FixedByWeightByTotalSettings fixedByWeightByTotalSettings
      
        )
        {
            this._localizationService = localizationService;
            this._logger = logger;
            this._paymentService = paymentService;
            this._settingService = settingService;
            this._shipmentService = shipmentService;
            this._shippingService = shippingService;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._genericAttributeService = genericAttributeService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._fixedByWeightByTotalSettings = fixedByWeightByTotalSettings;
            


            this.userNameGO = _fixedByWeightByTotalSettings.YurticiGOUserName;
            this.userNameAO = _fixedByWeightByTotalSettings.YurticiAOUserName;
            this.passwordGO = _fixedByWeightByTotalSettings.YurticiGOPassword;
            this.passwordAO = _fixedByWeightByTotalSettings.YurticiAOPassword;
            this.ProjeMusteriKodu = _fixedByWeightByTotalSettings.YurticiMusteriKodu;




    }
        #endregion

        

        




    public async Task<dynamic> SetOrderGO(Core.Domain.Orders.Order _order,string invoiceBillNumber)
        {
            Random rnd = new Random();
            string _cargoKey = string.Empty;
            for (int i = 0; i < 15; i++)
                _cargoKey = string.Concat(_cargoKey, rnd.Next(10).ToString());

            _logger.Error("Shipments | SetOrderGO Started For OrderId: "+_order.Id+" cargo key :"+ _cargoKey+" | shippingAdressId: "+_order.ShippingAddress.Id.ToString()+" | InvoiceBillnumber: "+ invoiceBillNumber+"| Phone number: "+_order.ShippingAddress.PhoneNumber, null,_order.Customer);


            ShippingOrderVO[] shippingOrderVO = new ShippingOrderVO[1];
            ShippingOrderVO shipping = new ShippingOrderVO();

            shipping.cargoKey = _cargoKey;
            shipping.invoiceKey = invoiceBillNumber;

            string name = RemoveAccent(_order.ShippingAddress.FirstName + " " + _order.ShippingAddress.LastName);

            if (name.Length <= 5)
            {
                name += " .  .  . ";
            }

            shipping.receiverCustName = name;


            string address = RemoveAccent(_order.ShippingAddress.Address1 + " " + _order.ShippingAddress.Address2);
            if (address.Length <= 10)
            {
                address += RemoveAccent(_order.ShippingAddress.StateProvince.Name);
                if (address.Length <= 10)
                {
                    address += RemoveAccent(_order.ShippingAddress.Country.Name);
                    if (address.Length <= 10)
                    {
                        address += " Telefon ile kontrol ediniz.";
                    }
                }

            }



            shipping.receiverAddress = address;



            string Phone = _order.ShippingAddress.PhoneNumber;

            string resultPhoneString = Phone.Replace("(", null).Replace(")", null).Replace(" ", null).Trim();

           

            int index = 0;
            while (resultPhoneString.StartsWith("0") || resultPhoneString.StartsWith("0"))
            {
                resultPhoneString = resultPhoneString.Substring(1);
                index++;
                if (index > 15)
                {
                    break;
                }
               
            }
            index = 0;
         
            while (resultPhoneString.Length < 10)
            {
              
                resultPhoneString = "5"+ resultPhoneString;
               
                index++;
                if (index > 15)
                {
                    break;
                }
            }





            _logger.Error("Shipments | SetOrderGO Phone number: " + resultPhoneString);
            shipping.receiverPhone1 = resultPhoneString;

            shipping.cityName = RemoveAccent(_order.ShippingAddress.StateProvince.Name);
            shipping.townName = RemoveAccent(_order.ShippingAddress.City);
            shipping.waybillNo = invoiceBillNumber;//irsaliye  numarası;


           

            shippingOrderVO[0] = shipping;
            createShipment create = new createShipment();
            create.userLanguage = lang;
            create.wsUserName = userNameGO;
            create.wsPassword = passwordGO;
            create.ShippingOrderVO = shippingOrderVO;


            ShippingOrderDispatcherServicesClient client = new ShippingOrderDispatcherServicesClient();
            dynamic result = await client.createShipmentAsync(create);

            _logger.Error("Shipments | SetOrderGO Response"+ result.createShipmentResponse.ShippingOrderResultVO.outResult + " For OrderId: " + _order.Id + " cargo key :" + _cargoKey , null, _order.Customer);
            //var isSuccess = ddsa.createShipmentResponse.ShippingOrderResultVO.outFlag == "0";
            //var response = ddsa.createShipmentResponse.ShippingOrderResultVO.shippingOrderDetailVO;
            //var asdas = response[0].cargoKey;
            //var asdasa = response[0].errCode;
            //var asdass = response[0].errCodeSpecified;

            //var asdads = response[0].invoiceKey;
            //var asdafs = response[0].errMessage;



            //var responseModel = JsonConvert.SerializeObject(ddsa);//<dynamic>(_response);

            client.Close();

            //SetOrderResponse orderresponse =   await arasCargoService.SetOrderAsync(setOrderRequest);//SetOrderAsync(orderInfos, orderInfo.UserName, orderInfo.Password);
            return result;
        }


        public async Task<queryShipmentResponse1> GetQueryShipments( string [] _keys)
        {

            queryShipmentDetail asd = new queryShipmentDetail();

            queryShipment _queryShipment = new queryShipment();

            _queryShipment.wsUserName = userNameGO;
            _queryShipment.wsPassword = passwordGO;
            _queryShipment.wsLanguage = lang;
            _queryShipment.keyType = 0;
            string[] keys = _keys;


            _queryShipment.keys = keys;
            _queryShipment.addHistoricalData = true;

            queryShipmentResponse1 aaasd = new queryShipmentResponse1();
         
            ShippingOrderDispatcherServicesClient client = new ShippingOrderDispatcherServicesClient();
            var ddsa = await client.queryShipmentAsync(_queryShipment);//createShipmentAsync(create);


         

            return ddsa;
        }

        //public async Task<dynamic> TestDiscover()
        //{

        //    ShippingOrderDispatcherServicesClient client = new ShippingOrderDispatcherServicesClient();
        //    //   client.queryShipmentDetailAsync


        //    return "asd";
        //}

        public static string RemoveAccent(string text)
        {


            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(text);
            return System.Text.Encoding.ASCII.GetString(bytes);

        }

    }
}
