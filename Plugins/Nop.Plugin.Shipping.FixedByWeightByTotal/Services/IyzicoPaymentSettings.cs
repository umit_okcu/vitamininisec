﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Services
{
    public class IyzicoPaymentSettings : ISettings
    {

        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public bool UseSandbox { get; set; }
        public string APIUrl { get; set; }
        public string APIUrlSandbox { get; set; }

        public TransactMode TransactMode { get; set; }
        public bool AdditionalFeePercentage { get; set; }
        public decimal AdditionalFee { get; set; }

        public string ErrorCode10051 { get; set; }
        public string ErrorCode10005 { get; set; }
        public string ErrorCode10012 { get; set; }
        public string ErrorCode10041 { get; set; }
        public string ErrorCode10043 { get; set; }
        public string ErrorCode10054 { get; set; }
        public string ErrorCode10084 { get; set; }
        public string ErrorCode10057 { get; set; }
        public string ErrorCode10058 { get; set; }
        public string ErrorCode10034 { get; set; }
        public string ErrorCode10093 { get; set; }
        public string ErrorCode10201 { get; set; }
        public string ErrorCode10202 { get; set; }
        public string ErrorCode10203 { get; set; }
        public string ErrorCode10204 { get; set; }
        public string ErrorCode10205 { get; set; }
        public string ErrorCode10206 { get; set; }
        public string ErrorCode10207 { get; set; }
        public string ErrorCode10208 { get; set; }
        public string ErrorCode10209 { get; set; }
        public string ErrorCode10210 { get; set; }
        public string ErrorCode10211 { get; set; }
        public string ErrorCode10212 { get; set; }
        public string ErrorCode10213 { get; set; }
        public string ErrorCode10214 { get; set; }
        public string ErrorCode10215 { get; set; }
        public string ErrorCode10216 { get; set; }
        public string ErrorCode10217 { get; set; }
        public string ErrorCode10218 { get; set; }
        public string ErrorCode10219 { get; set; }
        public string ErrorCode10220 { get; set; }
        public string ErrorCode10221 { get; set; }
        public string ErrorCode10222 { get; set; }
        public string ErrorCode10223 { get; set; }
        public string ErrorCode10224 { get; set; }
        public string ErrorCode10225 { get; set; }
        public string ErrorCode10226 { get; set; }
        public string ErrorCode10227 { get; set; }
        public string ErrorCode10228 { get; set; }
        public string ErrorCode10229 { get; set; }
        public string ErrorCode10230 { get; set; }
        public string ErrorCode10231 { get; set; }
        public string ErrorCode10232 { get; set; }
        public string ErrorCode10233 { get; set; }
        public string ErrorCode10234 { get; set; }
        public string ErrorCode10235 { get; set; }
        public string ErrorCode10236 { get; set; }




        public string ParasutClientId { get; set; }
        public string ParasutClientSecret { get; set; }
        public string ParasutUserName { get; set; }
        public string ParasutPassword { get; set; }
        public string ParasutCompanyId { get; set; }
        public string ParasutBankAccountId { get; set; }
        public string ParasutBaseApiToken {get;set;}
        public string ParasutBaseApi { get; set; }
        public bool UseParasut { get; set; }


    }
}
