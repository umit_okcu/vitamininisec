﻿using System;
using System.Collections.Generic;
using System.Text;

using Nop.Core;

using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Payments;
using Nop.Services.Tasks;
using Nop.Services.Shipping;
using Nop.Core.Domain.Shipping;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;
using System.Linq;
using Nop.Services.Common;
//using Nop.Core.Domain.Customers;
//using ArasCargoIntegrationService;
//using Order = ArasCargoIntegrationService.Order;
//using System.Threading.Tasks;
using Nop.Web.Models.ShoppingCart;

using Nop.Web.Factories;

using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;

//using TestAras;
//using OrderTest = TestAras.Order;
//using ServiceSoapClientTest = TestAras.ServiceSoapClient;
//using PieceDetailTest = TestAras.PieceDetail;
//using OrderResultInfoTest = TestAras.OrderResultInfo;
using Newtonsoft.Json;
using YurticiService;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;
using Nop.Plugin.Shipping.FixedByWeightByTotal.Models;
using Nop.Services.Discounts;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Services
{
    public class ShippingScheduledTask : IScheduleTask
    {


        #region Fields
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly ILocalizationService _localizationService;
        private readonly ILogger _logger;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly IShipmentService _shipmentService;
        private readonly IShippingService _shippingService;
        private readonly IOrderService _orderService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly FixedByWeightByTotalSettings _fixedByWeightByTotalSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly CallYurtici _callYurtici;
        private readonly ParasutService _parasutService;
        private readonly ICustomerService _customerService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;

        #endregion

        #region Ctor

        public ShippingScheduledTask(ILocalizationService localizationService, ParasutService parasutService, ICustomerService customerService,
        ILogger logger,
        IOrderTotalCalculationService orderTotalCalculationService,
            IPaymentService paymentService,
            ISettingService settingService,
            IShipmentService shipmentService,
            IShoppingCartModelFactory shoppingCartModelFactory,
        IShippingService shippingService,
            IOrderService orderService,
            IOrderProcessingService orderProcessingService,
             IGenericAttributeService genericAttributeService,
        IWorkContext workContext,
        IStoreContext storeContext,
        FixedByWeightByTotalSettings fixedByWeightByTotalSettings,
        CallYurtici callYurtici
   
        )
        {
            this._orderTotalCalculationService= orderTotalCalculationService;
            this._customerService = customerService;
            this._shoppingCartModelFactory = shoppingCartModelFactory;
            this._parasutService = parasutService;
            this._localizationService = localizationService;
            this._logger = logger;
            this._paymentService = paymentService;
            this._settingService = settingService;
            this._shipmentService = shipmentService;
            this._shippingService = shippingService;
            this._orderService = orderService;
            this._orderProcessingService = orderProcessingService;
            this._genericAttributeService = genericAttributeService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._fixedByWeightByTotalSettings = fixedByWeightByTotalSettings;
            this._callYurtici = callYurtici;


        }

        #endregion
        public void Execute()
        {

            ///////////////////////////////////////////////
            ///Initiation Of Invoce Creation With Parasüt//
            ///////////////////////////////////////////////
            var dateAfter = DateTime.UtcNow.AddDays(-7);
            var _ordersForInvoiceCreation = _orderService.SearchOrders(0, 0, 0, 0, 0, 0, 0, null, dateAfter, null, null, null, null, null, null, string.Empty, null, 0, 10000000, false).Where(x => x.ShippingStatus == ShippingStatus.NotYetShipped && x.AuthorizationTransactionCode == null);

            List<Core.Domain.Orders.Order> ordersForInvoiceCreation = new List<Core.Domain.Orders.Order>();
            _logger.Error("Shipments | ScheduledTask Started");

            bool gotOrderForInvoiceCreation = false;
            var eForInvoiceCreation = _ordersForInvoiceCreation as System.Collections.IEnumerable;
            //   if (eForInvoiceCreation == null || !eForInvoiceCreation.GetType().GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))) gotOrderForInvoiceCreation = false;
            int indexForInvoiceCreation = 0;
            foreach (object _ in eForInvoiceCreation)
            {
                gotOrderForInvoiceCreation = true;
                indexForInvoiceCreation++;
                if (indexForInvoiceCreation >= 1)
                {
                    break;
                }
            }



            if (gotOrderForInvoiceCreation)
            {

                ordersForInvoiceCreation = _ordersForInvoiceCreation.ToList();
                ordersForInvoiceCreation.Reverse();
                _logger.Error("Shipments | Parasut will be created Invoice_no for" + ordersForInvoiceCreation.Count + "orders. From orderID: " + ordersForInvoiceCreation.First().CustomOrderNumber.ToString() + " to " + ordersForInvoiceCreation.Last().CustomOrderNumber.ToString());



                foreach (var order in ordersForInvoiceCreation)
                {
                    Customer customer = _customerService.GetCustomerById(order.CustomerId);

                    

                    ///////////////////////////////////////////////
                    ///Should Use Parasut//
                    ///////////////////////////////////////////////

                    string _invoice_Id = "";
                    try
                    {
                        var IyzicoPaymentSettings = _settingService.LoadSetting<IyzicoPaymentSettings>(0);

                        //bool falseValue = false;
                        if (IyzicoPaymentSettings.UseParasut == true)
                        {

                            _logger.Error("Parasut || Started to create invoice.", null, customer);

                            List<OrderItem> _paidCartItems = new List<OrderItem>();
                                var aidCartItems = order.OrderItems;

                            //    List<ShoppingCartItemModel> _paidCartItems = _subtotals.Items as List<ShoppingCartItemModel>;
                            //   foreach (var item in _paidCartItems)
                            //   {
                            //       var mahmut = item.SubTotal;
                            //   }

                            //foreach (var item in _paidCart)
                            //{
                            //    var mahmut = item.Subtotal;
                            //}

                            foreach (var item in aidCartItems)
                            {

                                OrderItem newitem = new OrderItem()
                                {
                                    Quantity = item.Quantity,
                                    UnitPriceInclTax = item.UnitPriceInclTax,
                                    Product = item.Product,
                                    ProductId = item.ProductId,
                                    DiscountAmountInclTax = item.DiscountAmountInclTax,
                                    PriceInclTax = item.PriceInclTax,




                                };
                                _paidCartItems.Add(newitem);

                            }


                            ///////////////////////////////////////////////     Beware monger
                            ///Get Products for paraşüt Invoice//               This is a place where one could deceive his products!!
                            ///////////////////////////////////////////////

                            var cart = customer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                        .ToList();



                            decimal total;
                            var shoppingCartTotalBase = (decimal)order.OrderTotal;//_orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderTotalDiscountAmountBase, out List<DiscountForCaching> _, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);

                            total = shoppingCartTotalBase;
                            decimal shippingtotal;


                            shippingtotal = (decimal)order.OrderShippingInclTax;//_orderTotalCalculationService.GetShoppingCartShippingTotal(cart, true);
                            var cartTotal = total - shippingtotal;

                            ///////////////////////////////////////////////
                            ///Get Parasut Token//
                            ///////////////////////////////////////////////
                            ///
                            _logger.Error("Parasut || Trying to get a token with:" + IyzicoPaymentSettings.ParasutBaseApiToken + IyzicoPaymentSettings.ParasutClientId + IyzicoPaymentSettings.ParasutUserName + IyzicoPaymentSettings.ParasutPassword, null, customer);
                            var tokenData = _parasutService.GetToken();
                            _logger.Error("Parasut || Had token.", null, customer);
                            tokenData.Wait();

                            string _token = "";
                            if (tokenData.Result == "error")
                            {
                                _logger.Error("Parasut || Error orccured while token created .", null, customer);
                            }
                            else
                            {
                                _token = tokenData.Result.ToString();
                            }

                            
                            string endPoint = IyzicoPaymentSettings.ParasutBaseApi + "/" + IyzicoPaymentSettings.ParasutCompanyId;   /////// Create Parasut api EndPoint////
                            string account_id = IyzicoPaymentSettings.ParasutBankAccountId;//                                                                                                       ///////////////////////////////////////////////
                            _logger.Error("Parasut || endpoint:" + endPoint + " accountid:" + account_id, null, customer);                                                                                                ///Create costomer for invoice//
                            ///////////////////////////////////////////////
                            var _createCustomerWait = _parasutService.CreateCustomer(_token, endPoint, customer);
                            _createCustomerWait.Wait();

                            var _createCustomer = "";// _createCustomerWait.Result;
                            if (_createCustomerWait.Result == "error")
                            {
                                _logger.Error("Parasut || Error orccured while customer created to create invoice.", null, customer);
                            }
                            else
                            {
                                _createCustomer = _createCustomerWait.Result;
                            }

                            _logger.Error("Parasut || Customer created to create invoice.", null, customer);
                            var responseCustomerModel = JsonConvert.DeserializeObject<dynamic>(_createCustomer);


                            ///////////////////////////////////////////////
                            // Create Products for Invoice/////////////////
                            ///////////////////////////////////////////////
                            List<BasketItem> basketItems = new List<BasketItem>();










                            for (int i = 0; i < _paidCartItems.Count; i++)
                            {
                               // decimal discounttotal = 0.00m;





                                //List<decimal> allDiscounts = new List<decimal>();

                                //List<decimal> cumulativeDiscounts = new List<decimal>();
                                //List<decimal> notCumulativeDiscounts = new List<decimal>();
                                //List<dynamic> allTheSingleDiscounts = new List<dynamic>();
                                //List<decimal> allTheSingleDiscountAmounts = new List<decimal>();

                                //int alldiscounts = cart[i].Product.DiscountProductMappings.Count;
                                //int cumulativeQuantity = 0;
                                //int notCumulativeQuantity = 0;

                                //if (alldiscounts >= 1)
                                //{

                                //    foreach (var item in cart[i].Product.DiscountProductMappings)
                                //    {
                                //        bool iscumulative = (bool)item.Discount.IsCumulative;
                                //        if (iscumulative == true)
                                //        {
                                //            cumulativeQuantity++;
                                //            cumulativeDiscounts.Add(item.Discount.DiscountAmount);
                                //        }
                                //        else
                                //        {
                                //            notCumulativeQuantity++;
                                //            notCumulativeDiscounts.Add(item.Discount.DiscountAmount);
                                //        }

                                //        allTheSingleDiscounts.Add(item.Discount);
                                //        allTheSingleDiscountAmounts.Add(item.Discount.DiscountAmount);
                                //    }

                                //}



                                //decimal totalcumulative = 0.00m;
                                //decimal max = 0.00m;

                                //if (cumulativeDiscounts.Count > 0) totalcumulative = cumulativeDiscounts.Sum(x => Convert.ToDecimal(x));

                                //if (allTheSingleDiscountAmounts.Count > 0) max = allTheSingleDiscountAmounts.Max();






                                //if (totalcumulative > max)
                                //{

                                //    discounttotal = totalcumulative;

                                //}
                                //else
                                //{
                                //    discounttotal = max;
                                //}





                                for (int ii = 0; ii < _paidCartItems[i].Quantity; ii++)
                                {

                                    var category = _paidCartItems[i].Product.ProductCategories.FirstOrDefault(x => x.Category != null);
                                    BasketItem basketItem = new BasketItem();
                                    basketItem.Id = _paidCartItems[i].ProductId.ToString();
                                    basketItem.Name = _paidCartItems[i].Product.Name;

                                    if (category != null)
                                    {

                                        basketItem.Category1 = category.Category.Name;
                                    }
                                    else
                                    {
                                        basketItem.Category1 = "--";
                                    }
                                    //  basketItem.Category2 = "--";
                                    basketItem.ItemType = "0";
                                    //if(false)// (cart[i].Product.HasTierPrices == true)
                                    //{

                                    //    List<int> tierQuantities = new List<int>();
                                    //    foreach (var item in cart[i].Product.TierPrices)
                                    //    {

                                    //        int quantity = item.Quantity;
                                    //        tierQuantities.Add(quantity);
                                    //    }
                                    //    int indexOfTier = 0;
                                    //    tierQuantities.Sort();
                                    //    for (int z = 0; z < tierQuantities.Count; z++)
                                    //    {
                                    //        TierPrice _tierPrice = (TierPrice)cart[i].Product.TierPrices.ElementAt(z);
                                    //        if (cart[i].Quantity >= tierQuantities[z] &&
                                    //            (
                                    //            (_tierPrice.StartDateTimeUtc <= DateTime.Now && _tierPrice.EndDateTimeUtc >= DateTime.Now) ||
                                    //            (_tierPrice.StartDateTimeUtc == null && _tierPrice.EndDateTimeUtc == null)
                                    //            )
                                    //            )
                                    //        {
                                    //            indexOfTier = z;
                                    //        }
                                    //    }

                                    //    TierPrice tierPrice = (TierPrice)cart[i].Product.TierPrices.ElementAt(indexOfTier);

                                    //    basketItem.Price = (tierPrice.Price- discounttotal).ToString("#.#########").Replace(",", ".");


                                    //    string _____price = _paidCartItems[i].UnitPrice.Replace(".", "").Replace(",", ".");



                                    //   string _unitPrice = new String(_____price.Where(c => char.IsDigit(c) || c == (char)46).ToArray());







                                    //    basketItem.SubMerchantPrice = _unitPrice;

                                    ////    string asd = new String(_____price.Where(Char.IsPunctuation(',')).ToArray());
                                    //}
                                    //else
                                    //{
                                    basketItem.Price = (_paidCartItems[i].Product.Price).ToString("#.#########").Replace(",", ".");
                                    // string _____price = _paidCartItems[i].UnitPrice;
                                    // decimal _unitPrice = Convert.ToDecimal(_____price.Replace("₺", "").Replace(",", "."));
                                    // basketItem.SubMerchantPrice = _unitPrice.ToString("#.#########").Replace(",", ".");

                                    string _____price = _paidCartItems[i].UnitPriceInclTax.ToString().Replace(",", "."); //.Replace(" tl", "").Replace(",", ".").Replace(" Tl", "").Replace(" TL", "").Replace(" ", "");
                                    string _unitPrice = new String(_____price.Where(c => char.IsDigit(c) || c == (char)46).ToArray());
                                    basketItem.SubMerchantPrice = _unitPrice;
                                    //string _____price = _paidCartItems[i].UnitPrice.Replace(".", "").Replace(",", ".");
                                    //decimal _unitPrice = decimal.Parse(_____price, NumberStyles.Currency);
                                    //basketItem.SubMerchantPrice = _unitPrice.ToString();

                                    basketItems.Add(basketItem);

                                }

                            }
                            ///////////////////add shipping price///////////////////////////
                            if (shippingtotal > 0)
                            {
                                
                                BasketItem shippingItem = new BasketItem();
                                shippingItem.Price = shippingtotal.ToString("#.#########").Replace(",", ".");
                                shippingItem.SubMerchantPrice = shippingtotal.ToString("#.#########").Replace(",", ".");
                                shippingItem.Name = "Kargo Ücreti";
                                shippingItem.Id = "0";
                                basketItems.Add(shippingItem);
                            }
                            ////////////////////////////////////////////////////////////////////////////check If there is any problem//////////////////////////
                            //    List<decimal> discounts = new List<decimal>();

                            ///////////////////////////////////////////////
                            // to discount per item in invoice use here///////////
                            ///////////////////////////////////////////////
                            //foreach (var item in basketItems)
                            //{
                            //    var priceOfProduct = Convert.ToDecimal(item.Price);

                            //    var discountedPriceOfProduct = priceOfProduct - ((priceOfProduct / (total + orderTotalDiscountAmountBase)) * orderTotalDiscountAmountBase);
                            //   // item.Price = discountedPriceOfProduct.ToString("#.##").Replace(",", ".");

                            //    decimal discount = priceOfProduct - discountedPriceOfProduct;
                            //    discounts.Add(discount);

                            //} 



                            List<string> createdParasutProducts = new List<string>();  ///// list of created products////
                            for (int i = 0; i < basketItems.Count; i++)
                            {
                                BasketItem product = basketItems[i];


                                ///////////////////////////////////////////////
                                // Product creation ///////////////////////////
                                ///////////////////////////////////////////////
                                ///
                                _logger.Error("Parasut || Gonna CreateProduct with:." + endPoint + _token + product.Name, null, customer);
                                var _productResponse = _parasutService.CreateProduct(endPoint, _token, product);//.Result;
                                _productResponse.Wait();


                                var productResponse = "";
                                if (_productResponse.Result == "error")
                                {
                                    _logger.Error("Parasut || Error orccured while product created to create invoice.", null, customer);
                                }
                                else
                                {
                                    productResponse =  _productResponse.Result;
                                }




                          

                                _logger.Error("Parasut || Product Created:." + endPoint + _token + product.Name, null, customer);
                                createdParasutProducts.Add(productResponse);

                                //  var responseModel = JsonConvert.DeserializeObject<dynamic>(productResponse);


                            }
                            ///////////////////////////////////////////////
                            // Create Invoice /////////////////////////////
                            ///////////////////////////////////////////////
                            ///
                            decimal orderTotalDiscountAmountBase = order.OrderDiscount;// 0.0m;
                      //      foreach (var item in order.OrderItems)
                      //      {
                      //          for(int i = 0; i < item.Quantity; i++)
                      //          {
                      //
                      //          orderTotalDiscountAmountBase += item.Product.Price;
                      //              orderTotalDiscountAmountBase -= item.UnitPriceInclTax;
                      //          }
                      //      }
                            string customerId = responseCustomerModel.data.id;
                            var _parasutInvoiceResponce = _parasutService.CreateInvoice(_token, endPoint, createdParasutProducts, customerId, customer, orderTotalDiscountAmountBase, basketItems);//.Result;

                            _parasutInvoiceResponce.Wait();

                            var parasutInvoiceResponce = "";// _parasutInvoiceResponce.Result;
                            if (_parasutInvoiceResponce.Result == "error")
                            {
                                _logger.Error("Parasut || Error orccured while customer created to create invoice.", null, customer);
                            }
                            else
                            {
                                parasutInvoiceResponce = _parasutInvoiceResponce.Result;
                            }




                           
                            var parasutInvoiceResponseModel = JsonConvert.DeserializeObject<dynamic>(parasutInvoiceResponce);
                            ///////////////////////////////////////////////
                            // Pay Invoice /////////////////////////////
                            ///////////////////////////////////////////////

                            decimal net_total = parasutInvoiceResponseModel.data.attributes.net_total;
                            int invoice_Id = parasutInvoiceResponseModel.data.id;
                            _logger.Error("Parasut || CreatedInvoiceId: " + invoice_Id, null, customer);
                            int bankAccountId = Convert.ToInt32(account_id);
                            string iyzicoPaymenId = order.AuthorizationTransactionId.ToString();
                            var _parasutPayInvoice = _parasutService.PayInvoice(_token, endPoint, bankAccountId, net_total, invoice_Id, iyzicoPaymenId);//.Result;
                            _parasutPayInvoice.Wait();


                            var parasutPayInvoice = "";// _parasutInvoiceResponce.Result;
                            if (_parasutPayInvoice.Result == "error")
                            {
                                _logger.Error("Parasut || Error orccured while customer created to create invoice.", null, customer);
                            }
                            else
                            {
                                parasutPayInvoice = _parasutPayInvoice.Result;
                            }




                       

                            var parasutPayInvoiceResponseModel = JsonConvert.DeserializeObject<dynamic>(parasutPayInvoice);
                            _invoice_Id = invoice_Id.ToString();
                    order.AuthorizationTransactionCode = "InvoiceId," + _invoice_Id;
                    _orderService.UpdateOrder(order);
                _logger.Error("Parasut || InvoiceCretedWith Id :" + _invoice_Id, null, customer);
                        }
                        ///////////////////////////////////////////////
                        ///End Of Invoce Creation With Parasüt/////////
                        ///////////////////////////////////////////////
                    }
                    catch (Exception ex)
                    {
                        //PaymentInfoModel modelAlert = new PaymentInfoModel();
                        //
                        //modelAlert.Status = "Başarız";
                        //modelAlert.ErrorMessage = "Kartınızdan " + order.OrderTotal+ "TL çekildikten sonra teknik bir aksaklıktan dolayı siparişiniz sisteme işlenemedi. Lütfen site yönetimiyle irtibata geçiniz. Müşteri numaranız: " + customer.Id.ToString() + " | Iyzico Ödeme numaranız: " + order.AuthorizationTransactionId.ToString()+ "| Kartınızdan çekilen tutar :" + checkoutForm.PaidPrice;
                        _logger.Error("Parasut || Error : ", ex, customer);
                   //     return View("~/Plugins/Payments.Iyzico/Views/AlertView.cshtml", modelAlert);//     return Json("Hata Ödememiz alınmıştır. Site yönetimiyle "+ex);// System.Diagnostics.Debug.WriteLine(ex);  /////// Send Message to admin about ex
                    }




                }


            }




            //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///    //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///        //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///            //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///            //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///              //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///            //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///          //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///         //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///        //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///         //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///          //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            ///            //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////
            //////////////////// CHECK INVOICES FROM PARASUT ///////////////////////////////




        //    var dateAfter = DateTime.UtcNow.AddDays(-7);
            var _ordersForInvoice = _orderService.SearchOrders(0, 0, 0, 0, 0, 0, 0, null, dateAfter, null, null, null, null, null, null, string.Empty, null, 0, 10000000, false).Where(x => x.ShippingStatus == ShippingStatus.NotYetShipped && x.AuthorizationTransactionCode.Contains("InvoiceId,") && x.AuthorizationTransactionCode.Length>=17);
            List<Core.Domain.Orders.Order> ordersForInvoice = new List<Core.Domain.Orders.Order>();
            List<Models.YurticiCheckModel> checkYurticiKeys = new List<Models.YurticiCheckModel>();

            _logger.Error("Shipments | ScheduledTask Started");

            bool gotOrderForInvoice = false;
            var eForInvoice = _ordersForInvoice as System.Collections.IEnumerable;
         //   if (eForInvoice == null || !eForInvoice.GetType().GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))) gotOrderForInvoice = false;
            int indexForInvoice = 0;
            foreach (object _ in eForInvoice)
            {
                gotOrderForInvoice = true;
                indexForInvoice++;
                if (indexForInvoice >= 1)
                {
                    break;
                }
            }



            if (gotOrderForInvoice )
            {


                ordersForInvoice = _ordersForInvoice.ToList();
                ordersForInvoice.Reverse();
                _logger.Error("Shipments | Parasut will be checked for Invoice_no for" + ordersForInvoice.Count + "orders. From orderID: " + ordersForInvoice.First().CustomOrderNumber.ToString() + " to " + ordersForInvoice.Last().CustomOrderNumber.ToString());



                foreach (var order in ordersForInvoice)
                {


        //            var _customValues = _paymentService.DeserializeCustomValues(order);
        //        string InvoiceId = "";
        //
        //        if (_customValues.ContainsKey("InvoiceId"))
        //        {
        //
        //            InvoiceId = _customValues["InvoiceId"].ToString();
        //        }

                    bool isOrderGotComa = false;
                        isOrderGotComa = order.AuthorizationTransactionCode.Contains(',');


                   


                    if (isOrderGotComa)
                    {

                        string authorizationCode = order.AuthorizationTransactionCode;
                        string InvoiceId = authorizationCode.Split(',')[1];
                        //////////////Get invoice Bill number ////////////        ///////////// this vlue will gonna taken from order.AuthorizationTransactionCode if i
                        var invoiceBillNumber = "";
                      //  if (_customValues.ContainsKey("InvoiceBillNumber"))
                      //  {
                      //
                      //      invoiceBillNumber = _customValues["InvoiceBillNumber"].ToString();
                      //  }
                      //  else
                      //  {

                            var _isInvoicePrinted = _parasutService.CheckIfInvoicePrinted(InvoiceId);
                            _isInvoicePrinted.Wait();


                        var isInvoicePrinted = "";// _parasutInvoiceResponce.Result;
                        if (_isInvoicePrinted.Result == "error")
                        {
                            _logger.Error("Parasut || Error orccured while customer created to create invoice.");
                        }
                        else
                        {
                            isInvoicePrinted = _isInvoicePrinted.Result;
                        }



                        var _invoiceParasutResponce = JsonConvert.DeserializeObject<dynamic>(isInvoicePrinted);
                            var _invoice = _invoiceParasutResponce.data.attributes;//.invoice_no;

                            if (!String.IsNullOrEmpty(_invoice.invoice_no.ToString()))
                            {

                               // var invoiceSeries = _invoice.invoice_series;
                                var invoiceNo = _invoice.invoice_no;
                                invoiceBillNumber = invoiceNo.ToString().Replace(" ", null);

                                //            order.ShippingStatus = ShippingStatus.CargoInformed;
                                Core.Domain.Orders.Order customvaluesOrder = new Core.Domain.Orders.Order();
                                customvaluesOrder.CustomValuesXml = order.CustomValuesXml;
                                var deserialized = _paymentService.DeserializeCustomValues(customvaluesOrder);
                            if (deserialized.ContainsKey("InvoiceBillNumber"))
                            {
                                deserialized["InvoiceBillNumber"] = invoiceBillNumber;

                            }else
                            { 
                                deserialized.Add("InvoiceBillNumber", invoiceBillNumber);
                            }
                                _logger.Error("Shipments | Recieved Invoice data from Parasut for OrderId= " + order.Id.ToString() + " | invoice_no =  " + invoiceBillNumber);
                                ProcessPaymentRequest toSerialize = new ProcessPaymentRequest();//
                                toSerialize.CustomValues = deserialized;
                                var serialized = _paymentService.SerializeCustomValues(toSerialize);
                                order.CustomValuesXml = serialized;
                                order.AuthorizationTransactionCode = invoiceBillNumber.Replace(" ",null);
                                _orderService.UpdateOrder(order);
                            }
                            else
                            {
                                _logger.Error("Shipments | Error : Invoice of OrderId= " + order.Id.ToString() + " hasn't been printed yet.");
                            }
                    //    }



                    }
                }
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////    ///////              ////      /////           /////     //////////////    //////////////////////    //////     ////////////////////     //////      ////////////////////////////////////////////////////////////////////////
            //////////////////////////      /////            ////        /////           /////     //////////////    //////////////////////    //////     ////////////////////     //////      ////////////////////////////////////////////////////////////////////////
            //////////////////////////         ////         ////         /////           /////     ////      ////             ////                        //////        ///////                ////////////////////////////////////////////////////////////////////////
            //////////////////////////          ////     ////            /////           /////     ////      ////             ////                        //////                               ////////////////////////////////////////////////////////////////////////
            //////////////////////////            /// /////              /////           /////     ////      ////             ////             //////     //////                    //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////             //////                /////           /////     ////////////               ////             //////     //////                    //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////              ////                 /////           /////     ///////////                ////             //////     //////                    //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////              ////                 /////           /////     //// /////                 ////             //////     //////                    //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////              ////                 /////           /////     ////  /////                ////             //////     //////                    //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////              ////                 /////           /////     ////   //////              ////             //////     //////         //////     //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////              ////                 /////////////////////     ////      ////             ////             //////     /////////////////////     //////     ////////////////////////////////////////////////////////////////////////
            //////////////////////////              ////                 /////////////////////     ////       ////            ////             //////     ////////////////////      //////     ////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    ////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    ////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            bool falseValueToDevelop = false;
            if (falseValueToDevelop)
            {

            

           
            _logger.Error("Shipments | Engine Started for Yurtici");

                var _orders = _orderService.SearchOrders(0, 0, 0, 0, 0, 0, 0, null, dateAfter, null, null, null, null, null, null, string.Empty, null, 0, 10000000, false).Where(x => x.ShippingStatus == ShippingStatus.NotYetShipped && x.AuthorizationTransactionCode != null&&x.AuthorizationTransactionCode.Contains("InvoiceId,")==false); // && x.AuthorizationTransactionCode.Contains("K") && x.AuthorizationTransactionCode.Length == 16);
            List<Core.Domain.Orders.Order> orders = new List<Core.Domain.Orders.Order>();
         //   List<Models.YurticiCheckModel> checkYurticiKeys = new List<Models.YurticiCheckModel>();

           // _logger.Error("Shipments | ScheduledTask Started");

            bool gotOrder = false;
            var e = _orders as System.Collections.IEnumerable;
            if (e == null || !e.GetType().GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))) gotOrder = false;
                int indexToCheckIfOrderExist = 0;
            foreach (object _ in e)
            {
                gotOrder = true;
                    indexToCheckIfOrderExist++;
                    if (indexToCheckIfOrderExist>=1)
                    {
                        break;
                    }
            }



            if (gotOrder && _fixedByWeightByTotalSettings.UseYurtici == true)
            {

                orders = _orders.ToList();
                  orders.Reverse();
                _logger.Error("Shipments | Cargo will informed about " + orders.Count + "orders. From orderID: " + orders.First().CustomOrderNumber.ToString() + " to " + orders.Last().CustomOrderNumber.ToString());
                foreach (var order in orders)
                {

                    var shippingMethodName = order.ShippingMethod;
                    var shippingpluginName = order.ShippingRateComputationMethodSystemName;
                    var shippingStatus = order.ShippingStatus;

                    try
                    {
                        //////////////Get invoice Bill number ////////////        ///////////// this vlue will gonna taken from order.AuthorizationTransactionCode if i
                        string invoiceBillNumber = order.AuthorizationTransactionCode;

                        ///////////////////////////inform argo and get kargo key ////////////////////////////


                        //SetOrder() Yurtiçi
                        /////////////////////////////////////////////////////////Here We Call Yurtiçi///////////////////////////
                        
                        var callYurtici = _callYurtici.SetOrderGO(order, invoiceBillNumber);//CallAras(order);
                        callYurtici.Wait();
                        /////////////////Get Integration Code And Save In Order CustomAttributes   /////////////////////
                        createShipmentResponse1 errorcheck = new createShipmentResponse1();
                        errorcheck = callYurtici.Result;
                        var isSuccess = callYurtici.Result.createShipmentResponse.ShippingOrderResultVO;//.ShippingDeliveryVO.outFlag == "0";
                        var response = isSuccess.shippingOrderDetailVO;
                        var _isSuccess = isSuccess.outFlag == "0";

                        if (_isSuccess)
                        {

                            foreach (var item in response)
                            {


                                if (item.errCode != 0)
                                {
                                    _logger.Error("Shipments | Error: kargo key :" + item.cargoKey + " | errCode: " + item.errCode + " | errCodeSpecified " + item.errCodeSpecified + " " + item.errMessage);

                                }
                                else
                                {
                                    var _errCode = item.errCode;

                                    var _jobId = isSuccess.jobId;                  //Take Care of that little prick
                                    var _cargoKey = item.cargoKey;              // This is gonna be the barcode
                                    var _invoiceKey = item.invoiceKey;


                                    //add shipment

                                    Core.Domain.Orders.Order customvaluesOrder = new Core.Domain.Orders.Order();
                                    customvaluesOrder.CustomValuesXml = order.CustomValuesXml;
                                    var deserialized = _paymentService.DeserializeCustomValues(customvaluesOrder);

                                 

                                        if (deserialized.ContainsKey("YurticiCargoKey"))
                                        {
                                            deserialized["YurticiCargoKey"] = _cargoKey;

                                        }
                                        else
                                        {
                                            deserialized.Add("YurticiCargoKey", _cargoKey);
                                        }
                                        if (deserialized.ContainsKey("ParasutInvoiceReturnKey"))
                                        {
                                            deserialized["ParasutInvoiceReturnKey"] = _invoiceKey;

                                        }
                                        else
                                        {
                                            deserialized.Add("ParasutInvoiceReturnKey", _invoiceKey);
                                        }
                                        if (deserialized.ContainsKey("YurticiJobId"))
                                        {
                                            deserialized["YurticiJobId"] = _jobId;

                                        }
                                        else
                                        {
                                            deserialized.Add("YurticiJobId", _jobId);
                                        }




                                        if (deserialized.ContainsKey("InvoiceBillNumber"))
                                        {
                                            deserialized["InvoiceBillNumber"] = invoiceBillNumber;

                                        }
                                        else
                                        {
                                            deserialized.Add("InvoiceBillNumber", invoiceBillNumber);
                                        }

                                        ProcessPaymentRequest toSerialize = new ProcessPaymentRequest();//
                                    toSerialize.CustomValues = deserialized;
                                    var serialized = _paymentService.SerializeCustomValues(toSerialize);
                                    order.CustomValuesXml = serialized;
                                    Shipment shipment = new Shipment();
                                    shipment.OrderId = order.Id;
                                    shipment.CreatedOnUtc = DateTime.UtcNow;
                                    if (_errCode == 0)
                                    {
                                        order.ShippingStatus = ShippingStatus.CargoInformed;
                                        shipment.AdminComment = "yurticiCargoKey:," + _cargoKey.ToString() + ", yiJobId:," + _jobId.ToString() + ", yurticiInvoicekey:, " + _invoiceKey.ToString();
                                        _logger.Error("Shipments | Cargo informed for OrderId: " + order.Id.ToString() + " with " + shipment.AdminComment);
                                    }
                                    else
                                    {

                                        order.ShippingStatus = ShippingStatus.NotYetShipped;
                                        _logger.Error("Shipments | Error: OrderId: " + order.CustomOrderNumber.ToString() + "has an Yurtici Error Code:" + _errCode);
                                        string errorCommentForAdmin = "HataKodu:" + _errCode.ToString() + " | Error Message: " + item.errMessage.ToString();
                                        shipment.AdminComment = errorCommentForAdmin.Replace(',', ' ');
                                    }
                                    shipment.TotalWeight = 1.000m;

                                    foreach (var orderItem in order.OrderItems)
                                    {

                                        ShipmentItem shipmentItem = new ShipmentItem();
                                        shipmentItem.OrderItemId = orderItem.Id;
                                        shipmentItem.Quantity = orderItem.Quantity;
                                        shipmentItem.WarehouseId = 0;

                                        shipment.ShipmentItems.Add(shipmentItem);// = shipmentItems;
                                    }

                                    order.Shipments.Add(shipment);


                                }
                                _orderService.UpdateOrder(order);
                            }
                        }
                        else {
                            

                            _logger.Error("Shipments | Cargo informed for JobId: "+errorcheck.createShipmentResponse.ShippingOrderResultVO.jobId + " outFlag: " + errorcheck.createShipmentResponse.ShippingOrderResultVO.outFlag+ " outResult:" + errorcheck.createShipmentResponse.ShippingOrderResultVO.outResult);
                        }

                    }
                    catch (Exception ex)
                    {

                        _logger.Error("Shipments | Error: OrderId: " + order.CustomOrderNumber.ToString() + "has an exeption :" + ex);

                    }
                }

            }
            else
            {
                if (!gotOrder)
                {

                    _logger.Error("Shipments | Error: No new order present.");
                }

                if (_fixedByWeightByTotalSettings.UseYurtici != true)
                {
                    _logger.Error("Shipments | _fixedByWeightByTotalSettings.UseYurtici setting should be set to 'true' .");
                }
            }



            //////////////////////////////Create Control System!//////////////
            var shipments = _shipmentService.GetAllShipments().Where(x => x.Order.ShippingStatus != ShippingStatus.NotYetShipped && x.Order.ShippingStatus != ShippingStatus.Delivered);  //check if null

 //           List<Shipment> onTheWayShipments = new List<Shipment>();
 //           List<Shipment> informedShipments = new List<Shipment>();


            bool gotOrderToCheck = false;
            var ship = shipments as System.Collections.IEnumerable;
            if (ship == null || !ship.GetType().GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>))) gotOrderToCheck = false;

            foreach (object _ in ship)
            {
                gotOrderToCheck = true;
            }




            if (gotOrderToCheck)        ////////////zaman olsa böyle çirkin bırakmazdım burayı.////
            {
                shipments.ToList();
                _logger.Error("Shipments | System Checking " + shipments.Count().ToString() + " Informed cargo as usual.");




                foreach (var item in shipments)
                {

                    int orderId = item.OrderId;
                    var relatedOrder = item.Order;//_orderService.GetOrderById(Convert.ToInt32(orderId));

                    var status = item.Order.ShippingStatus;
                   // if (status == ShippingStatus.Shipped)
                   // {
                   //     onTheWayShipments.Add(item);
                   //
                   //
                   // }
                   // if (status == ShippingStatus.CargoInformed)
                   // {
                   //     informedShipments.Add(item);
                   //
                   // }
                    string _yurticiKargoKey = "";
                    Order customvaluesOrder = new Order
                    {
                        CustomValuesXml = relatedOrder.CustomValuesXml
                    };
                    var deserialized = _paymentService.DeserializeCustomValues(customvaluesOrder);

                    if (deserialized.ContainsKey("YurticiCargoKey"))
                    {
                        _yurticiKargoKey = deserialized["YurticiCargoKey"].ToString();

                    Models.YurticiCheckModel orderKeys = new Models.YurticiCheckModel();
                    orderKeys.CargoKey = _yurticiKargoKey;
                    orderKeys.OrderId = relatedOrder.Id.ToString();
                    orderKeys.ShipmentId = item.Id.ToString();

                    checkYurticiKeys.Add(orderKeys);
                    }


                }

            }

            int checkYurticiKeysCount = checkYurticiKeys.Count();


            if (checkYurticiKeysCount > 0)
            {




                string[] the_array = checkYurticiKeys.Select(x => x.CargoKey.ToString()).ToArray();

                //  YurticiService.queryShipmentResponse1 asdas = new queryShipmentResponse.ShippingDeliveryVO.shippingDeliveryDetailVO[0].shippingDeliveryItemDetailVO.docId; //////////////////////////////////////Here I am 
                var response = _callYurtici.GetQueryShipments(the_array);
                response.Wait();
                YurticiService.queryShipmentResponse1 responseModel = response.Result;

                var isSuccess = responseModel.queryShipmentResponse.ShippingDeliveryVO.outFlag == "0";



                if (isSuccess)
                {



                    var _shippingDeliveryDetailVO = responseModel.queryShipmentResponse.ShippingDeliveryVO.shippingDeliveryDetailVO;



                    foreach (var item in _shippingDeliveryDetailVO)
                    {
                        if (item.errCode != 0)
                        {
                            _logger.Error("Shipments | Error:" + item.errCode + " " + item.errCodeSpecified + " " + item.errMessage + " " + item.invoiceKey);

                        }
                        else
                        {



                            string _cargoKey = item.cargoKey;
                            string _orderId = checkYurticiKeys.FirstOrDefault(x => x.CargoKey == _cargoKey).OrderId;
                            string _shipmentId = checkYurticiKeys.FirstOrDefault(x => x.CargoKey == _cargoKey).ShipmentId;

                            int _operationCode = Int32.MaxValue;
                            _operationCode = (int)item.operationCode;///////// KARGO DURUMU

                            //////// KARGO TAKİP NUMARASI
                            string docId = "";
                            if (item.shippingDeliveryItemDetailVO != null)
                            {
                                docId = item.shippingDeliveryItemDetailVO.docId.ToString();

                            }


                            string kargoDurumu = "";

                            if (_operationCode != Int32.MaxValue)
                            {

                            string[] texts =new string[] { "Kargo İşlem Görmemiş.", "Kargo Teslimattadır.", "Kargo işlem görmüş, faturası henüz düzenlenmemiştir.", "Kargo Çıkışı Engellendi.", "Kargo daha önceden iptal edilmiştir.", "Kargo teslim edilmiştir.", "Fatura şube tarafından iptal edilmiştir." };
                            
                                switch (_operationCode)
                                {
                                    case 0:
                                        kargoDurumu = "Kargo İşlem Görmemiş.";
                                        break;
                                    case 1:
                                        kargoDurumu = "Kargo Teslimattadır.";

                                        var relatedOntheWayOrder = _orderService.GetOrderById(Convert.ToInt32(_orderId));

                                        if (relatedOntheWayOrder.ShippingStatus != ShippingStatus.Shipped)
                                        {

                                        if (docId != null)
                                        {

                                            var shipment = _shipmentService.GetShipmentById(Convert.ToInt32(_shipmentId));
                                            shipment.TrackingNumber = docId;
                                            shipment.ShippedDateUtc = DateTime.UtcNow;

                                            _shipmentService.UpdateShipment(shipment);
                                        }

                                        relatedOntheWayOrder.ShippingStatus = ShippingStatus.Shipped;
                                        relatedOntheWayOrder.OrderStatus = OrderStatus.Processing;

                                        _orderService.UpdateOrder(relatedOntheWayOrder);
                                        _logger.Error("Shipments | OrderId :" + relatedOntheWayOrder.CustomOrderNumber.ToString() + " has shipped at " + relatedOntheWayOrder.Shipments.First().ShippedDateUtc.ToString());
                                        }

                                        break;
                                    case 2:
                                        kargoDurumu = "Kargo işlem görmüş, faturası henüz düzenlenmemiştir.";
                                        break;
                                    case 3:
                                        kargoDurumu = "Kargo Çıkışı Engellendi.";
                                        break;
                                    case 4:
                                        kargoDurumu = "Kargo daha önceden iptal edilmiştir.";
                                        break;
                                    case 5:
                                        kargoDurumu = "Kargo teslim edilmiştir.";

                                        var relatedDeliveredOrder = _orderService.GetOrderById(Convert.ToInt32(_orderId));
                                        if(relatedDeliveredOrder.ShippingStatus != ShippingStatus.Delivered)
                                        {

                                        var deliveredShipment = _shipmentService.GetShipmentById(Convert.ToInt32(_shipmentId));
                                        var checkDocId = deliveredShipment.TrackingNumber;
                                        if (docId != null && !String.IsNullOrEmpty(checkDocId))
                                        {
                                            deliveredShipment.TrackingNumber = docId;

                                            deliveredShipment.DeliveryDateUtc = DateTime.UtcNow;

                                            _shipmentService.UpdateShipment(deliveredShipment);
                                        }

                                        relatedDeliveredOrder.ShippingStatus = ShippingStatus.Delivered;
                                        relatedDeliveredOrder.OrderStatus = OrderStatus.Complete;

                                        _logger.Error("Shipments | OrderId :" + relatedDeliveredOrder.CustomOrderNumber.ToString() + " has shipped at " + relatedDeliveredOrder.Shipments.First().DeliveryDateUtc.ToString());
                                        _orderService.UpdateOrder(relatedDeliveredOrder);
                                        }

                                        break;
                                    case 6:
                                        kargoDurumu = "Fatura şube tarafından iptal edilmiştir.";
                                        break;

                                }
                                var _relatedShipment = _shipmentService.GetShipmentById(Convert.ToInt32(_shipmentId));

                                string _adminComment = _relatedShipment.AdminComment;
                               
                               

                                  string __adminComment =  _adminComment.Replace(texts[0], null).Replace(texts[1], null).Replace(texts[2], null).Replace(texts[3], null).Replace(texts[4], null).Replace(texts[5], null).Replace(texts[6], null).Replace(" | ",null); 





                                    _relatedShipment.AdminComment = __adminComment + " | "+kargoDurumu;

                            


                                _shipmentService.UpdateShipment(_relatedShipment);


                          //      var relatedOrder = _orderService.GetOrderById(Convert.ToInt32(_orderId));
                          //      Core.Domain.Orders.Order customvaluesOrder = new Core.Domain.Orders.Order();
                          //      customvaluesOrder.CustomValuesXml = relatedOrder.CustomValuesXml;
                          //      var deserialized = _paymentService.DeserializeCustomValues(customvaluesOrder);
                          //
                          //      //deserialized.Add("InvoiceBillNumber", invoiceBillNumber);
                          //      //deserialized.Add("InvoiceBillNumber", invoiceBillNumber);
                          //      //deserialized.Add("InvoiceBillNumber", invoiceBillNumber);
                          //      //deserialized.Add("InvoiceBillNumber", invoiceBillNumber);
                          //
                          //
                          //
                          //
                          //
                          //      ProcessPaymentRequest toSerialize = new ProcessPaymentRequest();//
                          //      toSerialize.CustomValues = deserialized;
                          //      var serialized = _paymentService.SerializeCustomValues(toSerialize);
                          //      relatedOrder.CustomValuesXml = serialized;
                          //
                          //      _orderService.UpdateOrder(relatedOrder);




                            }



                            //    bool _operationCodeSpecified = item.operationCodeSpecified;
                            //    string _operationMessage = item.operationMessage;
                            //    string _operationStatus = item.operationStatus;
                            //    // var dsa = item.shippingDeliveryItemDetailVO.cargoEventExplanation;//kargo durumu
                            //    var _shippingDeliveryItemDetailVO = item.shippingDeliveryItemDetailVO;
                            //    if (_shippingDeliveryItemDetailVO != null)
                            //    {
                            //        var _cargoEventExplanation = _shippingDeliveryItemDetailVO.cargoEventExplanation;
                            //        var _cargoEventId = _shippingDeliveryItemDetailVO.cargoEventId;
                            //        var _cargoReasonExplanation = _shippingDeliveryItemDetailVO.cargoReasonExplanation;
                            //
                            //    }

                        }
                        //   var asdas = _shippingDeliveryDetailVO[0].cargoKey;
                        //   var asdasa = _shippingDeliveryDetailVO[0].errCode;
                        //   var asdass = _shippingDeliveryDetailVO[0].errCodeSpecified;
                        //
                        //   var asdads = _shippingDeliveryDetailVO[0].invoiceKey;
                        //   var asdafs = _shippingDeliveryDetailVO[0].errMessage;

                    }
                }
                else
                {

                    _logger.Error("Shipments | Yurtici Entegration response is not Succesful");



                }
            }


            _logger.Error("Shipments | ScheduledTask Completed");

        }
        }

   







        public static string RemoveAccent(string text)
        {


            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(text);
            return System.Text.Encoding.ASCII.GetString(bytes);

        }
    }
}





