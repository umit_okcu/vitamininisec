﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Models
{
    public class ParasutContactsModel
    {
        public Data data { get; set; }

        public class Data
        {
            public string id { get; set; }
            public string type { get; set; }
            public Attributes attributes { get; set; }
            public Relationships relationships { get; set; }
        }

        public class Attributes
        {
            public string email { get; set; }
            public string name { get; set; }
            public string contact_type { get; set; }
            public string tax_office { get; set; }
            public string tax_number { get; set; }
            public string district { get; set; }
            public string city { get; set; }
            public string address { get; set; }
            public string phone { get; set; }
            public bool archived { get; set; }
            public string account_type { get; set; }


            public string short_name { get; set; }
   
 
  

              public string  fax { get; set; }
                public bool is_abroad { get; set; }

               public string iban { get; set; }
  
        }

        public class Relationships
        {
            public Category category { get; set; }
        }

        public class Category
        {
            public Data1 data { get; set; }
        }

        public class Data1
        {
            public string id { get; set; }
            public string type { get; set; }
        }
    }
}
