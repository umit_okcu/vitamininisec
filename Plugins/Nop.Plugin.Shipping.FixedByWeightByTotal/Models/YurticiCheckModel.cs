﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Models
{
    public class YurticiCheckModel
    {

        public string CargoKey { get; set; }
        public string OrderId { get; set; }
        public string ShipmentId { get; set; }
    }
}
