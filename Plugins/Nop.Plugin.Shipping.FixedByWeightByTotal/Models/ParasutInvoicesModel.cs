﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Models
{
    public class ParasutInvoicesModel
    {
        public class Attributes
        {
            public string item_type { get; set; }
            public string description { get; set; }
            public string issue_date { get; set; }
            public string due_date { get; set; }
            public string invoice_series { get; set; }
            public string invoice_id { get; set; }
            public string currency { get; set; }
            public decimal exchange_rate { get; set; }
            public decimal withholding_rate { get; set; }
            public decimal vat_withholding_rate { get; set; }
            public string invoice_discount_type { get; set; }
            public decimal invoice_discount { get; set; }
            public string billing_address { get; set; }
            public string billing_phone { get; set; }
            public string billing_fax { get; set; }
            public string tax_office { get; set; }
            public string tax_number { get; set; }
            public string city { get; set; }
            public string district { get; set; }
            public bool is_abroad { get; set; }
            public string order_no { get; set; }
            public string order_date { get; set; }
            public string shipment_addres { get; set; }
        }

        public class Attributes2
        {
            public decimal net_total { get; set; }
            public decimal quantity { get; set; }
            public decimal unit_price { get; set; }
            public decimal vat_rate { get; set; }
            public string discount_type { get; set; }
            public decimal discount_value { get; set; }
            public string excise_duty_type { get; set; }
            public decimal excise_duty_value { get; set; }
            public decimal communications_tax_rate { get; set; }
            public string description { get; set; }
        }

        public class Data2
        {
            public string id { get; set; }
            public string type { get; set; }
        }

        public class Product
        {
            public Data2 data { get; set; }
        }

        public class Relationships2
        {
            public Product product { get; set; }
        }

        public class Datum
        {
            public string id { get; set; }
            public string type { get; set; }
            public Attributes2 attributes { get; set; }
            public Relationships2 relationships { get; set; }
        }

        public class Details
        {
            public List<Datum> data { get; set; }
        }

        public class Data3
        {
            public string id { get; set; }
            public string type { get; set; }
        }

        public class Contact
        {
            public Data3 data { get; set; }
        }

        public class Data4
        {
            public string id { get; set; }
            public string type { get; set; }
        }

        public class Category
        {
            public Data4 data { get; set; }
        }

        public class Datum2
        {
            public string id { get; set; }
            public string type { get; set; }
        }

        public class Tags
        {
            public List<Datum2> data { get; set; }
        }

        public class Relationships
        {
            public Details details { get; set; }
            public Contact contact { get; set; }
            public Category category { get; set; }
            public Tags tags { get; set; }
        }

        public class Data
        {
            public string id { get; set; }
            public string type { get; set; }
            public Attributes attributes { get; set; }
            public Relationships relationships { get; set; }
        }


        public class RootObject
        {
            public Data data { get; set; }
        }
       // public Data data { get; set; }
        
    }
}
