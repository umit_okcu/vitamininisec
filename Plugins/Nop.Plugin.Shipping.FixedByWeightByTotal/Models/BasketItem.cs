﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Models
{
    public class BasketItem
    {

        public string Id { get; set; }
        public string Price { get; set; }
        public string Name { get; set; }
        public string Category1 { get; set; }
        public string Category2 { get; set; }
        public string ItemType { get; set; }
        public string SubMerchantKey { get; set; }
        public string SubMerchantPrice { get; set; }
    }
}
