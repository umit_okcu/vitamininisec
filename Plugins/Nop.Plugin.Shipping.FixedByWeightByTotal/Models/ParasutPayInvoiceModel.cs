﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Models
{
    class ParasutPayInvoiceModel
    {
        public class Attributes
        {
            public string description { get; set; }
            public int account_id { get; set; }
            public string date { get; set; }
            public decimal amount { get; set; }
            public int exchange_rate { get; set; }
        }

        public class Data
        {
            public string id { get; set; }
            public string type { get; set; }
            public Attributes attributes { get; set; }
        }

        public class PayInvoice
        {
            public Data data { get; set; }
        }
    }
}
