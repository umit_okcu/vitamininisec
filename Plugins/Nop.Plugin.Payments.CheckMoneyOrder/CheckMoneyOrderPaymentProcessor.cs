﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Armut.Iyzipay.Model;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.CheckMoneyOrder.Models;
using Nop.Services.Configuration;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;

using Nop.Web.Factories;
using Nop.Web.Models.ShoppingCart;
using static Nop.Web.Models.ShoppingCart.ShoppingCartModel;

namespace Nop.Plugin.Payments.CheckMoneyOrder
{
    /// <summary>
    /// CheckMoneyOrder payment processor
    /// </summary>
    public class CheckMoneyOrderPaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly CheckMoneyOrderPaymentSettings _checkMoneyOrderPaymentSettings;
        private readonly ILocalizationService _localizationService;
        private readonly IPaymentService _paymentService;
        private readonly ISettingService _settingService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IWebHelper _webHelper;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;

        #endregion

        #region Ctor

        public CheckMoneyOrderPaymentProcessor(CheckMoneyOrderPaymentSettings checkMoneyOrderPaymentSettings,
            ILocalizationService localizationService,
                                                      IWorkContext workContext,
            IPaymentService paymentService,
            ISettingService settingService,
                                            IStoreContext storeContext,
            IShoppingCartService shoppingCartService,
                    IShoppingCartModelFactory shoppingCartModelFactory,

               IOrderTotalCalculationService orderTotalCalculationService,
            IWebHelper webHelper)
        {
            this._workContext = workContext;
            this._checkMoneyOrderPaymentSettings = checkMoneyOrderPaymentSettings;
            this._localizationService = localizationService;
            this._paymentService = paymentService;
            this._storeContext = storeContext;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._settingService = settingService;
            this._shoppingCartService = shoppingCartService;
            this._webHelper = webHelper;
            this._shoppingCartModelFactory = shoppingCartModelFactory;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var storeScope = _storeContext.ActiveStoreScopeConfiguration; //this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
            var CheckMoneyOrderPaymentSettings = _settingService.LoadSetting<CheckMoneyOrderPaymentSettings>(storeScope);
            var model = new PaymentInfoModel();
            Customer customer = _workContext.CurrentCustomer;
            var dateTimeNowTrim = DateTime.Today.ToString().Replace(" ", "-").Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty);
            var conversationId = customer.Id + "-" + dateTimeNowTrim + "-" + customer.CustomerGuid;

            try
            {



                if (true)//CheckMoneyOrderPaymentSettings.UseParasut == true)
                {



                    ShoppingCartModel _subtotals = GetSubTotals();

                    List<ShoppingCartItemModel> _paidCartItems = _subtotals.Items as List<ShoppingCartItemModel>;
                    foreach (var item in _paidCartItems)
                    {
                        var mahmut = item.SubTotal;
                    }

                    //foreach (var item in _paidCart)
                    //{
                    //    var mahmut = item.Subtotal;
                    //}




                    ///////////////////////////////////////////////
                    ///Get Products for paraşüt Invoice//
                    ///////////////////////////////////////////////

                    var cart = customer.ShoppingCartItems
                    .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                    .Where(sci => sci.StoreId == _storeContext.CurrentStore.Id)
                    .ToList();



                    decimal total;
                    var shoppingCartTotalBase = (decimal)_orderTotalCalculationService.GetShoppingCartTotal(cart, out decimal orderTotalDiscountAmountBase, out List<DiscountForCaching> _, out List<AppliedGiftCard> appliedGiftCards, out int redeemedRewardPoints, out decimal redeemedRewardPointsAmount);

                    total = shoppingCartTotalBase;
                    decimal shippingtotal;


                    shippingtotal = (decimal)_orderTotalCalculationService.GetShoppingCartShippingTotal(cart, true);
                    var cartTotal = total - shippingtotal;

                    ///////////////////////////////////////////////
                    ///Get Parasut Token//
                    ///////////////////////////////////////////////
                    var tokenData = GetToken(CheckMoneyOrderPaymentSettings.ParasutBaseApiToken, CheckMoneyOrderPaymentSettings.ParasutClientId, CheckMoneyOrderPaymentSettings.ParasutUserName, CheckMoneyOrderPaymentSettings.ParasutPassword);
                    string _token = tokenData.Result.ToString();
                    string endPoint = CheckMoneyOrderPaymentSettings.ParasutBaseApi + "/" + CheckMoneyOrderPaymentSettings.ParasutCompanyId;   /////// Create Parasut api EndPoint////
                    string account_id = CheckMoneyOrderPaymentSettings.ParasutBankAccountId;//                                                                                                       ///////////////////////////////////////////////
                                                                                            ///Create costomer for invoice//
                    ///////////////////////////////////////////////
                    var _createCustomer = CreateCustomer(_token, endPoint, customer).Result;
                    var responseCustomerModel = JsonConvert.DeserializeObject<dynamic>(_createCustomer);


                    ///////////////////////////////////////////////
                    // Create Products for Invoice/////////////////
                    ///////////////////////////////////////////////
                    List<BasketItem> basketItems = new List<BasketItem>();










                    for (int i = 0; i < cart.Count; i++)
                    {
                        decimal discounttotal = 0.00m;


                        for (int ii = 0; ii < cart[i].Quantity; ii++)
                        {

                            var category = cart[i].Product.ProductCategories.FirstOrDefault(x => x.Category != null);
                            BasketItem basketItem = new BasketItem();
                            basketItem.Id = cart[i].ProductId.ToString();
                            basketItem.Name = cart[i].Product.Name;

                            if (category != null)
                            {

                                basketItem.Category1 = category.Category.Name;
                            }
                            else
                            {
                                basketItem.Category1 = "--";
                            }
                            basketItem.ItemType = BasketItemType.PHYSICAL.ToString();

                            basketItem.Price = (cart[i].Product.Price - discounttotal).ToString("#.#########").Replace(",", ".");


                            string _____price = _paidCartItems[i].UnitPrice.Replace(".", "").Replace(",", ".");
                            string _unitPrice = new String(_____price.Where(c => char.IsDigit(c) || c == (char)46).ToArray());
                            basketItem.SubMerchantPrice = _unitPrice;


                            basketItems.Add(basketItem);

                        }

                    }
                    ///////////////////add shipping price///////////////////////////
                    if (shippingtotal > 0)
                    {

                        BasketItem shippingItem = new BasketItem();
                        shippingItem.Price = shippingtotal.ToString("#.#########").Replace(",", ".");
                        shippingItem.SubMerchantPrice = shippingtotal.ToString("#.#########").Replace(",", ".");
                        shippingItem.Name = "Kargo Ücreti";
                        shippingItem.Id = "0";
                        basketItems.Add(shippingItem);
                    }
                    ////////////////////////////////////////////////////////////////////////////check If there is any problem//////////////////////////
                    //    List<decimal> discounts = new List<decimal>();

                    ///////////////////////////////////////////////
                    // to discount per item in invoice use here///////////
                    ///////////////////////////////////////////////
                    //foreach (var item in basketItems)
                    //{
                    //    var priceOfProduct = Convert.ToDecimal(item.Price);

                    //    var discountedPriceOfProduct = priceOfProduct - ((priceOfProduct / (total + orderTotalDiscountAmountBase)) * orderTotalDiscountAmountBase);
                    //   // item.Price = discountedPriceOfProduct.ToString("#.##").Replace(",", ".");

                    //    decimal discount = priceOfProduct - discountedPriceOfProduct;
                    //    discounts.Add(discount);

                    //} 



                    List<string> createdParasutProducts = new List<string>();  ///// list of created products////
                    for (int i = 0; i < basketItems.Count; i++)
                    {
                        BasketItem product = basketItems[i];


                        ///////////////////////////////////////////////
                        // Product creation ///////////////////////////
                        ///////////////////////////////////////////////
                        string productResponse = CreateProduct(endPoint, _token, product).Result;

                        createdParasutProducts.Add(productResponse);

                        var responseModel = JsonConvert.DeserializeObject<dynamic>(productResponse);


                    }
                    ///////////////////////////////////////////////
                    // Create Invoice /////////////////////////////
                    ///////////////////////////////////////////////


                    var parasutInvoiceResponce = CreateInvoice(_token, endPoint, createdParasutProducts, responseCustomerModel, customer, orderTotalDiscountAmountBase, basketItems).Result;

                    var parasutInvoiceResponseModel = JsonConvert.DeserializeObject<dynamic>(parasutInvoiceResponce);
                    ///////////////////////////////////////////////
                    // Pay Invoice /////////////////////////////
                    ///////////////////////////////////////////////

                    decimal net_total = parasutInvoiceResponseModel.data.attributes.net_total;
                    int invoice_Id = parasutInvoiceResponseModel.data.id;
                    int bankAccountId = Convert.ToInt32(account_id);
                    var parasutPayInvoice = PayInvoice(_token, endPoint, bankAccountId, net_total, invoice_Id, "Kapıda ödeme").Result;
                    var parasutPayInvoiceResponseModel = JsonConvert.DeserializeObject<dynamic>(parasutInvoiceResponce);

                }
                ///////////////////////////////////////////////
                ///End Of Invoce Creation With Parasüt/////////
                ///////////////////////////////////////////////
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.WriteLine(ex);  /////// Send Message to admin about ex
            }

           
            

            return new ProcessPaymentResult();
        }

        /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //nothing
       


        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country

            if (_checkMoneyOrderPaymentSettings.ShippableProductRequired && !_shoppingCartService.ShoppingCartRequiresShipping(cart))
                return true;

            return false;
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shopping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            return _paymentService.CalculateAdditionalFee(cart,
                _checkMoneyOrderPaymentSettings.AdditionalFee, _checkMoneyOrderPaymentSettings.AdditionalFeePercentage);
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            return new CapturePaymentResult { Errors = new[] { "Capture method not supported" } };
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            return new RefundPaymentResult { Errors = new[] { "Refund method not supported" } };
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            return new VoidPaymentResult { Errors = new[] { "Void method not supported" } };
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            return new ProcessPaymentResult { Errors = new[] { "Recurring payment not supported" } };
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            return new CancelRecurringPaymentResult { Errors = new[] { "Recurring payment not supported" } };
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));

            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Validate payment form
        /// </summary>
        /// <param name="form">The parsed form values</param>
        /// <returns>List of validating errors</returns>
        public IList<string> ValidatePaymentForm(IFormCollection form)
        {
            return new List<string>();
        }

        /// <summary>
        /// Get payment information
        /// </summary>
        /// <param name="form">The parsed form values</param>
        /// <returns>Payment info holder</returns>
        public ProcessPaymentRequest GetPaymentInfo(IFormCollection form)
        {
            return new ProcessPaymentRequest();
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/PaymentCheckMoneyOrder/Configure";
        }

        /// <summary>
        /// Gets a name of a view component for displaying plugin in public store ("payment info" checkout step)
        /// </summary>
        /// <returns>View component name</returns>
        public string GetPublicViewComponentName()
        {
            return "CheckMoneyOrder";
        }

        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new CheckMoneyOrderPaymentSettings
            {
                DescriptionText = "<p>Mail Personal or Business Check, Cashier's Check or money order to:</p><p><br /><b>NOP SOLUTIONS</b> <br /><b>your address here,</b> <br /><b>New York, NY 10001 </b> <br /><b>USA</b></p><p>Notice that if you pay by Personal or Business Check, your order may be held for up to 10 days after we receive your check to allow enough time for the check to clear.  If you want us to ship faster upon receipt of your payment, then we recommend your send a money order or Cashier's check.</p><p>P.S. You can edit this text from admin panel.</p>"
            };
            _settingService.SaveSetting(settings);

            //locales
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFee", "Additional fee");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFee.Hint", "The additional fee.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFeePercentage", "Additional fee. Use percentage");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.DescriptionText", "Description");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.DescriptionText.Hint", "Enter info that will be shown to customers during checkout");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.PaymentMethodDescription", "Pay by cheque or money order");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.ShippableProductRequired", "Shippable product required");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.ShippableProductRequired.Hint", "An option indicating whether shippable products are required in order to display this payment method during checkout.");

            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<CheckMoneyOrderPaymentSettings>();

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFee");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFee.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFeePercentage");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.AdditionalFeePercentage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.DescriptionText");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.DescriptionText.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.PaymentMethodDescription");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.ShippableProductRequired");
            _localizationService.DeletePluginLocaleResource("Plugins.Payment.CheckMoneyOrder.ShippableProductRequired.Hint");

            base.Uninstall();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.NotSupported; }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get { return PaymentMethodType.Standard; }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a payment method description that will be displayed on checkout pages in the public store
        /// </summary>
        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Payment.CheckMoneyOrder.PaymentMethodDescription"); }
        }

        #endregion






        public static async Task<string> GetToken(string uri, string clientId, string userMail, string password)
        {

            string Token_Url = uri + "?client_id=" + clientId + "&username=" + userMail + "&password=" + password + "&grant_type=password&redirect_uri=urn:ietf:wg:oauth:2.0:oob";

            var stringContent = new StringContent("");
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            using (HttpResponseMessage response = await client.PostAsync(Token_Url, stringContent))
            using (HttpContent content = response.Content)
            {
                string data = await content.ReadAsStringAsync();

                if (data != null)
                {

                    // ...   
                    dynamic json = JsonConvert.DeserializeObject(data);

                    var a = json.access_token;


                    return a.ToString();
                }


            }
            return "error";


        }




        public static async Task<string> CreateCustomer(string token, string endPoint, Customer customer)
        {
            string url = endPoint + "/contacts";

            System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            ParasutContactsModel parasutContactsModel = new ParasutContactsModel();

            ParasutContactsModel.Data parasutData = new ParasutContactsModel.Data();


            parasutData.id = "";
            parasutData.type = "contacts";

            ParasutContactsModel.Attributes parasutAttributes = new ParasutContactsModel.Attributes();

            parasutAttributes.email = customer.BillingAddress.Email;
            parasutAttributes.name = customer.BillingAddress.FirstName + " " + customer.BillingAddress.LastName;
            parasutAttributes.contact_type = "person";
            parasutAttributes.tax_number = customer.BillingAddress.TCKN.ToString();
            parasutAttributes.district = customer.BillingAddress.City;
            parasutAttributes.city = customer.BillingAddress.StateProvince.Name;
            parasutAttributes.address = customer.BillingAddress.Address1 + customer.BillingAddress.Address2;//"asdasd";
            parasutAttributes.phone = customer.BillingAddress.PhoneNumber;
            parasutAttributes.archived = false;
            parasutAttributes.account_type = "customer";

            parasutAttributes.short_name = customer.BillingAddress.FirstName;




            parasutAttributes.fax = customer.BillingAddress.FaxNumber;
            parasutAttributes.is_abroad = false;

            parasutAttributes.iban = "";







            ParasutContactsModel.Relationships parasutRelationships = new ParasutContactsModel.Relationships();

            ParasutContactsModel.Category parasutCategory = new ParasutContactsModel.Category();

            ParasutContactsModel.Data1 parasutCategoryData = new ParasutContactsModel.Data1();

            parasutCategoryData.id = "";// "item_kategori_no";
            parasutCategoryData.type = "item_categories";// "item_categories";

            parasutCategory.data = parasutCategoryData;

            parasutRelationships.category = parasutCategory;

            parasutContactsModel.data = parasutData;

            parasutData.attributes = parasutAttributes;
            parasutData.relationships = parasutRelationships;


            var json = JsonConvert.SerializeObject(parasutContactsModel);

            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response2 = await client2.PostAsync("", stringContent);

            var _response = response2.Content.ReadAsStringAsync().Result.Trim();
            var responseModel = JsonConvert.DeserializeObject<dynamic>(_response);

            if (response2.IsSuccessStatusCode)
            {


                return _response;


            }
            else
            {
                return "error";
            }




        }


        public static async Task<string> CreateProduct(string endPoint, string token, BasketItem product)
        {








            ParasutProductsModel parasutProductsModel = new ParasutProductsModel();

            ParasutProductsModel.Data parasutData = new ParasutProductsModel.Data();


            parasutData.id = "";
            parasutData.type = "products";

            ParasutProductsModel.Attributes parasutAttributes = new ParasutProductsModel.Attributes();

            decimal _price = Convert.ToDecimal(product.SubMerchantPrice);
            decimal list_price = _price / 1.18m;   ///1.18x=total =>  x = total *100/118

            decimal net_price = Math.Round(list_price, 5);
            parasutAttributes.code = product.Id;//should edit
            parasutAttributes.name = product.Name;
            parasutAttributes.vat_rate = 18.00m;
            parasutAttributes.sales_excise_duty = 0.00m;
            parasutAttributes.sales_excise_duty_type = "percentage";
            parasutAttributes.purchase_excise_duty = 0;
            parasutAttributes.purchase_excise_duty_type = "percentage";
            parasutAttributes.unit = "";
            parasutAttributes.communications_tax_rate = 0;
            parasutAttributes.archived = false;
            parasutAttributes.list_price = net_price;// 111.98m;


            parasutAttributes.currency = "TRL";



            parasutAttributes.buying_price = net_price;// _price; //142;
            parasutAttributes.buying_currency = "TRL";
            parasutAttributes.inventory_tracking = false;
            parasutAttributes.initial_stock_count = 0;






            ParasutProductsModel.Relationships parasutRelationships = new ParasutProductsModel.Relationships();

            ParasutProductsModel.Category parasutCategory = new ParasutProductsModel.Category();

            ParasutProductsModel.Data1 parasutCategoryData = new ParasutProductsModel.Data1();

            parasutCategoryData.id = "";// "item_kategori_no";
            parasutCategoryData.type = "";// "item_categories";

            parasutCategory.data = parasutCategoryData;

            parasutRelationships.category = parasutCategory;

            parasutProductsModel.data = parasutData;

            parasutData.attributes = parasutAttributes;
            //parasutData.relationships = parasutRelationships;


            var json = JsonConvert.SerializeObject(parasutProductsModel);

            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");





            System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();

            // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);//.Add("application/json");


            string url = endPoint + "/products";

            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var response2 = await client.PostAsync("", stringContent);
            var _response = response2.Content.ReadAsStringAsync().Result.Trim();
            var responseModel = JsonConvert.DeserializeObject<dynamic>(_response);
            if (response2.IsSuccessStatusCode)
            {


                return _response;


            }
            else
            {
                return "error";
            }







        }

        public virtual ShoppingCartModel GetSubTotals()
        {
            var cart = _workContext.CurrentCustomer.ShoppingCartItems
             .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
             .LimitPerStore(_storeContext.CurrentStore.Id)
             .ToList();

            //prepare model
            var model = new ShoppingCartModel();
            model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

            return model;
        }


        public static async Task<string> CreateInvoice(string token, string endPoint, List<string> createdParasutProducts, dynamic responseCustomerModel, Customer customer, decimal orderTotalDiscountAmountBase, List<BasketItem> basketItems)
        {






            ParasutInvoicesModel.RootObject parasutInvoicesModel = new ParasutInvoicesModel.RootObject();

            ParasutInvoicesModel.Data parasutData = new ParasutInvoicesModel.Data();


            parasutData.id = "";
            parasutData.type = "sales_invoices";

            ParasutInvoicesModel.Attributes parasutAttributes = new ParasutInvoicesModel.Attributes();
            var date = DateTime.Now;
            var conversationId = customer.Id + "-" + date + "-" + customer.CustomerGuid;
            var _formatedDate = String.Format("{0:yyyy-MM-dd HH:mm}", date);
            parasutAttributes.item_type = "invoice";
            parasutAttributes.description = conversationId;
            parasutAttributes.issue_date = String.Format("{0:yyyy-MM-dd HH:mm}", date); //"2018-10-08";//datetime 2018-10-04
            parasutAttributes.due_date = _formatedDate; //"2018-10-08";//datetime 2018-10-04
            parasutAttributes.invoice_series = null;//"";
            parasutAttributes.invoice_id = null;
            parasutAttributes.currency = "TRL";
            parasutAttributes.exchange_rate = 1.00m;
            parasutAttributes.withholding_rate = 0.00m;
            parasutAttributes.vat_withholding_rate = 0.00m;
            parasutAttributes.invoice_discount_type = "amount"; //"amount"
            decimal discountWithoutTax = orderTotalDiscountAmountBase / 1.18m; /// discart KDV
            parasutAttributes.invoice_discount = discountWithoutTax;
            parasutAttributes.billing_address = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;
            parasutAttributes.billing_phone = customer.BillingAddress.PhoneNumber;//"5551111122";
            parasutAttributes.billing_fax = null;// "";
            parasutAttributes.tax_office = "İstanbul";// "";
            parasutAttributes.tax_number = customer.BillingAddress.TCKN.ToString();//"5551111122";
            parasutAttributes.city = customer.BillingAddress.StateProvince.Name;

            parasutAttributes.district = customer.BillingAddress.City;
            parasutAttributes.is_abroad = false;
            parasutAttributes.order_no = "";//"";
            parasutAttributes.order_date = _formatedDate; //datetime 2018-10-04
            parasutAttributes.shipment_addres = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;






            ///////////Add cart items to invoice relationships details ///
            List<ParasutInvoicesModel.Datum> CartItems = new List<ParasutInvoicesModel.Datum>();
            var item_quantity = createdParasutProducts.Count;

            for (int i = 0; i < item_quantity; i++)
            {
                var productResponse = JsonConvert.DeserializeObject<dynamic>(createdParasutProducts[i]);
                if (item_quantity > 1)
                {


                    string aa = "1";
                    string b = "2";
                    string c = "1";
                    string d = " 2";
                    string e = "1";
                    string f = "2";
                    if (i >= 1)
                    {

                        var productResponseBefore = JsonConvert.DeserializeObject<dynamic>(createdParasutProducts[i - 1]);

                        aa = productResponse.data.attributes.list_price.ToString();
                        b = productResponseBefore.data.attributes.list_price.ToString();
                        c = productResponse.data.attributes.name.ToString();
                        d = productResponseBefore.data.attributes.name.ToString();
                        e = productResponse.data.attributes.buying_price.ToString();
                        f = productResponseBefore.data.attributes.buying_price.ToString();
                    }



                    if (i >= 1 && (aa == b) && (c == d) && (e == f))
                    {



                        var itemCount = CartItems.Count;

                        var item = CartItems[itemCount - 1];

                        var quantity = item.attributes.quantity;
                        quantity++;
                        item.attributes.quantity = quantity;
                        decimal discount = item.attributes.discount_value;
                        item.attributes.discount_value = (discount / (quantity - 1)) * quantity;




                        CartItems[itemCount - 1] = item;

                    }
                    else
                    {
                        decimal _price = Convert.ToDecimal(basketItems[i].Price);


                        decimal net_price = Math.Round(_price, 4) / 1.18m;
                        decimal discount = 0.00m;
                        if (basketItems[i].Price != basketItems[i].SubMerchantPrice)
                        {
                            discount = Convert.ToDecimal(basketItems[i].Price) - Convert.ToDecimal(basketItems[i].SubMerchantPrice);
                            discount = discount / 1.18m;
                        }
                        decimal net_discount = Math.Round(discount, 4);//Math.Round(discount, 5); 


                        ParasutInvoicesModel.Attributes2 parasutCartItemAtributes = new ParasutInvoicesModel.Attributes2();
                        parasutCartItemAtributes.net_total = net_price;//buying_price;// 210.04m;
                        parasutCartItemAtributes.quantity = 1;
                        parasutCartItemAtributes.unit_price = net_price;
                        parasutCartItemAtributes.vat_rate = 18;
                        parasutCartItemAtributes.discount_type = "amount";//"amount"


                        parasutCartItemAtributes.discount_value = net_discount;//discounts[i];// 0;// "0.00";


                        parasutCartItemAtributes.excise_duty_type = "amount"; //"amount"
                        parasutCartItemAtributes.excise_duty_value = 0;// "0.00";
                        parasutCartItemAtributes.communications_tax_rate = 0;// "0.00";
                        parasutCartItemAtributes.description = "";





                        /////// Add Details data  relationships /////////
                        ParasutInvoicesModel.Relationships2 parasutCartItemRelationships = new ParasutInvoicesModel.Relationships2();
                        ParasutInvoicesModel.Product product = new ParasutInvoicesModel.Product();
                        ParasutInvoicesModel.Data2 productData = new ParasutInvoicesModel.Data2();
                        productData.id = productResponse.data.id;
                        productData.type = "products";

                        product.data = productData;



                        parasutCartItemRelationships.product = product;


                        ParasutInvoicesModel.Datum singleCartItem = new ParasutInvoicesModel.Datum();

                        //////add item relationships for sales_invoice_details/////
                        singleCartItem.relationships = parasutCartItemRelationships;
                        //////add item atributes for sales_invoice_details/////
                        singleCartItem.attributes = parasutCartItemAtributes;

                        singleCartItem.id = "";
                        singleCartItem.type = "sales_invoice_details";

                        CartItems.Add(singleCartItem);
                    }
                }
                else
                {
                    decimal _price = Convert.ToDecimal(basketItems[i].Price);


                    decimal net_price = Math.Round(_price, 4) / 1.18m;
                    decimal discount = 0.00m;
                    if (basketItems[i].Price != basketItems[i].SubMerchantPrice)
                    {
                        discount = Convert.ToDecimal(basketItems[i].Price) - Convert.ToDecimal(basketItems[i].SubMerchantPrice);
                        discount = Math.Round(discount, 4) / 1.18m;
                    }
                    decimal net_discount = Math.Round(discount, 5);
                    ParasutInvoicesModel.Attributes2 parasutCartItemAtributes = new ParasutInvoicesModel.Attributes2();
                    parasutCartItemAtributes.net_total = net_price;//buying_price;// 210.04m;
                    parasutCartItemAtributes.quantity = 1;
                    parasutCartItemAtributes.unit_price = net_price;
                    parasutCartItemAtributes.discount_type = "amount";//"amount"
                    parasutCartItemAtributes.discount_value = net_discount;//discounts[i];// 0;// "0.00";
                    parasutCartItemAtributes.excise_duty_type = "amount"; //"amount"
                    parasutCartItemAtributes.excise_duty_value = 0;// "0.00";
                    parasutCartItemAtributes.communications_tax_rate = 0;// "0.00";
                    parasutCartItemAtributes.description = "";



                    /////// Add Details data  relationships /////////
                    ParasutInvoicesModel.Relationships2 parasutCartItemRelationships = new ParasutInvoicesModel.Relationships2();
                    ParasutInvoicesModel.Product product = new ParasutInvoicesModel.Product();
                    ParasutInvoicesModel.Data2 productData = new ParasutInvoicesModel.Data2();
                    productData.id = productResponse.data.id;
                    productData.type = "products";

                    product.data = productData;



                    parasutCartItemRelationships.product = product;


                    ParasutInvoicesModel.Datum singleCartItem = new ParasutInvoicesModel.Datum();

                    //////add item relationships for sales_invoice_details/////
                    singleCartItem.relationships = parasutCartItemRelationships;
                    //////add item atributes for sales_invoice_details/////
                    singleCartItem.attributes = parasutCartItemAtributes;

                    singleCartItem.id = "";
                    singleCartItem.type = "sales_invoice_details";

                    CartItems.Add(singleCartItem);
                }
            }






            ParasutInvoicesModel.Details parasutInvoiceDetails = new ParasutInvoicesModel.Details();
            parasutInvoiceDetails.data = CartItems;

            ////////////// Add Contact information to Invoive relationships////
            ParasutInvoicesModel.Contact invoiceContact = new ParasutInvoicesModel.Contact();
            ParasutInvoicesModel.Data3 contactData = new ParasutInvoicesModel.Data3();
            contactData.id = responseCustomerModel.data.id;//"10606288";
            contactData.type = "contacts";

            invoiceContact.data = contactData;


            /////////////Add Tag Information////////////

            ParasutInvoicesModel.Tags invoceTags = new ParasutInvoicesModel.Tags();


            List<ParasutInvoicesModel.Datum2> invoiceTagsDataList = new List<ParasutInvoicesModel.Datum2>();

            ParasutInvoicesModel.Datum2 invoiceTagsData = new ParasutInvoicesModel.Datum2();
            invoiceTagsData.id = "string";
            invoiceTagsData.type = "tags";
            invoiceTagsDataList.Add(invoiceTagsData);
            invoceTags.data = invoiceTagsDataList;







            ///////////////Add category Information/////////////

            ParasutInvoicesModel.Category parasutCategory = new ParasutInvoicesModel.Category();

            ParasutInvoicesModel.Data4 parasutCategoryData = new ParasutInvoicesModel.Data4();

            parasutCategoryData.id = "";// "item_kategori_no";
            parasutCategoryData.type = "item_categories";// "item_categories";
            parasutCategory.data = parasutCategoryData;

            ////////////// Put together Relationships /////////////////


            ParasutInvoicesModel.Relationships parasutRelationships = new ParasutInvoicesModel.Relationships();

            parasutRelationships.category = parasutCategory;
            parasutRelationships.details = parasutInvoiceDetails;
            parasutRelationships.contact = invoiceContact;
            //parasutRelationships.tags = invoceTags;



            parasutInvoicesModel.data = parasutData;

            parasutData.attributes = parasutAttributes;
            parasutData.relationships = parasutRelationships;


            var json = JsonConvert.SerializeObject(parasutInvoicesModel);

            System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");

            string url = endPoint + "/sales_invoices";
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //   client2.DefaultRequestHeaders.Add("Content-Type", "application/json");
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            //  stringContent.Headers.Add
            //  HttpResponseMessage response2 = client2.PostAsync("", stringContent).Result;
            var response2 = await client2.PostAsync("", stringContent);//.Result;
            var a = response2.Content.ReadAsStringAsync().Result.Trim();

            var responseModel = JsonConvert.DeserializeObject<dynamic>(a);

            if (response2.IsSuccessStatusCode != false)
            {


                return a;


            }
            else
            {
                return "error";
            }




        }


        public static async Task<string> PayInvoice(string token, string endPoint, int account_id, decimal amountToPay, int invoiceId, string iyzicoPaymenId)
        {

            ParasutPayInvoiceModel.PayInvoice payInvoice = new ParasutPayInvoiceModel.PayInvoice();


            ParasutPayInvoiceModel.Attributes payInvoiceAttributes = new ParasutPayInvoiceModel.Attributes();
            payInvoiceAttributes.account_id = account_id;
            payInvoiceAttributes.description = iyzicoPaymenId.ToString();
            payInvoiceAttributes.date = DateTime.Now.ToString("yyyy-MM-dd");
            payInvoiceAttributes.exchange_rate = 1;
            payInvoiceAttributes.amount = amountToPay;


            ParasutPayInvoiceModel.Data payInvoiceData = new ParasutPayInvoiceModel.Data();
            payInvoiceData.id = "";
            payInvoiceData.type = "payments";
            payInvoiceData.attributes = payInvoiceAttributes;


            payInvoice.data = payInvoiceData;


            var json = JsonConvert.SerializeObject(payInvoice);

            System.Net.Http.HttpClient client2 = new System.Net.Http.HttpClient();
            client2.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
            string url = endPoint + "/sales_invoices/" + invoiceId.ToString() + "/payments";
            client2.BaseAddress = new Uri(url);
            client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //   client2.DefaultRequestHeaders.Add("Content-Type", "application/json");
            HttpContent stringContent = new StringContent(json, Encoding.UTF8, "application/json");
            //  stringContent.Headers.Add
            //  HttpResponseMessage response2 = client2.PostAsync("", stringContent).Result;
            var response2 = await client2.PostAsync("", stringContent);//.Result;
            var a = response2.Content.ReadAsStringAsync().Result.Trim();

            var responseModel = JsonConvert.DeserializeObject<dynamic>(a);

            if (response2.IsSuccessStatusCode != false)
            {


                return a;


            }
            else
            {
                return "error";
            }




        }






    }
}