﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Payments.CheckMoneyOrder.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Payments.CheckMoneyOrder.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    public class PaymentCheckMoneyOrderController : BasePaymentController
    {
        #region Fields

        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public PaymentCheckMoneyOrderController(ILanguageService languageService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._settingService = settingService;
            this._storeContext = storeContext;
        }

        #endregion

        #region Methods

        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var checkMoneyOrderPaymentSettings = _settingService.LoadSetting<CheckMoneyOrderPaymentSettings>(storeScope);

            var model = new ConfigurationModel
            {
                DescriptionText = checkMoneyOrderPaymentSettings.DescriptionText
            };

            //locales
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.DescriptionText = _localizationService
                    .GetLocalizedSetting(checkMoneyOrderPaymentSettings, x => x.DescriptionText, languageId, 0, false, false);
            });
            model.AdditionalFee = checkMoneyOrderPaymentSettings.AdditionalFee;
            model.AdditionalFeePercentage = checkMoneyOrderPaymentSettings.AdditionalFeePercentage;
            model.ShippableProductRequired = checkMoneyOrderPaymentSettings.ShippableProductRequired;


            model.ParasutUserName = checkMoneyOrderPaymentSettings.ParasutUserName;
            model.ParasutPassword = checkMoneyOrderPaymentSettings.ParasutPassword;
            model.ParasutCompanyId = checkMoneyOrderPaymentSettings.ParasutCompanyId;
            model.ParasutBankAccountId = checkMoneyOrderPaymentSettings.ParasutBankAccountId;
            model.ParasutClientId = checkMoneyOrderPaymentSettings.ParasutClientId;
            model.UseParasut = checkMoneyOrderPaymentSettings.UseParasut;

            model.ParasutClientSecret = checkMoneyOrderPaymentSettings.ParasutClientSecret;
            model.ParasutBaseApiToken = checkMoneyOrderPaymentSettings.ParasutBaseApiToken;
            model.ParasutBaseApi = checkMoneyOrderPaymentSettings.ParasutBaseApi;


            model.ActiveStoreScopeConfiguration = storeScope;
            if (storeScope > 0)
            {
                model.DescriptionText_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.DescriptionText, storeScope);
                model.AdditionalFee_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.AdditionalFee, storeScope);
                model.AdditionalFeePercentage_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.AdditionalFeePercentage, storeScope);
                model.ShippableProductRequired_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ShippableProductRequired, storeScope);
                model.ParasutUserName_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutUserName, storeScope);
                model.ParasutPassword_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutPassword, storeScope);
                model.ParasutCompanyId_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutCompanyId, storeScope);
                model.ParasutBankAccountId_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutBankAccountId, storeScope);
                model.ParasutClientId_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutClientId, storeScope);
                model.ParasutClientSecret_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutClientSecret, storeScope);
                model.ParasutBaseApiToken_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutBaseApiToken, storeScope);
                model.ParasutBaseApi_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.ParasutBaseApi, storeScope);

                model.UseParasut_OverrideForStore = _settingService.SettingExists(checkMoneyOrderPaymentSettings, x => x.UseParasut, storeScope);
            }

            return View("~/Plugins/Payments.CheckMoneyOrder/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAntiForgery]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManagePaymentMethods))
                return AccessDeniedView();

            if (!ModelState.IsValid)
                return Configure();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var checkMoneyOrderPaymentSettings = _settingService.LoadSetting<CheckMoneyOrderPaymentSettings>(storeScope);

            //save settings
            checkMoneyOrderPaymentSettings.DescriptionText = model.DescriptionText;
            checkMoneyOrderPaymentSettings.AdditionalFee = model.AdditionalFee;
            checkMoneyOrderPaymentSettings.AdditionalFeePercentage = model.AdditionalFeePercentage;
            checkMoneyOrderPaymentSettings.ShippableProductRequired = model.ShippableProductRequired;
            checkMoneyOrderPaymentSettings.ParasutUserName = model.ParasutUserName;
            checkMoneyOrderPaymentSettings.ParasutPassword = model.ParasutPassword;
            checkMoneyOrderPaymentSettings.ParasutCompanyId = model.ParasutCompanyId;
            checkMoneyOrderPaymentSettings.ParasutBankAccountId = model.ParasutBankAccountId;
            checkMoneyOrderPaymentSettings.ParasutClientId = model.ParasutClientId;
            checkMoneyOrderPaymentSettings.ParasutClientSecret = model.ParasutClientSecret;
            checkMoneyOrderPaymentSettings.UseParasut = model.UseParasut;
            checkMoneyOrderPaymentSettings.ParasutBaseApiToken = model.ParasutBaseApiToken;
            checkMoneyOrderPaymentSettings.ParasutBaseApi = model.ParasutBaseApi;
            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.DescriptionText, model.DescriptionText_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.AdditionalFee, model.AdditionalFee_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.AdditionalFeePercentage, model.AdditionalFeePercentage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ShippableProductRequired, model.ShippableProductRequired_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutClientId, model.ParasutClientId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutClientSecret, model.ParasutClientSecret_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutUserName, model.ParasutUserName_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutPassword, model.ParasutPassword_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutCompanyId, model.ParasutCompanyId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutBankAccountId, model.ParasutBankAccountId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutBaseApiToken, model.UseParasut_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.ParasutBaseApi, model.ParasutBaseApiToken_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(checkMoneyOrderPaymentSettings, x => x.UseParasut, model.ParasutBaseApi_OverrideForStore, storeScope, false);
            //now clear settings cache
            _settingService.ClearCache();

            //localization. no multi-store support for localization yet.
            foreach (var localized in model.Locales)
            {
                _localizationService.SaveLocalizedSetting(checkMoneyOrderPaymentSettings,
                    x => x.DescriptionText, localized.LanguageId, localized.DescriptionText);
            }

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion
    }
}