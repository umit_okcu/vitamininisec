﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Payments.CheckMoneyOrder.Models
{
    public class ConfigurationModel : BaseNopModel, ILocalizedModel<ConfigurationModel.ConfigurationLocalizedModel>
    {
        public ConfigurationModel()
        {
            Locales = new List<ConfigurationLocalizedModel>();
        }

        public int ActiveStoreScopeConfiguration { get; set; }
        
        [NopResourceDisplayName("Plugins.Payment.CheckMoneyOrder.DescriptionText")]
        public string DescriptionText { get; set; }
        public bool DescriptionText_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payment.CheckMoneyOrder.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payment.CheckMoneyOrder.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Plugins.Payment.CheckMoneyOrder.ShippableProductRequired")]
        public bool ShippableProductRequired { get; set; }
        public bool ShippableProductRequired_OverrideForStore { get; set; }

        public IList<ConfigurationLocalizedModel> Locales { get; set; }

        #region Nested class

        public partial class ConfigurationLocalizedModel : ILocalizedLocaleModel
        {
            public int LanguageId { get; set; }
            
            [NopResourceDisplayName("Plugins.Payment.CheckMoneyOrder.DescriptionText")]
            public string DescriptionText { get; set; }
        }

        #endregion


        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutClientId")]
        public string ParasutClientId { get; set; }
        public bool ParasutClientId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutUserName")]
        public string ParasutUserName { get; set; }
        public bool ParasutUserName_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutPassword")]
        public string ParasutPassword { get; set; }
        public bool ParasutPassword_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutCompanyId")]
        public string ParasutCompanyId { get; set; }
        public bool ParasutCompanyId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutBankAccountId")]
        public string ParasutBankAccountId { get; set; }
        public bool ParasutBankAccountId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutClientSecret")]
        public string ParasutClientSecret { get; set; }
        public bool ParasutClientSecret_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutBaseApiToken")]
        public string ParasutBaseApiToken { get; set; }
        public bool ParasutBaseApiToken_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.ParasutBaseApi")]
        public string ParasutBaseApi { get; set; }
        public bool ParasutBaseApi_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Payments.Iyzico.Fields.UseParasut")]
        public bool UseParasut { get; set; }
        public bool UseParasut_OverrideForStore { get; set; }

    }
}