﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Payments.CheckMoneyOrder.Models
{
    public class ParasutProductsModel
    {
        public Data data { get; set; }

        public class Data
        {
            public string id { get; set; }
            public string type { get; set; }
            public Attributes attributes { get; set; }
            public Relationships relationships { get; set; }
        }

        public class Attributes
        {
            public string code { get; set; }
            public string name { get; set; }
            public decimal vat_rate { get; set; }
            public decimal sales_excise_duty { get; set; }
            public string sales_excise_duty_type { get; set; }
            public decimal purchase_excise_duty { get; set; }
            public string purchase_excise_duty_type { get; set; }
            public string unit { get; set; }
            public decimal communications_tax_rate { get; set; }
            public bool archived { get; set; }
            public decimal list_price { get; set; }
            public string currency { get; set; }
            public decimal buying_price { get; set; }
            public string buying_currency { get; set; }
            public bool inventory_tracking { get; set; }
            public decimal initial_stock_count { get; set; }
 
        }

        public class Relationships
        {
            public Category category { get; set; }
        }

        public class Category
        {
            public Data1 data { get; set; }
        }

        public class Data1
        {
            public string id { get; set; }
            public string type { get; set; }
        }
    }
}
